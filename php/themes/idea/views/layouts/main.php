<!DOCTYPE HTML>
<html>
    <head>
        <title>GlobalPayment</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/css/styleMenu.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/favicon.ico" type="image/x-icon" rel="shortcut icon"/> 
     

    </head>
    <body>
        <!----start-wrap---->
        
        <div class="wrap topHeader">
            <!---start-header---->
            <div class="top-links" id="top">
                <div class="contact-info">
                    <?php if (yii::app()->user->isGuest) { ?>
                        <a href="<?php echo yii::app()->createUrl('site/login') ?>"><strong>Iniciar Sesión</strong></a>
                    <?php } else { ?>
                        <a href="<?php echo (yii::app()->user->checkAccess('admin') ? Yii::app()->createUrl('backend/admin/index') : Yii::app()->createUrl('backend/empresas/index')) ?> "><i class="icon-user"></i> <strong><?php echo yii::app()->user->name ?></strong></a>
                        &nbsp;
                        <a href="<?php echo yii::app()->createUrl('site/logout') ?>">Cerrar Sesión </a>  
                    <?php } ?>
                </div>
                <div class="clear"> </div>
            </div>
        </div>
    
    
  <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo yii::app()->createUrl('site/index') ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/logo.png" title="logo" /></a>
              
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php echo (yii::app()->controller->action->id=='index')? 'active':'' ?>"><a href="<?php echo Yii::app()->createUrl('site/index'); ?>">Inicio</a></li>
                        <li class="<?php echo (yii::app()->controller->action->id=='about')? 'active':'' ?>"><a href="<?php echo Yii::app()->createUrl('site/about'); ?>">Que es globalPaymet</a></li>
                                                <li class="<?php echo (yii::app()->controller->action->id=='integracion')? 'active':'' ?>"><a href="<?php echo Yii::app()->createUrl('site/integracion'); ?>">Como Funciona</a></li>
                                                <li class="<?php echo (yii::app()->controller->action->id=='servicios')? 'active':'' ?>"><a href="<?php echo Yii::app()->createUrl('site/servicios'); ?>">Servicios</a></li>
                        <li class="<?php echo (yii::app()->controller->action->id=='contact')? 'active':'' ?>"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">Contacto</a></li>
                        

                        <!-- <li class="<?php echo (yii::app()->controller->action->id=='soporte')? 'active':'' ?>"><a href="<?php echo Yii::app()->createUrl('site/soporte'); ?>">Soporte</a></li> -->
                        <div class="clear"> </div>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
     

  
        <div class="main">
            <div id="page-wrapper">                  
                <?php echo $content; ?>
            </div>

            <div class="content_bottom">
                <div class="wrap">
                    <div class="section group">						
                        <div class="col_1_of_3 span_1_of_3 justify" >
                            <h3>Visión</h3>
                            <p><span>Seremos la organización de atención integral en la evaluación e implementación de soluciones 
                                    en tecnología; líder en el mercado nacional, a través de la mejora continua de nuestros 
                                    servicios, guiada por la integridad, trabajo en equipo, servicio y la innovación de nuestra 
                                    gente.
                                </span>
                            </p>

                            <div class="read_more"><a href="<?php echo Yii::app()->createUrl('site/about'); ?>">Saber más</a></div>
                        </div>
                        <div class="col_1_of_3 span_1_of_3">
                            <h3>Otros Productos</h3>
                            <ul>
                                <li><a href="http://www.curiasport.com" target="_blank">CuriaSport</a></li>
                                <li><a href="#.html" >GlobalMarketing</a></li>
                                <li><a href="http://g-clock.com/" target="_blank">GlobalClock</a></li>
                                <li><a href="#">GlobalBio</a></li>


                            </ul>
                        </div>
                        <div class="col_1_of_3 span_1_of_3">
                            <div class="new-products">
                                <h3>Clientes</h3>
                                
                                <p><a href="http:\\www.curiasport.com"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/clientes/curiasport.jpg" alt=""></a></p>
                                <p><a href="http://curly.unet.edu.ve/ippunet/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/clientes/ipp.jpg" alt=""></a></p>
                               
                                <!-- <div class="read_more"><a href="<?php echo yii::app()->createUrl('site/clientes');?>">Ver Todos</a></div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="footer">
            <div class="wrap">
                <div style="width: 55%">
                    <p class="address">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/location.png" alt="" /> 
                        Calle 11 entre Carreras 24 y 25
                        Edif. Residencias Doña Olga 
                        PB, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Oficina Nº. 1
                        Urb. Barrio Obrero, San Cristóbal.
                    </p>
                </div>
             
                <p class="phone"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/phone.png" alt="" /> Linea Directa: <span>0501-GLOBALR</span></p>
                <div class="clear"></div>
            </div>	
        </div>
        <div class="copy-right">
            <div class="wrap">
                <p>Globalr © Todos los derechos reservados   <a href="http://w3layouts.com">W3Layouts</a></p>
                <div class="clear"></div>
            </div>	
        </div>
       </body>
</html>

