<!DOCTYPE html>
<html>
    <head>
        <title>GlobalPayment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
          <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
        <![endif]-->

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/favicon.ico" type="image/x-icon" rel="shortcut icon"/> 

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/assets/css/flat-ui.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/assets/css/sb-admin.css" rel="stylesheet">

    </head>


    <body>



        <nav class="navbar navbar-inverse navbar-fixed-top " role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                    <span class="sr-only">Toggle navigation</span>
                </button>

                <?php
                if (Yii::app()->user->checkAccess('admin'))
                    echo '<a class="navbar-brand"  href="' . yii::app()->baseUrl . '/backend/admin/index"><img style="height: 40px;" src=' . Yii::app()->request->baseUrl . '/images/logo/payment.png></a>';
                else
                if (Yii::app()->user->checkAccess('empresa'))
                    echo '<a class="navbar-brand"  href="' . yii::app()->baseUrl . '/backend/empresas/index"><img style="height: 40px;" src=' . Yii::app()->request->baseUrl . '/images/logo/payment.png></a>';
                ?>

            </div>

            <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-01">
                <?php if (Yii::app()->user->checkAccess('admin')): ?>      
                    <ul class="nav navbar-nav">

                        <li><a href="<?php echo yii::app()->createUrl('adminEmpresa/lista') ?>">Empresas</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('aplicacion/index') ?>">Aplicaciones</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('servicio/index') ?>">Servicios</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('recurso/index') ?>">Recursos</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('solicitudes/verificarDeposito') ?>"></i> Solicitudes<span class="navbar-new"><?php echo Transaccion::model()->count($condition='(codigoPago=2 or codigoPago=3) and STATUS_PAYMENT_id=1'); ?></span></a></li>
                        <li><a href="<?php echo yii::app()->createUrl('reportes/reportes') ?>"></i> Reportes</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('../auth') ?>">Permisologia</a></li>
                        <li><a type="button" class="btn btn-primary btn-large btn-block " data-toggle="dropdown">
                                <i class="icon-user"></i>&nbsp;&nbsp;&nbsp;<?php echo yii::app()->user->name ?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo yii::app()->createUrl("adminEmpresa/view", array("id" => yii::app()->user->id)) ?>"> Perfil</a></li>                        
                              
                                <li class="divider"></li>
                                <li><a href="<?php echo yii::app()->createUrl('../site/logout') ?>" class="btn btn-primary  btn-large btn-block"> Cerrar Sesión</a></li>
                    
                            </ul>
                        </li>
                        </ul>         
                <?php elseif (Yii::app()->user->checkAccess('empresa')): ?>
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo yii::app()->createUrl('empresas/serviciosContratados') ?>">Servicios</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('empresas/aplicacionesAsociadas') ?>">Aplicaciones</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('empresas/estadoTransaccion') ?>">Transacciones</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('empresas/guiaIntegracion') ?>">Guias de Integracion</a></li>
                        <li><a href="<?php echo yii::app()->createUrl('empresas/descargaRecursos') ?>">Recursos</a></li>
                       <!-- <li><a href="<?php echo yii::app()->createUrl('empresas/listaServicios') ?>"><i class="icon-edit"></i> Adicionar Servicio</a></li> -->
                        <li><a href="<?php echo yii::app()->createUrl('empresas/reportes') ?>"> Reportes</a></li>
                        <li><a type="button" class="btn btn-primary " data-toggle="dropdown">
                                <i class="icon-user"></i>&nbsp;&nbsp;&nbsp;<?php echo yii::app()->user->name ?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo yii::app()->createUrl("empresas/perfil", array("id" => yii::app()->user->id)) ?>"> Perfil</a></li>                        
                                <li class="divider"></li>
                                <li><a href="<?php echo yii::app()->createUrl('../site/logout') ?>" class="btn btn-primary  btn-large btn-block"> Cerrar Sesión</a></li>
                    
                            </ul>
                        </li>
                    </ul>   

                <?php endif; ?>
            </div><!-- /.navbar-collapse -->
        </nav><!-- /navbar -->

        <br><br>


        <div class="col-md-10 col-md-offset-1" style="margin-bottom: 40px; "> 

            <?php echo $content; ?>

        </div>

        <div class="navbar-fixed-bottom" style="text-align: center; background: rgb(235, 235, 235);">

                            <!-- <p>Globalr © Todos los derechos reservados </p>-->

        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
    </body>

</html>
