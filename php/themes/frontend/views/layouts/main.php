<?php /* @var $this Controller */ ?>
<!DOCTYPE html>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title><?php echo Yii::app()->name;?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">    
         <link href="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/favicon.ico" type="image/x-icon" rel="shortcut icon"/> 
       
      
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/css/bootstrap-responsive.min.css" rel="stylesheet">
        
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/css/font-awesome.min.css" rel="stylesheet">      

      <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/css/base-admin-3.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/css/base-admin-3-responsive.css" rel="stylesheet">


        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style type="text/css"></style></head>

    <body>

        <nav class="navbar navbar-inverse" role="navigation" style="color: white; text-align: right; height: 40px;">
            <div style="height: 100%; padding-top: 20px;padding-right: 10px; font-size: 16px;">
                <?php if(yii::app()->user->isGuest){?>
            <a href="<?php echo yii::app()->createUrl('site/login')?>"><strong>Iniciar Sesion</strong></a>
                <?php }else{ ?>
            <a href="<?php echo (yii::app()->user->checkAccess('admin')? Yii::app()->createUrl('backend/admin/index') : Yii::app()->createUrl('backend/empresas/index')) ?> "><i class="icon-user"></i> <strong><?php echo yii::app()->user->name?></strong></a>
            &nbsp;
            <a href="<?php echo yii::app()->createUrl('site/logout')?>">Cerrar Sesion </a>  
                <?php }?>
            </div>
            
        </nav>



<?php $this->widget('bootstrap.widgets.TbNavbar', array(
    'type'=>'null', // null or 'inverse'
    'brand'=>CHtml::image(Yii::app()->getBaseUrl().'/images/logo/payment.png'),
    'brandUrl'=>'#',
    'fixed' => false,
    'fluid'=>'false',
    
    'collapse'=>true, // requires bootstrap-responsive.css
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'type'=>'tabs',
            'htmlOptions'=>array('class'=>'pull-right icono-menu'),
            'items'=>array(
                array('label'=>yii::t('frontEnglish','Inicio'), 'icon'=>'home', 'url'=>Yii::app()->createUrl('site/index')),
                array('label'=>'Nosotros', 'icon'=>'info-sign', 'url'=>Yii::app()->createUrl('site/renderEstaticas', array('pagina'=>'about'))),
                array('label'=>'Contacto', 'icon'=>'comments', 'url'=>Yii::app()->createUrl('site/contact')),
                array('label'=>'Servicios', 'icon'=>'tasks', 'url'=>Yii::app()->createUrl('site/renderEstaticas', array('pagina'=>'servicios'))),
                array('label'=>'Integracion', 'icon'=>'cogs', 'url'=>Yii::app()->createUrl('site/renderEstaticas', array('pagina'=>'integracion'))),
                array('label'=>'Dudas', 'icon'=>'question-sign', 'url'=>Yii::app()->createUrl('site/renderEstaticas', array('pagina'=>'about'))),
                array('label'=>'Registro', 'icon'=>'check-sign', 'url'=>Yii::app()->createUrl('site/renderEstaticas', array('pagina'=>'registro'))),

            ),
        ),
 
    ),
)); ?>

<?php echo $content;?>

        <div class="extra">

            <div class="container">

                <div class="row">

                    <div class="col-md-3" style="font-size: 16px; font-family: sans-serif;">

                        <strong>CONTACTO</strong>

                        <ul>
                            <li></li>
                            <li>Global Resources
                                San Cristóbal:
                                Calle 11 entre Carreras 24 y 25
                                Edif. Residencias Doña Olga
                                PB, Oficina Nº. 1
                                Urb. Barrio Obrero, San Cristóbal.
                            </li>
                            <li>
                                Oficina: <strong>+(58) (276) 3554254</strong>
                            </li>    
                            <li>
                                Central: <strong>0501-GLOBAL-R</strong>
                            </li>                            
                        </ul>

                    </div> <!-- /span3 -->

                    <div class="col-md-3">

                        <h4></h4>

                        <ul>
                            <li><a href="javascript:;"></a></li>
                            
                        </ul>

                    </div> <!-- /span3 -->



                </div> <!-- /row -->

            </div> <!-- /container -->

        </div> <!-- /extra -->




        <div class="footer">

            <div class="container">

                <div class="row">

                    <div id="footer-copyright" class="col-md-6" >
                        <div style="margin-top: 10px">
                        © 2014 Global-R
                        </div>
                    </div> <!-- /span6 -->

                    <div id="footer-terms" class="col-md-6">
                        
                        
                        <div style="float: right; width: 60px;">
                        <a href="#" style="color: #b3b3b3">
                            <i class="icon-twitter" style="font-size: 35px"></i>                                                                     
                        </a>
                        </div>
                        <div style="float: right; width: 40px;">    
                        <a href="#" style="color: #b3b3b3">
                            <i class="icon-facebook" style="font-size: 35px"></i>                                                                     
                        </a>
                        </div>
                        <div style="float: right; margin-top: 10px">  
                        Siguenos en: 
                        </div>

                    </div> <!-- /.span6 -->

                </div> <!-- /row -->

            </div> <!-- /container -->

        </div> <!-- /footer -->

<a id="back-to-top" href="http://jumpstartuidemo.com/themes/base/index.html#top" style="display: none;"><i class="icon-chevron-up"></i></a></body></html>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/js/Application.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/frontend/assets/js/bootstrap.min.js"></script>


</body>
</html>
