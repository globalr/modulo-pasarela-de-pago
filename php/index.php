<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config = dirname(__FILE__).'/protected/config/front.php'; // SEPARANDO FRONTEND AND BACKEND

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once($yii);
Yii::createWebApplication($config)->runEnd('front');// SEPARANDO FRONTEND AND BACKEND
