function decrementSeconds(_idTimerMsg){
    try{
        elementoSegundos = document.getElementById(_idTimerMsg);
        valor = elementoSegundos.innerHTML;
        valor = parseInt(valor);
        if(valor > 0){
            valor = parseInt(valor)-1;            
            if(valor == 0){
                if(decrementMinuts()){
                    elementoSegundos.innerHTML = "59";                                
                }else{
                    elementoSegundos.innerHTML = "00";                                    
                }
            }else{
                if(valor < 10){
                    valor = "0"+valor;
                }
                elementoSegundos.innerHTML = valor;
            }
        }
    }catch(e){
        console.log(e);
    }    
}
function decrementMinuts(){
    try{
        elementoMinutos = document.getElementById("minutos");
        valor = elementoMinutos.innerHTML;
        valor = parseInt(valor);
        if(valor > 0){
            valor = parseInt(valor)-1; 
            if(valor < 10){
                valor = "0"+valor;
            }
            elementoMinutos.innerHTML = valor;            
            return true;
        }else{
            return false;
        }
    }catch(e){
        console.log(e);
    }    
}

var minutos = 0;
var segundos = 0;
function calcularTiempos(min, sec){
    if(sec == 0 && min>0){
        segundos = 59;  
        min = min -1;
    }else{
        segundos = sec;            
    }
    elemento = document.getElementById("segundos");
    elemento.innerHTML = segundos;    
    minutos = min;
    elemento = document.getElementById("minutos");
    elemento.innerHTML = minutos;
}




