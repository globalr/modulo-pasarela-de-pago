/**
* globalr 2013
* funciones javascript utilitarias
*/
function allnumeric(inputtxt){  
	return checkNumberRegex(inputtxt.value);
}

function checkNumberRegex(valor){
	var numbers = /^[0-9]+$/;  
	if(valor.match(numbers)){  
      		return true;  
      	}else{  
		return false;  
      	}  
}

function fechaTDC(valor){
	var numbers = /^[0-9]{4}$/;  
	if(valor.value.match(numbers)){  
      		return true;  
      	}else{  
		return false;  
      	}  
}

function allnumericOrEmpty(inputtxt){  
	if(inputtxt.value == null || inputtxt.value == ""){
		return true;
	}
	return allnumeric(inputtxt);
}   
/**
* retorna true si el caracter tipeado por el usuario es
* un numero, false en caso contrario
*/
function checkNumber(e) {
	var key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	var keychar = String.fromCharCode(key);
	if(key == 8 || key == 9 || key == 0){
		return true;
	}else{
		return checkNumberRegex(keychar);
	}
}