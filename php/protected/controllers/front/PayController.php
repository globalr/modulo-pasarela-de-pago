<?php

require_once 'lib/globalr/cypher/cypher.php';
include Yii::app()->basePath . '/payment/lib/globalr/payment/coredev.php';
include Yii::app()->basePath . '/payment/lib/globalr/dbtools/util.php';

//include Yii::app()->basePath.'/payment/lib/globalr/mail/globalMail.php';
class PayController extends Controller {

    
    public function actionTdc() {
        $this->layout = '//layouts/admin';
        $model = new PagoTdc;

        if (isset($_POST['PagoTdc'])) {

            $model->attributes = $_POST['PagoTdc'];

            if ($model->validate()) {

                $transaccion = Transaccion::model()->findByAttributes(array('codigoTransaccion' => Yii::app()->user->getState('codigoTransaccion'), 'APLICACION_id' => Yii::app()->user->getState('idAplicacion')));

                if($transaccion){
                    if ($transaccion->STATUS_PAYMENT_id == 'Procesando' || $transaccion->STATUS_PAYMENT_id == 'Aprobada')
                        throw new CHttpException(1234, 'disculpe La transaccion ya está registrada.');
                }else{
                    $transaccion= new Transaccion;
                    $transaccion->codigoTransaccion = $model->idTransaccion;                    
                    $transaccion->descripcion = 'transaccion iniciada';
                    $transaccion->monto = $model->monto;
                    $transaccion->tiempoTransaccion = $model->tiempo;
                    $transaccion->APLICACION_id = Yii::app()->user->getState('idAplicacion');
                    $transaccion->STATUS_PAYMENT_id = 1;
                    $transaccion->codigoPago = 1;
                    $transaccion->rifCliente = $model->rif;
                }
                    
                if ($model->tiempo > 0) {
                    // START PAYMENT
                    // obtener el cliente nusoap, conexion con el web service 
                    // de sitef
                    //$conexion = obtenerConexion();
                    //$err = $conexion->getError();

                    $err = false; //para simular
                    if ($err) {
                        $adminMsg = $err;
                        $userMsg = "Error PY1, su transacción no pudo ser procesada";
                    } else {
                        // configurar el objeto para solicitar el pago                
                        // $objetoPago = obtenerObjetoPago($conexion);

                        $objetoPago = true; //para simular
                        if ($objetoPago) {

                            //REQUERIDO. IMPORTANT: When using IATA this field is expected the sum (amount + departureTax)
                            //CAMBIO REALIZADO POR MI
                            $merchantUSN = $model->idTransaccion; //Sequential number of the order (in store) *** . no requerido 		
                            // BEGIN TRANSACTION
                            $transactionResponse = "-1";
                            for ($i = 1; $i <= 3; $i++) {
                                $transactionResponse = obtenerNitTimed($objetoPago, $model->monto, $merchantUSN, $model->idTransaccion);
                                if ($transactionResponse != "-1") {
                                    break;
                                }
                            }
                            //$transactionResponse= "-1";// para pruebas
                            if ($transactionResponse == "-1") { //timeout en beginTransaction
                                // enviar alarma a administrador
                                // registrar en logs                                
                                $userMsg='<strong>¡Error PY2!. </strong> su transacción no pudo ser procesada...';
                            } else {
                                $nit = $transactionResponse['transactionResponse']['nit'];
                                // parametros que vienen del sistema que solicita el pago
                                $customerId = $model->rif; //OJO MODIFICADO POR MI ES EL RIF O CEDULA DEL QUE PAGA
                                $cardNumber = $model->numeroTarjeta; //OJO MODIFICADO POR MI ES EL NUMERO DE TARJETA DE CREDITO
                                //$nit=null;//PARA PRUEBAS
                                if ($nit) {
                                    /* $authorizerId = $model->tipoTarjeta; //TIPO DE LA TARJETA DE CREDITO POR EJEMPLO VISA
                                      $autoConfirmation = 'true';
                                      $cardExpiryDate = $model->fechaVencimiento;
                                      $cardSecurityCode = $model->codigoSeguridad;
                                      $extraField = '';
                                      $installmentType = '4';
                                      $installments = '1';

                                      $solicitud = obtenerSolicitudPago($authorizerId, $autoConfirmation, $cardExpiryDate, $cardNumber, $cardSecurityCode, $customerId, $extraField, $installmentType, $installments, $nit);
                                     */
                                    $solicitud = true; //para simulacion

                                    $resultado = pagarTimeout($objetoPago, $solicitud);

                                    if ($resultado == -1) {
                                        // timeout en dopayment	
                                        for ($i = 1; $i <= 3; $i++) {
                                            $resultado = obtenerEstatusTransaccionTimeOut($objetoPago, $nit);
                                            //print_r($resultado);
                                            if ($resultado != "-1") {
                                                break;
                                            }
                                        }
                                    }

                                    // Aca se debe evaluar el resultado de la transaccion y si es necesario
                                    // arreglar el error que se está produciendo
                                    // o mostrar la salida que indica al usuario el tipo de error
                                    // que se esta presentado, en caso de sea un problema
                                    // relativo a datos incorrectos
                                    $responseCode = $resultado['paymentResponse']['responseCode'];
                                    $responseStatus = $resultado['paymentResponse']['transactionStatus'];
                                    $adminMsg = $resultado['paymentResponse']['message'];

                                    //GUARDO EN LA BASE DE DATOS SI EL RESULTADO DE LA TRANSACCION ES COMPLETADA O NEGADA
                                    //GUARDO EN EL HISTORICO EL ESTADO ANTERIOR DE LA TRANSACCION
                                    //$dbtool->consultar("UPDATE transaccion set estatus='$responseStatus', fhmod=now() where nit='$nit';");
                                    
                                    //$responseCode=0;//PARA PRUEBAS
                                    //$responseStatus = 'CAN';
                                    if ($responseCode == 0) {   
                                        switch ($responseStatus) {
                                            case 'NOV':
                                                $transaccion->STATUS_PAYMENT_id = 1;
                                                $userMsg = "La transaccion está siendo procesada";
                                                break;
                                            case 'CON':
                                                $transaccion->STATUS_PAYMENT_id = 2;
                                                $userMsg = "el pago se ha registrado exitosamente";
                                                break;
                                            case 'NEG':
                                                $transaccion->STATUS_PAYMENT_id = 3;
                                                $userMsg = "La transaccion ha sido negada";
                                                break;
                                            default :
                                                $transaccion->STATUS_PAYMENT_id =5;
                                                $transaccion->codigoError = $responseStatus;
                                                $userMsg = "se ha producido un error interno. Contacte al Administrador";
                                                break;                                            
                                        }                                                                                
                                    } else {
                                        $responseStatus = 'INV';
                                        switch ($responseCode) {
                                            case 1:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 2:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 3:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 4:
                                                $userMsg = "Error al procesar la transacción: Tipo de tarjeta inválido";
                                                break;
                                            case 5:
                                                $userMsg = "Error al procesar la transacción: Tipo de tarjeta inválido";
                                                break;
                                            case 6:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 7:
                                                $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                break;
                                            case 8:
                                                $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                break;
                                            case 9:
                                                $userMsg = "Error al procesar la transacción: Fecha de vencimiento ha expirado";
                                                break;
                                            case 10:
                                                $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                break;
                                            case 11:
                                                $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                break;
                                            case 12:
                                                $userMsg = "(12) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                break;
                                            case 13:
                                                $userMsg = "(13) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                break;
                                            case 14:
                                                $userMsg = "(14) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                break;
                                            case 15:
                                                $userMsg = "(15) Error al procesar la transacción: Número de tarjeta incorrecto";

                                                break;
                                            case 16:
                                                $userMsg = "(16) Error al procesar la transacción: Número de tarjeta incorrecto";

                                                break;
                                            case 17:
                                                $userMsg = "(17) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                break;
                                            case 18:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 19:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 20:
                                                $userMsg = "Error al procesar la transacción: Id de cliente incorrecto";
                                                break;
                                            case 22:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 23:
                                                $userMsg = "Error al procesar la transacción";
                                                break;
                                            case 30:
                                                $userMsg = "Error al procesar la transacción: La transacción ya está finalizada";
                                                break;
                                            case 131:
                                                $userMsg = "Error al procesar la transacción: Error de comunicación";
                                                break;
                                            case 150:
                                                $userMsg = "Error al procesar la transacción: Error de comunicación";
                                                break;
                                            case 255:
                                                $userMsg = "Error al procesar la transacción: operaci&oacute;n rechazada";
                                                break;
                                        }
                                        $transaccion->STATUS_PAYMENT_id =5;                                        
                                        $transaccion->codigoError = $responseCode;
                                        
                                      }
                                    
                                   $transaccion->fechaCreacion = date('Y-m-d H:i:s');
                                   if($transaccion->save()){
                                        $historicoTransaccion = new LogEstadosTransaccion;
                                        $historicoTransaccion->TRANSACCION_id = $transaccion->id;
                                        $historicoTransaccion->statusTransaccion = $transaccion->STATUS_PAYMENT_id;
                                        $historicoTransaccion->fecha = date('Y-m-d H:i:s');
                                        if ($historicoTransaccion->save()){
                                            if($responseStatus=='CON')
                                                $this->render('respuesta', array('valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos'), 'idTransaccion' => Yii::app()->user->getState('codigoTransaccion'), 'idAplicacion' => Yii::app()->user->getState('codigoAplicacion'), 'status' => $transaccion->STATUS_PAYMENT_id));
                                                Yii::app()->user->setState('codigoAplicacion', null);
                                                Yii::app()->user->setState('idAplicacion', null);
                                                Yii::app()->user->setState('codigoTransaccion', null);  
                                                Yii::app()->user->setState('valoresEstaticos', null); 
                    
                                        }else
                                            $userMsg = 'Error PY5. por favor contacte al administrador.';
                                        
                                  }else
                                       $userMsg = 'Error PY6. por favor contacte al administrador.';                                
                                  
                                       
                                    
                                    
                                    // si el response code es alguno de los que implica
                                    // que el proceso trabaja de forma erronea, es decir
                                    // uno de los siguientes:
                                    // 255, 150, 131, 6 , 1, 2, 3, 4, 5
                                    $error_email = array(255, 150, 131, 6, 1, 2, 3, 4, 5);

                                    //ENVIAR EMAIL CON INFORMACION DE TRANSACCIONES RECHAZADAS
                                    /*
                                      if (in_array($responseCode, $error_email)) {
                                      // consultar las reservaciones
                                      $consultaTransaccionesRechazadas = $dbtool->consultar("SELECT count(id) as total FROM transaccion WHERE estatus!='CON' and fcrea >= date(now()) and aplicacion_id=$app_id");
                                      $transaccionesRechazadas = mysql_fetch_assoc($consultaTransaccionesRechazadas);
                                      echo "DEV --> transacciones reprobadas:" . $transaccionesRechazadas['total'];
                                      if ($transaccionesRechazadas['total'] > 5) {
                                      $mailBody = "<p>Transacciones rechazadas hoy: " . $transaccionesRechazadas['total']."</p>";
                                      // la trasacción ya existe, mostrar mensaje al usuario e interrumpir el flujo
                                      enviarCorreo("samuel.olaechea@globalr.net", "Alarma de transacciones rechazadas", $mailBody);
                                      }
                                      }
                                     */
                                } else                                    
                                    $userMsg = '<strong>¡Error PY3!. </strong> su transacción no pudo ser procesada...';
                                    
                            }        
                        } else {
                            //$adminMsg = $conexion->getError();
                            $userMsg ='<strong>¡Error PY4!. </strong> Por favor contacte al administrador...';
                        }
                    }
                    Yii::app()->user->setFlash('info', $userMsg);
                    $this->render('tdc', array('model' => $model,'valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos')));                                    
                                   
            
                } else {                    
                    //tiempo expirado
                    $transaccion->fechaCreacion=date('Y-m-d H:i:s');
                    $transaccion->STATUS_PAYMENT_id=4; //4 == Tiempo Expirado
                    $userMsg = '<strong>¡Error!. </strong> El tiempo de la transaccion ha expirado...';
                    if ($transaccion->save()) {
                        $historicoTransaccion = new LogEstadosTransaccion;
                        $historicoTransaccion->TRANSACCION_id = $transaccion->id;
                        $historicoTransaccion->statusTransaccion = $transaccion->STATUS_PAYMENT_id;
                        $historicoTransaccion->fecha = date('Y-m-d H:i:s');
                        if(!$historicoTransaccion->save())
                            $userMsg = 'Error PY5. por favor contacte al administrador.';
                                        
                   }else
                       $userMsg = 'Error PY6. por favor contacte al administrador.'; 
                   
                    Yii::app()->user->setFlash('info', $userMsg);
                    $this->render('tdc', array('model' => $model,'valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos')));
                }
            } else
                $this->render('tdc', array('model' => $model,'valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos')));
            
        } else {
            if (isset($_POST['idEmpresa'], $_POST['password'], $_POST['idAplicacion'], $_POST['idTansaccion'], $_POST['tiempoTransaccion'], $_POST['tipoPago'], $_POST['Monto'])) {

                $idEmpresa = decrypt($_POST['idEmpresa']);
                $password = decrypt($_POST['password']);
                $idAplicacion = decrypt($_POST['idAplicacion']);
                $idTansaccion = decrypt($_POST['idTansaccion']);
                $tiempoTransaccion = decrypt($_POST['tiempoTransaccion']);
                $tipoPago = decrypt($_POST['tipoPago']);
                $monto = decrypt($_POST['Monto']);                
                $empresa = Usuario::model()->findByAttributes(array('codigoEmpresa' => $idEmpresa));
                if (strcmp(decrypt($empresa->password), $password) === 0) {
                        $aplicacion = Aplicacion::model()->findByAttributes(array('USUARIOS_id' => $empresa->id, 'codigoAplicacion' => $idAplicacion));
                        if ($aplicacion) {                            
                            Yii::app()->user->setState('codigoAplicacion', $aplicacion->codigoAplicacion);
                            Yii::app()->user->setState('idAplicacion', $aplicacion->id);
                            Yii::app()->user->setState('codigoTransaccion', $idTansaccion);                               
                            Yii::app()->user->setState('valoresEstaticos', array('urlRetorno'=>$aplicacion->urlRetorno,'cancelUrl'=>$aplicacion->cancelUrl,'dirApp'=>$aplicacion->nombre));                            
                            $model->idTransaccion =$idTansaccion;
                            $model->monto=$monto;                            
                            $model->tiempo=$tiempoTransaccion;                             
                            $this->render('tdc', array('model' => $model,'valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos')));
                        }
                        else
                            throw new CHttpException(404, 'Datos de validacion incorrectos.idAplicacion incorrecto');
                    }
                    else
                        throw new CHttpException(404, 'Datos de validacion incorrectos.Password incorrecto');
            }
            else
                throw new CHttpException(404, 'Exiten inconvenientes con los datos recibidos.');
        }// fin else recibiendo por post
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    public function actionDepTrans() {

        $this->layout = 'null';
        $model = new DetalleTransaccion;
        if (isset($_POST['DetalleTransaccion'], $_POST['tipoPago'])) {
            $model->attributes = $_POST['DetalleTransaccion'];
            if ($model->validate()) {
                //Yii::app()->user->setState('codigoTransaccion', null);
                
               if (Transaccion::model()->findByAttributes(array('codigoTransaccion' => yii::app()->user->getState('codigoTransaccion'), 'APLICACION_id' => yii::app()->user->getState('idAplicacion')))) {
                                throw new CHttpException(1234, 'La transaccion ya existe.');
               }else {
                    $transaccion= new Transaccion;
                    $transaccion->codigoTransaccion = yii::app()->user->getState('codigoTransaccion'); 
                    $transaccion->fechaCreacion=date('Y-m-d H:i:s');
                    $transaccion->descripcion = 'transaccion iniciada';
                    $transaccion->monto = $model->monto;
                    $transaccion->tiempoTransaccion = '20';
                    $transaccion->APLICACION_id = yii::app()->user->getState('idAplicacion');
                    $transaccion->STATUS_PAYMENT_id = 1;
                    $transaccion->codigoPago = $_POST['tipoPago'];
                    $transaccion->rifCliente = $model->rifCliente;
                    if($transaccion->save()){
                        $historicoTransaccion = new LogEstadosTransaccion;
                        $historicoTransaccion->TRANSACCION_id = $transaccion->id;
                        $historicoTransaccion->statusTransaccion = $transaccion->STATUS_PAYMENT_id;
                        $historicoTransaccion->fecha = date('Y-m-d H:i:s');
                        if($historicoTransaccion->save()){
                            if($transaccion->codigoPago==2)
                                $model->bancoEmisor=10;
                           $model->TRANSACCION_id=$transaccion->id;
                            if ($model->save()) {
                                $this->render('respuesta', array('valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos'), 'idTransaccion' => Yii::app()->user->getState('codigoTransaccion'), 'idAplicacion' => Yii::app()->user->getState('codigoAplicacion'), 'status' => 'Procesando'));
                                Yii::app()->user->setState('codigoAplicacion', null);
                                Yii::app()->user->setState('idAplicacion', null);
                                Yii::app()->user->setState('codigoTransaccion', null);  
                                Yii::app()->user->setState('valoresEstaticos', null); 
                                
                            }
                            else
                                throw new CHttpException(404, 'Error interno al intentar guardar el detalle del deposito'); 
                            
                        }else
                            throw new CHttpException(404, 'Error PY5. por favor contacte al administrador.');
                        
                        
                }else
                            throw new CHttpException(404, 'Error interno al intentar guardar la transaccion en la DB.');
            }
            }
            else
                $this->render('depTrans', array('model' => $model,'valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos')));
        } else {
            if (isset($_POST['idEmpresa'], $_POST['password'], $_POST['idAplicacion'], $_POST['idTansaccion'], $_POST['tiempoTransaccion'], $_POST['tipoPago'], $_POST['Monto'])) {


                $idEmpresa = decrypt($_POST['idEmpresa']);
                $password = decrypt($_POST['password']);
                $idAplicacion = decrypt($_POST['idAplicacion']);
                $idTansaccion = decrypt($_POST['idTansaccion']);
                $tiempoTransaccion = decrypt($_POST['tiempoTransaccion']);
                $tipoPago = decrypt($_POST['tipoPago']);
                $monto = decrypt($_POST['Monto']);

                $empresa = Usuario::model()->findByAttributes(array('codigoEmpresa' => $idEmpresa));

                    if (strcmp(decrypt($empresa->password), $password) === 0) {

                        $aplicacion = Aplicacion::model()->findByAttributes(array('USUARIOS_id' => $empresa->id, 'codigoAplicacion' => $idAplicacion));

                        if ($aplicacion) {                      
                                Yii::app()->user->setState('valoresEstaticos', array('urlRetorno'=>$aplicacion->urlRetorno,'cancelUrl'=>$aplicacion->cancelUrl,'dirApp'=>$aplicacion->nombre)); 
                                
                                Yii::app()->user->setState('codigoAplicacion', $idAplicacion);
                                Yii::app()->user->setState('idAplicacion', $aplicacion->id);
                                Yii::app()->user->setState('codigoTransaccion', $idTansaccion);                                
                                $model->monto=$monto;   
                                $model->TRANSACCION_id='1';
                                $this->render('depTrans', array('model' => $model, 'valoresEstaticos'=>yii::app()->user->getState('valoresEstaticos')));
                             
                        }
                        else
                            throw new CHttpException(404, 'Datos de validacion incorrectos. idaplicacion Incorrecto');
                    }
                    else 
                        throw new CHttpException(404, 'Datos de validacion incorrectos.Contraseña Incorrecta');

                }
                else
                    throw new CHttpException(404, 'Exiten inconvenientes con los datos recibidos.');
        }
    }

}

?>
