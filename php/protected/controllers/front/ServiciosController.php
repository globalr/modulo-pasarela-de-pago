<?php

class ServiciosController  extends Controller{
    

    public function actions()
    {
        return array(
            'transaccion'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }
    
 /**
     * @param string the symbol of the stock
     * @return float the stock price
     * @soap
     */
    public function getPrice($symbol)
    {
        $prices=array('IBM'=>100, 'GOOGLE'=>350);
        return isset($prices[$symbol])?$prices[$symbol]:0;
        //...return stock price for $symbol
    }
    
    
    /**
     * @param string el id de la empresa
     * @param string el password de acceso
     * @param string el id de la aplicacion
     * @param string el id de la transacciona consultar
     * @return string el estatus de la transaccion
     * @soap
     */
    public function getTransaccion($idEmpresa,$password,$idAplicacion,$idTransaccion)
    {
        $empresa = Usuario::model()->findByAttributes(array('codigoEmpresa'=>$idEmpresa));
        if($empresa){
            if($password == decrypt($empresa->password)){
                $aplicacion = Aplicacion::model()->findByAttributes(array('codigoAplicacion'=>$idAplicacion,'USUARIOS_id'=>$empresa->id));
                if($aplicacion){
                    $transaccion= Transaccion::model()->findByAttributes(
                            array(),
                            $condition='APLICACION_id =:aplicacion and codigoTransaccion =:transaccion',
                            array(':aplicacion'=>$aplicacion->id,':transaccion'=>$idTransaccion));
                    if($transaccion){                           
                       return $transaccion->STATUS_PAYMENT_id;
                    }else
                        return 'ERW4';
                }else
                    return 'ERW3';
            }else
                return 'ERW2';
        }else
            return 'ERW1';
       
    }
    
    /*
     * errores de retorno del webService:
     * ERW1 => id de Empresa incorrecto o no existe
     * ERW2 => password incorrecto
     * ERW2 => id de aplicacion incorrecto
     * ERW4 => id de transaccion incorrecto o no existe
     */
    
    
    /**
     * @param string the symbol of the stock
     * @param string the symbol of the stock
     * @param string the symbol of the stock
     * @param string the symbol of the stock
     * @return string[] the stock price
     * @soap
     */
    public function getTransaccionArray($idEmpresa,$password,$idAplicacion,$idTransaccion)
    {
        $empresa = Usuario::model()->findByAttributes(array('codigoEmpresa'=>$idEmpresa));
        if($empresa){
            if($password == decrypt($empresa->password)){
                $aplicacion = Aplicacion::model()->findByAttributes(array('codigoAplicacion'=>$idAplicacion));
                if($aplicacion){
                    $transaccion= Transaccion::model()->findByAttributes(
                            array(),
                            $condition='APLICACION_id =:aplicacion and codigoTransaccion =:transaccion',
                            array(':aplicacion'=>$aplicacion->id,':transaccion'=>$idTransaccion));
                    if($transaccion){
                        return $array = [
                                "transaccion" => $transaccion->codigoTransaccion,
                                "estado" => $transaccion->STATUS_PAYMENT_id,
                            ];                        
                       // return $transaccion->STATUS_PAYMENT_id;
                    }else
                        return 'nada';
                }else
                    return 'nada';
            }else
                return 'nada';
        }else
            return 'nada';
       
    }
    
    
    
    /**
     * @param string the symbol of the stock
     * @param string the symbol of the stock
     * @param string the symbol of the stock
     * @param string the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function getTransaccionJSON($idEmpresa,$password,$idAplicacion,$idTransaccion)
    {
        $empresa = Usuario::model()->findByAttributes(array('codigoEmpresa'=>$idEmpresa));
        if($empresa){
            if($password == decrypt($empresa->password)){
                $aplicacion = Aplicacion::model()->findByAttributes(array('codigoAplicacion'=>$idAplicacion));
                if($aplicacion){
                    $transaccion= Transaccion::model()->findByAttributes(
                            array(),
                            $condition='APLICACION_id =:aplicacion and codigoTransaccion =:transaccion',
                            array(':aplicacion'=>$aplicacion->id,':transaccion'=>$idTransaccion));
                    if($transaccion){                           
                       return CJSON::encode($transaccion->STATUS_PAYMENT_id);
                    }else
                        return 'error de parametros';
                }else
                    return 'error de parametros';
            }else
                return 'error de parametros';
        }else
            return 'error de parametros';
       
    }
    
}  

?>
