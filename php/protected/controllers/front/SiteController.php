<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
    
		);
	}

        

        public function actionEmpresa()
{
    $model=new Usuario;

    // uncomment the following code to enable ajax-based validation
    /*
    if(isset($_POST['ajax']) && $_POST['ajax']==='usuario-empresa-form')
    {
        echo CActiveForm::validate($model);
        Yii::app()->end();
    }
    */

    if(isset($_POST['Usuario']))
    {
        $model->attributes=$_POST['Usuario'];
        if($model->validate())
        {
            // form inputs are valid, do something here
            return;
        }
    }
    $this->render('empresa',array('model'=>$model));
}

	public function actionIndex()
	{		
		$this->render('index');
	}
        
        public function actionClientes(){
            $this->render('clientes');
        }
        
        
        public function actionServicios(){
            $this->render('servicios');
        }
        
        public function actionIntegracion(){
            $this->render('integracion');
        }
        public function actionSoporte(){
            $this->render('soporte');
        }
      public function actionAbout(){
            $this->render('about');
        }
        public function actionAdminRoles()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('adminRoles');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                                $mailer->Host="smtp.gmail.com";
                                $mailer->IsSMTP();
                                $mailer->CharSet = 'utf-8';
                                $mailer->IsHTML(true);
                                $mailer->SMTPAuth = true;
                                $mailer->SMTPSecure = "ssl";
                                $mailer->From = $model->email;
                                //$mailer->To= 'david.galvis@globalr.net';
                                $mailer->AddReplyTo($model->email);
                                $mailer->AddAddress($model->email,$model->name);
                                $mailer->AddAddress("david.galvis.barrios@gmail.com",'David Galvis');
                                $mailer->FromName = $model->name;                                
                                $mailer->Subject = $model->subject;
                                $mailer->Body = $model->body;
                                $mailer->Port=465;
                                $mailer->Username = "david.galvis.barrios@gmail.com";
                                $mailer->Password = "israel076";                                
                                
                                if($mailer->Send())
                                    Yii::app()->user->setFlash('info','Gracias por comunicarte con Nosotros. Pronto te atenderemos. ');
				else
                                    
                                    Yii::app()->user->setFlash('info','no se pudo');
				
                               
			}
		}
		$this->render('contact',array('model'=>$model));
	}
        
        public function actionAccesoDenegado(){
            $this->render('accesoDenegado');
            
        }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {

                if (Yii::app()->user->checkAccess('admin')) {  

                    $this->redirect(Yii::app()->request->baseUrl . "/backend/admin/index"); // OJO CON ESTO        
                    //$this->redirect(Yii::app()->request->baseUrl . "/auth"); // OJO CON ESTO
                } else
                if (Yii::app()->user->checkAccess('empresa')) {                    
                    $this->redirect(Yii::app()->request->baseUrl . "/backend/empresas/index"); // OJO CON ESTO
                }
                else
                    $this->redirect(Yii::app()->user->returnUrl);
            }else{
                
                Yii::app()->user->setFlash('error', '<strong>Error!</strong> Usuario o Contraseña Incorrectos.');
            }
        }
		// display the login form
		$this->render('login',array('model'=>$model));
             
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
        
        
        
        
        
        //****************************************************** ZONA DE PAGO
        
        
        public function actionPago(){
            $model=  Transaccion::model();
            $this->render('pago',array('model'=>$model));
        }

}