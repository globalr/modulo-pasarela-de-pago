<?php

class AdminController extends BackEndController {

    public function actionIndex() {
        $model = Usuario::model();
        $this->render('index', array('model' => $model));
    }

    /* tarea programada para evaluar las transacciones que quedaron procesando 
      y no se procesaron por algun inconveniente */

    public function actionCronJobTransacciones() {
        //admin:Globalr2014
        if (isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            if ($_SERVER['PHP_AUTH_USER'] == 'admin' && $_SERVER['PHP_AUTH_PW'] == 'Globalr2014') {
                $horaActual = date("Y-m-d H:i:s");
                Transaccion::model()->updateAll(array('STATUS_PAYMENT_id' => 4), '(fechaCreacion <:tiempo) and (STATUS_PAYMENT_id = 1 )', array(':tiempo' => date('Y-m-d H:i:s', strtotime('-1 day', strtotime($horaActual)))));
                $this->render('cronJobTransacciones');
            } else {
                header('HTTP/1.0 401 Unauthorized');
                die("No Autorizado");
            }
        } else {
            header('HTTP/1.0 401 Unauthorized');
            die("No Autorizado");
        }
    }

    public function actionCronJobGuardarEnHistorico() {

        if (true) {
            if (true) {

                $diaActual = date("Y-m-d");
                $tiempoResta = date('Y-m-d', strtotime('-1 month', strtotime($diaActual)));
                $model = Transaccion::model();
                $transaction = $model->dbConnection->beginTransaction();
                try {
                    $transViejas = $model->with('detalleTransaccions')->findAll('fechaCreacion <:tiempo', array(':tiempo' => $tiempoResta));
                    foreach ($transViejas as $trans) {
                        $historico = new HistoricoTransaccion;
                        $historico->attributes = $trans->attributes;

                        if ($historico->save()) {
                            if ($historico->codigoPago != 'Tarjeta de Credito') {
                                $detalleHistorico = new DetalleHistorico;
                                $detalleHistorico->attributes = $trans->detalleTransaccions[0]->attributes;
                                $detalleHistorico->TRANSACCION_id = $historico->id;
                                $detalleHistorico->save();
                            }
                        }
                    }
                    $transaction->commit();
                    $model->with('detalleTransaccion')->deleteAll('fechaCreacion <:tiempo', array(':tiempo' => $tiempoResta));
                } catch (Exception $e) {
                    echo $e;
                    $transaction->rollback();
                }
            } else {
                header('HTTP/1.0 401 Unauthorized');
                die("No Autorizado");
            }
        } else {
            header('HTTP/1.0 401 Unauthorized');
            die("No Autorizado");
        }
    }

}
