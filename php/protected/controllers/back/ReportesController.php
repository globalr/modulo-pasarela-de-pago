<?php

class ReportesController extends BackEndController {

    public function actionReportes() {
        //$criteria = new CDbCriteria;
        //$criteria->select = 'count(id)';
        //$criteria->group = 'APLICACION_id';     

        //$dataProvider = new CActiveDataProvider('transaccion',array('criteria'=>$criteria));
        //var_dump(Transaccion::model()->findAll($criteria));
//        $criteria = new CDbCriteria;
//        $criteria->select = 'COUNT(*) as cuenta';
//        $criteria->group = 'APLICACION_id';
        
         $sql = "SELECT nombre, COUNT(*) as transacciones 
                FROM Transaccion t
                join Aplicacion a
                where t.APLICACION_id = a.id
                group by APLICACION_id";
        $command = Yii::app()->db->createCommand($sql);
        $pieChart = $command->queryAll();
        
        $data =array(array('label','value'));
            foreach ($pieChart as $key=>$value) {
                $pieChart[$key]['transacciones'] = (int)$value['transacciones'];
                   $data[]=array($pieChart[$key]['nombre'],$pieChart[$key]['transacciones']); 
            }
           
        $this->render('reportes',array('pieChart'=>$data));
        
    }
    public function actionListas(){
        $model = Transaccion::model();
        $this->render('listas',array('model'=>$model->search()));
    }
    public function actionTransaccionesPorApp($id){

        $criteria=new CDbCriteria;
        $criteria->compare('APLICACION_id',$id);
        $model = new CActiveDataProvider(Transaccion::model(), array(
			'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=>6,
                ),
		));
        $this->render('listas',array('model'=>$model));
    }
    public function actionActualizarAplicacionAjax(){
        var_dump($_POST['empresaId']);
        $data=  Aplicacion::model()->findAllByAttributes(array('USUARIOS_id'=>$_POST['empresaId']));
        //echo $data;
        $data=CHtml::listData($data,'id','nombre');
        echo CHtml::tag('option',
                       array('value'=>''),CHtml::encode('todas'),true);
        foreach($data as $value=>$nombre)
        {
            echo CHtml::tag('option',
                       array('value'=>$value),CHtml::encode($nombre),true);
        }
    }
    
    public function actionAjaxTransacciones($empresaId,$aplicacionId,$tipoPago,$estado){
        
        $criteria = new CDbCriteria();
        $criteria->select='*';  
        $criteria->alias='t';
        if($empresaId!=''){
            $criteria->join = 'JOIN aplicacion ON aplicacion.id = t.APLICACION_id';
            $criteria->condition = "aplicacion.USUARIOS_id =  :USUARIOS_id";
            $criteria->params= array(":USUARIOS_id" => $empresaId);
        }
    
        if($aplicacionId!='')
            $criteria->compare('APLICACION_id', $aplicacionId);
        if($tipoPago!='')
            $criteria->compare('codigoPago', $tipoPago);
        if($estado!='')
            $criteria->compare('STATUS_PAYMENT_id', $estado);        
       
        //$criteria->addCondition('firstName',$this->firstName,'OR');
        //$criteria->addCondition('myfield = :myval');
        $model=  Transaccion::model()->findAll($criteria);
        
//$criteria->params = array(':myval' => 'value');
     /* $model=  Transaccion::model()->findAllByAttributes(array(
          'APLICACION_id'=>$aplicacionId,
          'codigoPago'=>$tipoPago,
          'STATUS_PAYMENT_id'=>$estado));*/
       $this->renderPartial('_gridTrans',array('model'=>$model) , false, true);
    }

}
?>
