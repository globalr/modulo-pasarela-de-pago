<?php

class ServicioController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    public $listaServicios = array();
    /**
     * @return array action filters
     */


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /*public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }*/

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
            $model=  Servicio::model()->findByPk ($id);
            $aplicacionesAsignadas =  ServicioHasAplicacion::model()->findAllByAttributes(array('servicio_id'=>$id));
            $recursorAsignados= ServicioHasRecurso::model()->findAllByAttributes(array('servicio_id'=>$id));
            //var_dump($aplicacionesAsignadas);
            $this->render('view', 
                    array(
                        'model' => $model,
                        'aplicacionesAsignadas'=>$aplicacionesAsignadas,
                        'recursosAsignados'=>$recursorAsignados)
                    );
    }
    
    public function actionAsignar($id){
        
        $model= Aplicacion::model()->findByPk($id);
        $servicios = Servicio::model();
        $this->render('asignar',array('model'=>$model,'servicios'=>$servicios));
    }   

    public function actionSeleccionar($id,$attribute){

        $servicios = Servicio::model();  
        $model = Aplicacion::model()->findByPk($attribute);  
        
        $relacion=ServicioHasAplicacion::model()->findByAttributes(array('servicio_id'=>$id,'aplicacion_id'=>$attribute));
        
        if($relacion)
                $relacion->delete();
        else{
                $relacion = new ServicioHasAplicacion;
                $relacion->aplicacion_id=$attribute;
                $relacion->servicio_id=$id;
                $relacion->fechaContratacion=  date('Y-m-d');
                $relacion->precio=$servicios->findByPk($id)->getAttribute('precio');
                $relacion->contratado =1;                
                $relacion->save();

        }
                
		$this->redirect('../asignar/'.$attribute); 
                
        
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Servicio;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Servicio'])) {
            $model->attributes = $_POST['Servicio'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Servicio'])) {
            $model->attributes = $_POST['Servicio'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
// we only allow deletion via POST request
            $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model=  Servicio::model();
        //$dataProvider = new CActiveDataProvider('Servicio');
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Servicio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Servicio']))
            $model->attributes = $_GET['Servicio'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Servicio::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'servicio-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
