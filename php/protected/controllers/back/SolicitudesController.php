<?php


class SolicitudesController extends BackEndController{
    
    public function actionIndex(){
        
        $this->render('index');
    }
    public function actionAdicionEmpresas(){
        $model=  Usuario::model()->findByAttributes(array(''));
        $this->render('adicionEmpresas',array('model'=>$model));
    }
    
    public function actionAdicionServicio(){
        $solicitudServicios= ServicioHasAplicacion::model()->with(array('aplicacion','servicio' => array('condition'=>'contratado=1')))->findAll();

        $this->render('adicionServicio',array('solicitudServicios'=>$solicitudServicios));
    }
    
    public function actionAjaxAprobarServicio($id,$opcion){
        $servicio =  ServicioHasAplicacion::model()->findByPk($id);
        
        $servicio->contratado=($opcion===2 ? 2 : 3);
        $servicio->save();
        
    }
    
    
    
    public function actionVerificarDeposito(){
        //codigoPago => 1=Tarjeta de Credito 2=deposito 3=transferencia
        //$solicitudDeposito= Transaccion::model()->findAllByAttributes(array('codigoPago'=>2));
        $solicitudDeposito= Transaccion::model()->findAllByAttributes(array(),$condition='(codigoPago=2 or codigoPago=3) and STATUS_PAYMENT_id=1');
        $this->render('verificarDeposito',array('solicitudDeposito'=>$solicitudDeposito));
    }
    
    public function actionDetalleDeposito($id){
        $deposito = Transaccion::model()->findByPk($id);
        $detalle = DetalleTransaccion::model()->findByAttributes(array('TRANSACCION_id'=>$id));
        if($deposito && $detalle)
            $this->render('detalleDeposito',array('deposito'=>$deposito,'detalle'=>$detalle));
        else
            $this->redirect (array('reportes/listas'));        
    }
    public function actionAjaxAprobarRechazar($observacion,$transaccionId,$accion){
        
        //ENUM('Procesando','Aprobada','Denegada','Tiempo Expirado','Error Interno')
        if($accion === 'aprobar')
            if(Transaccion::model()->updateByPk($transaccionId,array('STATUS_PAYMENT_id'=>2))){
                $log = new LogEstadosTransaccion;
                $log->TRANSACCION_id=$transaccionId;
                $log->statusTransaccion=2;
                $log->fecha= date('Y-m-d H:i:s');
                if($log->save()){
                    //Post::model()->updateAll($attributes,$condition,$params);
                    if(DetalleTransaccion::model()->updateAll(array('observaciones'=>$observacion),'TRANSACCION_id=:transaccionId',array(':transaccionId'=>$transaccionId)))               
                        Yii::app()->user->setFlash('info', '<strong>¡Correcto!</strong> el deposito se ha procesado exitosamente.'); 
                    else
                        Yii::app()->user->setFlash('info', '<strong>¡Correcto!</strong> se Cambio el estatus correctamente...');
                }else
                    Yii::app()->user->setFlash('info', '<strong>¡Error!</strong> no se pudo registrar el proceso');
            }else{
                Yii::app()->user->setFlash('info', '<strong>¡Error!</strong> no se realizó ningun cambio');
            }
                    
                  
        if($accion === 'rechazar')
            if(Transaccion::model()->updateByPk($transaccionId,array('STATUS_PAYMENT_id'=>3))){
                $log = new LogEstadosTransaccion;
                $log->TRANSACCION_id=$transaccionId;
                $log->statusTransaccion=3;
                $log->fecha= date('Y-m-d H:i:s');
                if($log->save()){
                    if(DetalleTransaccion::model()->updateAll(array('observaciones'=>$observacion),'TRANSACCION_id=:transaccionId',array(':transaccionId'=>$transaccionId)))               
                        Yii::app()->user->setFlash('info', '<strong>¡Correcto!</strong> el deposito se ha procesado exitosamente.');
                    else
                        Yii::app()->user->setFlash('info', '<strong>¡Correcto!</strong> se Cambio el estatus correctamente...');
                }else
                    Yii::app()->user->setFlash('info', '<strong>¡Error!</strong> no se pudo registrar el proceso');
            }else{
                Yii::app()->user->setFlash('info', '<strong>¡Error!</strong> no se realizó ningun cambio...');
            }
       

    }
}

?>
