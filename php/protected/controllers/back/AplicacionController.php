<?php

class AplicacionController extends BackEndController
{
	
    public function actions() {
        return array(
            'coco' => array(
                'class' => 'CocoAction',
            ),
        );
    }
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAsignar($id)
	{
                
		$model=new Aplicacion;
                $empresa=  Usuario::model()->findByPk($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);	
                //yii::app()->end();
		if(isset($_POST['Aplicacion'])and isset($_POST['Usuario']))
		{

			$model->attributes=$_POST['Aplicacion'];
                        $model->USUARIOS_id=$_POST['Usuario']['id'];                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('asignar',array(
			'model'=>$model,'empresa'=>$empresa,
		));
	}
       public function actionCreate() {
        $empresa = Usuario::model();
        $model = new Aplicacion;
        if (isset($_POST['Aplicacion']) and isset($_POST['Usuario'])) {

            $model->attributes = $_POST['Aplicacion'];
            $model->USUARIOS_id = $_POST['Usuario']['razonSocial'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model, 'empresa' => $empresa,
        ));
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                
		$model=$this->loadModel($id);
                $empresa=$model->uSUARIOS;
                //$empresa=  Usuario::model()->findByPk($model->USUARIOS_id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Aplicacion']))
		{
			$model->attributes=$_POST['Aplicacion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,'empresa'=>$empresa,
		));
	}


	public function actionDelete($id)
	{
            $model= Aplicacion::model()->findByPk($id);
            $transacciones = Transaccion::model()->findAllByAttributes(array('APLICACION_id'=>$id));
            $servicios = ServicioHasAplicacion::model()->findAllByAttributes(array('aplicacion_id'=>$id));
            
            $this->render('delete',array('model'=>$model,'transacciones'=>$transacciones,'servicios'=>$servicios));
			               
        }

        public function actionDeleteAplicacion($id){            
            if($this->loadModel($id)->delete())
               $this->redirect('../index');
        }
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{            
		$model= Aplicacion::model();
                
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Aplicacion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Aplicacion']))
			$model->attributes=$_GET['Aplicacion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionCaracteristicas($id){
            $model= Aplicacion::model()->findByPk($id);
            $this->render('caracteristicas',array('model'=>$model));
        }


        	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Aplicacion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Aplicacion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Aplicacion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='aplicacion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
