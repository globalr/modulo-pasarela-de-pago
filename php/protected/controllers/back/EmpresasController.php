<?php

class EmpresasController extends BackEndController {

    public function actionIndex() {
        $this->render('index');
    }

    public function actionServiciosContratados() {
        $servicios = ServicioHasAplicacion::model()->with(
                        array('aplicacion' => array('condition' => 'USUARIOS_id = :id_empresa', 'params' => array(':id_empresa' => yii::app()->user->id))))->findAll();


        $this->render('servicios/serviciosContratados', array('servicios' => $servicios));
    }

    public function actionDescargaRecursos() {
        $recursos = ServicioHasRecurso::model()->with(
                        array(
                            'servicio.servicioHasAplicacions.aplicacion' => array(
                                'condition' => 'USUARIOS_id = :id_empresa',
                                'params' => array(':id_empresa' => yii::app()->user->id)), 'recurso'
                ))->findAll();

        $this->render('descargaRecursos', array('recursos' => $recursos));
    }


   public function actionCrearAplicacion(){
        $model = new Aplicacion;
        if (isset($_POST['Aplicacion'])) {
            $model->attributes = $_POST['Aplicacion'];
            if ($model->save()){
                Yii::app()->user->setFlash('info', '<strong>¡Solicitud enviada con exito!.</strong> 
            se ha enviado una solicitud al administracion para que proceda a verificar y realizar 
            la adicion de la Aplicacion.');
                $this->render('crearAplicacion', array('model' => $model));
            }
        }

        $this->render('crearAplicacion', array('model' => $model));
   }

public function actionEstadoTransaccion(){

    $transacciones=null;
    $aplicacion = Aplicacion::model();
    $this->render('estadoTransaccion',array('transacciones'=>$transacciones,'aplicacion'=>$aplicacion));
}
    
     public function actionAjaxTransacciones($aplicacion) {
        
        $transacciones = Transaccion::model()->findAllByAttributes(array('APLICACION_id' =>$aplicacion));
            
        $this->renderPartial('_gridTrans',array('transacciones'=>$transacciones) , false, true);
    }
    
    
    public function actionGuiaIntegracion() {
        $this->render('integracion/guiaIntegracion');
    }
    
        public function actionReportes() {

         $sql = "SELECT nombre, COUNT(*) as transacciones 
                FROM Transaccion t
                join Aplicacion a
                where t.APLICACION_id = a.id and a.USUARIOS_id =:empresa
                group by APLICACION_id";
            $command = Yii::app()->db->createCommand($sql);
        
        $command->bindValue(":empresa",yii::app()->user->id,PDO::PARAM_STR);
        $pieChart = $command->queryAll();        
        $data =array(array('label','value'));
            foreach ($pieChart as $key=>$value) {
                $pieChart[$key]['transacciones'] = (int)$value['transacciones'];
                   $data[]=array($pieChart[$key]['nombre'],$pieChart[$key]['transacciones']); 
            }
           
        $this->render('reportes',array('pieChart'=>$data));
        
    }
    
    public function actionAplicacionesAsociadas(){
        $aplicacionesAsignadas = Aplicacion::model()->findAllByAttributes(array('USUARIOS_id'=>yii::app()->user->id));
        $this->render('aplicacionesAsociadas',array('aplicacionesAsignadas'=>$aplicacionesAsignadas));
        
    }
    
    
    public function actionlistaServicios(){
        $servicios= Servicio::model()->findAll();
        $this->render('servicios/listaServicios',array('servicios'=>$servicios));
    }
    
    
    public function actionAsignarServicio($idServicio){
        $serv=  Servicio::model()->findByPk($idServicio);
        //$aplicaciones= Aplicacion::model();
        $aplicaciones=Aplicacion::model()->findAllByAttributes(array('USUARIOS_id' => yii::app()->user->id));
        $this->render('servicios/asignarServicio',array('serv'=>$serv,'aplicaciones'=>$aplicaciones));
    }
    
    public function actionAjaxSolicitarServicio($aplicacion,$servicio,$precio){
        
        $servicioNuevo = new ServicioHasAplicacion;
        $servicioNuevo->aplicacion_id=$aplicacion;
        $servicioNuevo->servicio_id=$servicio;        
        $servicioNuevo->fechaContratacion=date('Y-m-d H:i:s');
        $servicioNuevo->precio=$precio;
        if($servicioNuevo->save())
            Yii::app()->user->setFlash('info', '<strong>¡Solicitud enviada con exito!.</strong> 
            se ha enviado una solicitud al administracion para que proceda a verificar 
            la contratacion del servicio.');
        else
            Yii::app()->user->setFlash('info', '<strong>¡Error!.</strong> por favor contacte al administrador.');
    }
    
    public function actionCaracteristicas($id){
            $model= Aplicacion::model()->findByPk($id);
            $this->render('caracteristicas',array('model'=>$model));
        }
        
        public function actionPerfil($id){
            
            $model = Usuario::model()->findByPk($id);
            if($model){
                $model->password= decrypt($model->password);
                $this->render('perfil',array('model'=>$model));
            }
        }
        
        public function actionDetalleAplicacion($id){
            $model=  Aplicacion::model()->findByPk($id);
            $this->render('detalleAplicacion',array('model'=>$model));
        }
    
}
