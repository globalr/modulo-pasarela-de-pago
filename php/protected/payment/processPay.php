<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Global Payment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->


        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="../../images/globalr.png">

        <!-- scripts -->    
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/js/jquery.js"></script>    
    
    </head>

    <body> 
        <!-- Modal -->
        <div id="waitModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Procesando pago</h3>
            </div>
            <div class="modal-body">	  
                <div class="paragraphs">
                    <div class="row">
                        <div class="span4">
                            <img style="float:left" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/images/ajax-loader.gif" alt="cargando">      
                            <p>Por favor espere…</p>
                            <div style="clear:both"></div>
                        </div>    
                    </div>
                </div>	  	  	  
            </div>
        </div>  
        <script type="text/javascript">
            try {
                $('#waitModal').modal('show');
                $(document).ready(function() {
                    $('#waitModal').modal('hide');
                });
            } catch (e) {
            }
        </script>          
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand pull-right" style="color: #999999" href="#">Global Payment</a>
                    <ul class="nav">
                        <li class=""></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container" style="margin-top: 15px">

            <?php
            
            include Yii::app()->basePath.'/payment/lib/globalr/payment/coredev.php';
            include Yii::app()->basePath.'/payment/lib/globalr/dbtools/util.php';
            include Yii::app()->basePath.'/payment/lib/globalr/mail/globalMail.php';
            
                     
            $adminMsg = "";
            $responseCode;
            $sucess = -1; // variable bandera, 0 si todo va correctamente

            $errorEntrada = false;
// validar que los campos ingresados por el usuario sean validos
            if (ctype_digit($_POST['cardNumber']) != 1) {
                $errorEntrada = true;
                $adminMsg = "Por favor ingrese solo números: Número de tarjeta";
            } else {
                if (ctype_digit($_POST['cardExpiryDate']) != 1) {
                    $errorEntrada = true;
                    $adminMsg = "Por favor ingrese solo números: Fecha de vencimiento";
                } else {
                    if (ctype_digit($_POST['authorizerId']) != 1) {
                        $errorEntrada = true;
                        $adminMsg = "Por favor ingrese solo números: Tipo de tarjeta";
                    } else {
                        if (ctype_digit($_POST['cardSecurityCode']) != 1) {
                            $errorEntrada = true;
                            $adminMsg = "Por favor ingrese solo números: Código de seguridad";
                        } else {
                            if (ctype_digit($_POST['customerId']) != 1) {
                                $errorEntrada = true;
                                $adminMsg = "Por favor ingrese solo números: Cédula o RIF del Cliente";
                            }
                        }
                    }
                }
            }

            if (!$errorEntrada) {                
                
                $orderId = $_SESSION['idTransaccion']; // Code of the order (in store) **. no requerido                 
                $app_id = $_SESSION['origen'];
                $monto = $_SESSION['totalfinal']; //$_POST['monto']; //Total price of the purchase (in cents) *** .                 
                $codigo = $_SESSION['codigo'];

                echo "orden: $orderId<br/>";
                echo "app: $app_id<br/>";
                echo "monto: $monto<br/>";
                echo "codigo: $codigo<br/>";

                $criteria = new CDbCriteria;
                $criteria->select='*';  // only select the 'title' column
                $criteria->condition='codigoTransaccion=:codigoTransaccion and APLICACION_id=:APLICACION_id';
                $criteria->params=array(':codigoTransaccion'=>$orderId,'APLICACION_id'=>$app_id);
                $transacciones=  Transaccion::model()->findAll($criteria);

                $success=null;

                
                if ($transacciones) {
                    //var_dump($transacciones);
                    // la trasacción ya existe, mostrar mensaje al usuario e interrumpir el flujo
                    $userMsg = "Disculpe, la transacción que intenta procesar ya esta registrada en el sistema";
                } else {

                    $transaccion = new Transaccion;
                    $transaccion->codigoTransaccion=$orderId;
                    $transaccion->descripcion='insertada en processpay';
                    $transaccion->monto=$monto;
                    $transaccion->tiempoTransaccion=$_POST['tiempo'];
                    $transaccion->fechaCreacion=date('Y-m-d H:i:s');
                    $transaccion->APLICACION_id=$app_id;
                    $transaccion->codigoPago=1;
                    $transaccion->STATUS_PAYMENT_id=1;
                    
                    if($transaccion->save()){
                                        $historicoTransaccion =  new LogEstadosTransaccion;
                                        $historicoTransaccion->TRANSACCION_id=$transaccion->id;
                                        $historicoTransaccion->statusTransaccion= $transaccion->STATUS_PAYMENT_id;
                                        $historicoTransaccion->fecha=date('Y-m-d H:i:s');
                                        $historicoTransaccion->save();   
                    }else{
                        
                        echo'no guardo l transaccion';
                    }
                    // START PAYMENT
                    // obtener el cliente nusoap, conexion con el web service 
                    // de sitef
                    //$conexion = obtenerConexion();
                    //$err = $conexion->getError();
                    
                    $err=false;//para simular
                    if ($err) {
                        $adminMsg = $err;
                        $userMsg = "Error de conexión, su transacci&oacute;n no pudo ser procesada";
                    } else {
                        // verificar el tiempo
                        $hoy = strtotime(date('Y-m-d H:i:s'));
                        if ((isset($_POST['admin']) == false) && $hoy > $_SESSION['tiempo']) {
                            $userMsg = "Disculpe, el tiempo de pago de la transacción se ha agotado";
                        } else {
                            // configurar el objeto para solicitar el pago                
                           // $objetoPago = obtenerObjetoPago($conexion);
                            
                            $objetoPago=true;//para simular
                            if ($objetoPago) {

                                //REQUERIDO. IMPORTANT: When using IATA this field is expected the sum (amount + departureTax)

                                $merchantUSN = $orderId; //Sequential number of the order (in store) *** . no requerido		
                                // BEGIN TRANSACTION
                                $transactionResponse = "-1";
                                for ($i = 1; $i <= 3; $i++) {
                                    $transactionResponse = obtenerNitTimed($objetoPago, $monto, $merchantUSN, $orderId);
                                    echo "<pre>";
                                    echo "transactionResponse:";
                                    print_r($transactionResponse);
                                    echo "</pre>";
                                    if ($transactionResponse != "-1") {
                                        break;
                                    }
                                }

                                if ($transactionResponse == "-1") { //timeout en beginTransaction
                                    // enviar alarma a administrador
                                    // registrar en logs
                                    $adminMsg = "Error de comunicacion: timeout en beginTrasaction";
                                    $userMsg = "Error de comunicacion: su transacci&oacute; no pudo ser procesada";
                                } else {
                                    $nit = $transactionResponse['transactionResponse']['nit'];
                                    // parametros que vienen del sistema que solicita el pago
                                    $customerId = $_POST['customerId'];
                                    $cardNumber = $_POST['cardNumber'];
                                    if ($nit) {
                                        sleep(5);
                                        // ACTUALIZAR EL ESTADO DE LA TRANSACCION EN LA TABLA TARNSACCION 
                                        //INSERTAR UNA NUEVA FILA EN EL HISTORICO DE ESTADOS.
                                        $transaccion= Transaccion::model()->findByAttributes(array('codigoTransaccion'=>$orderId));
                                        $transaccion->STATUS_PAYMENT_id=2;
                                        if($transaccion->save()){
                                            $historicoTransaccion =  new LogEstadosTransaccion;
                                            $historicoTransaccion->TRANSACCION_id=$transaccion->id;
                                            $historicoTransaccion->statusTransaccion= $transaccion->STATUS_PAYMENT_id;
                                            $historicoTransaccion->fecha=date('Y-m-d H:i:s');
                                            $historicoTransaccion->save(); 
                                        }
                                        /*$testat = $transactionResponse['transactionResponse']['transactionStatus'];
                                        $ipCliente = $_SERVER['REMOTE_ADDR'];
                                        $dbtool->consultar("INSERT INTO transaccion(monto, cliente_id, estatus, fhmod, fcrea, 
                                                                aplicacion_id, transaccion_id, codigo, nit, cliente_ip,
                                                                card_number) 
                                                                values($monto, '$customerId','$testat', now(), now(),
                                                                $app_id, $orderId, '$codigo', '$nit', '$ipCliente',
                                                                    '$cardNumber')");
                                        */
                                        /*$authorizerId = $_POST['authorizerId'];
                                        $autoConfirmation = 'true';
                                        $cardExpiryDate = $_POST['cardExpiryDate'];
                                        $cardSecurityCode = $_POST['cardSecurityCode'];
                                        $extraField = '';
                                        $installmentType = '4';
                                        $installments = '1';

                                        $solicitud = obtenerSolicitudPago($authorizerId, $autoConfirmation, $cardExpiryDate, $cardNumber, $cardSecurityCode, $customerId, $extraField, $installmentType, $installments, $nit);
                                        */
                                        $solicitud=true;//para simulacion
                                        
                                        $resultado = pagarTimeout($objetoPago, $solicitud);
                                        echo "<pre>";
                                        echo "pago:";
                                        print_r($resultado);
                                        echo "</pre>";
                                        
                                        if ($resultado == -1) {
                                            // timeout en dopayment	
                                            for ($i = 1; $i <= 3; $i++) {
                                                $resultado = obtenerEstatusTransaccionTimeOut($objetoPago, $nit);
                                                //print_r($resultado);
                                                if ($resultado != "-1") {
                                                    break;
                                                }
                                            }
                                        }

                                        // Aca se debe evaluar el resultado de la transaccion y si es necesario
                                        // arreglar el error que se está produciendo
                                        // o mostrar la salida que indica al usuario el tipo de error
                                        // que se esta presentado, en caso de sea un problema
                                        // relativo a datos incorrectos
                                        $responseCode = $resultado['paymentResponse']['responseCode'];
                                        $responseStatus = $resultado['paymentResponse']['transactionStatus'];
                                        $adminMsg = $resultado['paymentResponse']['message'];

                                        sleep(5); // sleep(10);
                                        
                                        //GUARDO EN LA BASE DE DATOS SI EL RESULTADO DE LA TRANSACCION ES COMPLETADA O NEGADA
                                        //GUARDO EN EL HISTORICO EL ESTADO ANTERIOR DE LA TRANSACCION
                                        
                                        $transaccion= Transaccion::model()->findByAttributes(array('codigoTransaccion'=>$orderId));
                                        $transaccion->STATUS_PAYMENT_id=3;
                                        if($transaccion->save()){
                                            $historicoTransaccion =  new LogEstadosTransaccion;
                                            $historicoTransaccion->TRANSACCION_id=$transaccion->id;
                                            $historicoTransaccion->statusTransaccion= $transaccion->STATUS_PAYMENT_id;
                                            $historicoTransaccion->fecha=date('Y-m-d H:i:s');
                                            $historicoTransaccion->save(); 
                                        }
                                        
                                        //$dbtool->consultar("UPDATE transaccion set estatus='$responseStatus', fhmod=now() where nit='$nit';");
                                        
                                        if ($responseCode == 0) {
                                            if ($responseStatus == 'CON') {
                                                // transaccion regostrada correctamente y en espera de confirmacion
                                                $success = 1;
                                                // si la transaccion fue creada se guarda en base de datos
                                                // el estatus en este momento debe ser NOV
                                                $userMsg = "Transacción Exitosa. Para completar el proceso debe continuar presionando el boton Finalizar";
                                            } else {
                                                //comunicación correcta pero la transaccion no se llevo a cabo correctamente
                                                $success = -1;
                                                $userMsg = "Error al procesar la transacción";
                                            }
                                        } else {
                                            switch ($responseCode) {
                                                case 1:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 2:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 3:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 4:
                                                    $userMsg = "Error al procesar la transacción: Tipo de tarjeta inválido";
                                                    break;
                                                case 5:
                                                    $userMsg = "Error al procesar la transacción: Tipo de tarjeta inválido";
                                                    break;
                                                case 6:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 7:
                                                    $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                    break;
                                                case 8:
                                                    $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                    break;
                                                case 9:
                                                    $userMsg = "Error al procesar la transacción: Fecha de vencimiento ha expirado";
                                                    break;
                                                case 10:
                                                    $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                    break;
                                                case 11:
                                                    $userMsg = "Error al procesar la transacción: Fecha de vencimiento incorrecta";
                                                    break;
                                                case 12:
                                                    $userMsg = "(12) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                    break;
                                                case 13:
                                                    $userMsg = "(13) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                    break;
                                                case 14:
                                                    $userMsg = "(14) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                    break;
                                                case 15:
                                                    $userMsg = "(15) Error al procesar la transacción: Número de tarjeta incorrecto";

                                                    break;
                                                case 16:
                                                    $userMsg = "(16) Error al procesar la transacción: Número de tarjeta incorrecto";

                                                    break;
                                                case 17:
                                                    $userMsg = "(17) Error al procesar la transacción: Número de tarjeta incorrecto";
                                                    echo "asigna: $userMsg";
                                                    break;
                                                case 18:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 19:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 20:
                                                    $userMsg = "Error al procesar la transacción: Id de cliente incorrecto";
                                                    break;
                                                case 22:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 23:
                                                    $userMsg = "Error al procesar la transacción";
                                                    break;
                                                case 30:
                                                    $userMsg = "Error al procesar la transacción: La transacción ya está finalizada";
                                                    break;
                                                case 131:
                                                    $userMsg = "Error al procesar la transacción: Error de comunicación";
                                                    break;
                                                case 150:
                                                    $userMsg = "Error al procesar la transacción: Error de comunicación";
                                                    break;
                                                case 255:
                                                    $userMsg = "Error al procesar la transacción: operaci&oacute;n rechazada";
                                                    break;
                                            }
                                        }
                                        
                                        // si el response code es alguno de los que implica
                                        // que el proceso trabaja de forma erronea, es decir
                                        // uno de los siguientes:
                                        // 255, 150, 131, 6 , 1, 2, 3, 4, 5
                                        $error_email = array(255, 150, 131, 6, 1, 2, 3, 4, 5);
                                        
                                        //ENVIAR EMAIL CON INFORMACION DE TRANSACCIONES RECHAZADAS
                                        /*
                                        if (in_array($responseCode, $error_email)) {
                                            // consultar las reservaciones
                                            $consultaTransaccionesRechazadas = $dbtool->consultar("SELECT count(id) as total FROM transaccion WHERE estatus!='CON' and fcrea >= date(now()) and aplicacion_id=$app_id");
                                            $transaccionesRechazadas = mysql_fetch_assoc($consultaTransaccionesRechazadas);
                                            echo "DEV --> transacciones reprobadas:" . $transaccionesRechazadas['total'];
                                            if ($transaccionesRechazadas['total'] > 5) {
                                                $mailBody = "<p>Transacciones rechazadas hoy: " . $transaccionesRechazadas['total']."</p>";
                                                // la trasacción ya existe, mostrar mensaje al usuario e interrumpir el flujo
                                                enviarCorreo("samuel.olaechea@globalr.net", "Alarma de transacciones rechazadas", $mailBody);
                                            }
                                        }
                                        */
                                    } else {
                                        $adminMsg = "Error de comunicacion: (no nit)";
                                        $userMsg = "Error de comunicacion: (no nit)";
                                    }
                                }
                            } else {
                                $adminMsg = $conexion->getError();
                                $userMsg = "Error de comunicacion. (no proxy): ";
                            }
                        }
                    }
                }
            }

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //http://curiasport.com
            //http://pure-inlet-5406.herokuapp.com
            $server = $_POST['urlServer']; //"http://localhost:8080/curia";
            $urlActualizarEstatusTransaccion = $_POST['urlActualizarEstatus'];
            //$success=-1; //PARA SIMULAR
            if ($success == 1) {?>
                        <form action="<?php echo $urlActualizarEstatusTransaccion?>" method="post" name="formulario" >
                        <input type="hidden" name="idTransaccion" value="<?php echo $orderId?>">
                        <input type="hidden" name="nit" value="<?php echo $nit?>">                       
                        </form>     
                        <script>document.formulario.submit();</script>                    
             <?php               
            } else {
                echo "<hr/>$adminMsg";
                echo "<br/>$userMsg<hr/>";
                ?>
                    <form method="post" action="pay">
                    <input type="hidden" name="redirectTo"             value="<?php echo $_POST['redirectTo']; ?>"/>		
                    <input type="hidden" name="logo"                   value="<?php echo $_POST['logo']; ?>"/>	
                    <input type="hidden" name="css"                    value="<?php echo $_POST['css']; ?>"/>	
                    <input type="hidden" name="urlActualizarEstatus"   value="<?php echo $_POST['urlActualizarEstatus']; ?>"/>	
                    <input type="hidden" name="cancelUrl"              value="<?php echo $_POST['cancelUrl']; ?>"/>	
                    <input type="hidden" name="tiempo"                 value="<?php echo $_POST['tiempo']; ?>"/>	
                    <input type="hidden" name="idTransaccion"          value="<?php echo $_POST['idTransaccion']; ?>"/>
                    <input type="hidden" name="fromerr"                value="1"/>                    
                    <input type="hidden" name="urlServer"              value="<?php echo $_POST['urlServer']; ?>"/>
                    <?php
                    if (isset($_POST['admin']) == true) {
                        echo "<input type=\"hidden\" name=\"admin\" value=\"true\"/>";
                    }
                    ?>                     
                    <button class="btn btn-large" type="submit" tabindex="9">Atras</button>		
                </form>
            <?php } ?>	
        </div> <!-- /container -->



        <div class="footer navbar navbar-fixed-bottom" style="background-color: #1b1b1b;">
            <div class="container">
                <p>Copyright 2012 Global Payment - <a href="http://www.globalr.net" target="_blank">Global Resources</a></p>
                <p>Caracas - San Crist&oacute;bal </p>
            </div>
        </div>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->   
    </body>
</html>