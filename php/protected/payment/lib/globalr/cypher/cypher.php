<?php

function encrypt($str) {
    $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

    // How do you do 256-bit AES encryption in PHP vs. 128-bit AES encryption???
    // The answer is:  Give it a key that's 32 bytes long as opposed to 16 bytes long.
    // For example:
    $key128 = '1234567890123456';
    // Here's our 128-bit IV which is used for both 256-bit and 128-bit keys.
    $iv = 'abcdefghijklmnop';

    if ($str != null && strlen($str) > 16) { 
        $str =  substring($str, 0, 15);
    }
    $str = padString($str);

    mcrypt_generic_init($td, $key128, $iv);
    $encrypted = mcrypt_generic($td, $str);

    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);

    return base64_encode($encrypted);
}

function padString($stringToPad) {
    $ciclos = 0;
    $ciclos = ceil(strlen($stringToPad)/ 16.0);
    $max = $ciclos * 16;
    $agregar = 0;
    if ($max > 0) {
        $agregar = $max - strlen($stringToPad);
    } else {
        $agregar = 16 - strlen($stringToPad);
    }
    $relleno = "________________"; //16 characters string
    $stringToPad = $stringToPad . substr($relleno, 0, $agregar);
    return $stringToPad;
}

function decrypt($code) {

    $key128 = '1234567890123456';
    $iv = "abcdefghijklmnop";
    $code = base64_decode($code);


    $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

    mcrypt_generic_init($td, $key128, $iv);
    $decrypted = mdecrypt_generic($td, $code);

    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    return prepareInput($decrypted);
}

function prepareInput($value) {
    return str_replace("_", "", $value);
}

?>