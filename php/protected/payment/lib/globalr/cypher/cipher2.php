<?php

function encrypt($str, $key) {   
	//$td = mcrypt_module_open("rijndael-128", "", "cbc", "fedcba9876543210");
	$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
	
	// How do you do 256-bit AES encryption in PHP vs. 128-bit AES encryption???
	// The answer is:  Give it a key that's 32 bytes long as opposed to 16 bytes long.
	// For example:
	$key256 = '12345678901234561234567890123456';
	$key128 = '1234567890123456';	
	// Here's our 128-bit IV which is used for both 256-bit and 128-bit keys.
	$iv =  'abcdefghijklmnop';
	
	//$key128 = hex2bin($key128); 
	//$iv = hex2bin($iv); 
	
	
	mcrypt_generic_init($td, $key128, $iv);//$td, $key, "fedcba9876543210");
	$encrypted = mcrypt_generic($td, $str);

	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);

	return base64_encode($encrypted);//bin2hex($encrypted);
}


function decrypt($code, $key) {
  
  	//$key = hex2bin($key);
  	$key128 = '1234567890123456';	
  	$iv =  "abcdefghijklmnop";
    
  	$code = base64_decode($code);//hex2bin($code);


	$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

	//$key128 = hex2bin($key128); 
	//$iv = base64_decode($iv); 

  	mcrypt_generic_init($td, $key128, $iv);
  	$decrypted = mdecrypt_generic($td, $code);

  	mcrypt_generic_deinit($td);
  	mcrypt_module_close($td);

  	//return utf8_encode(trim($decrypted));
  	//return base64_decode($str);
  	return $decrypted;
}

/*function hex2bin($hexdata) {
  $bindata = "";
  for ($i = 0; $i < strlen($hexdata); $i += 2) {
    $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
  }
  return $bindata;
}

function encripta2(){
	// set keys
	$secret_key   = "1234567890123456";
	$iv           = "abcdefghijklmnop";
	$plaintext = "hola mundo";
	// encryption
	$enc = mcrypt_encrypt(MCRYPT_RIJNDAEL_128,
	          $secret_key, $plaintext, MCRYPT_MODE_CBC, $iv);
	// base64 encoding
	$enc64 = base64_encode($enc);
	echo $enc64 . "\n";
}*/
function ejemplo1(){
	$resultado = encrypt("1234567890123456", "1234567890123456");
	echo "pasa: $resultado</br>";
	$resultado2 = decrypt($resultado, "1234567890123456");
	echo "era: $resultado2</br>";
}

ejemplo1();
/*echo "<br/><br/><br/>";
ejemplo2();
echo "<br/><br/><br/>";
ejemplo4();*/
?>