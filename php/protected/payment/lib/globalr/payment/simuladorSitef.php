<?php

/**
 * para simular las respuestas de los servicios web de sitef
 */
//nit               Identifier of the transaction on e-SiTef (encrypted) = 64 A
//transactionStatus Status of the transaction on e-Sitef. See Table 10 =3A
//responseCode      Numeric code of response of the beginTransaction method. <=4 N See Table 3.
//message           Response message of the beginTransaction method. “OK” for <=500 A “success”
//extraField        Unused N/A

function simulador_beginTransaction() {
    $result = array(
        "transactionResponse" => array(
            "nit" => generateRandomString(),
            "transactionStatus" => "NOV",
            "responseCode" => 0,
            "message" => "transaccion generada por globalr desarrollo",
            "extraField" => "",
        )
    );
    return $result;
}

function simulador_doPayment() {
    $randomResult = rand(0, 1);
    $result = null;
    if ($randomResult == 0) {
        $result = successPay();
    } else {
        $result = errorPay();
    }
    return $result;
}

function successPay() {
    $result = array(
        "paymentResponse" => array(
            "responseCode" => 0,
            "transactionStatus" => "CON",
            "message" => "transaccion generada por globalr desarrollo",
        )
    );
    return $result;
}

//INV Transaction wasn’t created successfully; the store probably
//PPC Pending confirmation PPC Transaction authorized and awaiting/pending confirmation
//PPN Payment cancelled PPN Unconfirmed pending payment (cancelled)
//CON Payment OK CON Payment confirmed by the financial institution.
//NEG Payment denied NEG Payment denied by financial institution.
//CAN Payment cancelled CAN Payment cancelled due to communication failure with SiTef
//ERR Error ERR A communication error occurred. Try sending the request again.
//BLQ Blocked BLQ The transaction is blocked after many attempts of doPayment calling.

function errorPay() {
    $result = array(
        "paymentResponse" => array(
            "responseCode" => 5,
            "transactionStatus" => "NEG",
            "message" => "transaccion generada por globalr desarrollo",
        )
    );
    return $result;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

?>
