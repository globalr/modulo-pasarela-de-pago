<?php
// local
require_once('lib/nusoap.php');
require_once('lib/globalr/payment/simuladorSitef.php');
//
//produccion
//require_once('../../../dev-payment/lib/nusoap.php');
//require_once('simuladorSitef.php');
/*
 * abre la conexion con esitef y retorna un objeto que representa dicha conexion 
 */

/*
  DATOS DE PROCUCCION
  merchant id: J401260411
  merchant key: CB91994D17B1EE82598777EE9335B45467AFFDFD583CF50CD2E2A23AC28BD079
  url: https://esitef.softwareexpress.com.br/e-sitef/Payment2?wsdl
 */

function obtenerConexion() {
    //$client = new nusoap_client("https://esitef-homologacao.softwareexpress.com.br/e-sitef-hml/Payment2?wsdl", true, false, false, false, false, 0, 110);
    //$client = new nusoap_client("https://esitef-homologacao.softwareexpress.com.br/e-sitef/Payment2?wsdl", true, false, false, false, false, 0, 150);	
    //$client = new nusoap_client("https://esitef-homologacao.softwareexpress.com.br/e-sitef/Payment2?wsdl", true);
    $err = $client->getError();
    if ($err) {
        return null;
    }
    return $client;
}

/* retorna el objeto que sera usado para realizar las operaciones
  o null en caso de algun error
 */

function obtenerObjetoPago($conexion) {
    return $conexion->getProxy();
}

/*
 * preparar el pago especificando los datos de la tarjeta, de la transaccion, el nit del objeto de pago creado
 *
 * authorizerId: Code of the authorizer on e-Sitef. REQUERIDO. 1 visa, 2 master, 3 american
 * autoConfirmation: If true, the payment will be approved and true confirmed. REQUERIDO. If false, the payment will be false  kept pending confirmation. The operation 
 * 		     confirmPayment is the next step in this case. 
 *
 * cardExpiryDate: Expiration date in MMYY format MMAA. REQUERIDO
 * cardNumber : The number of the credit card. REQUERIDO
 * cardSecurityCode: Security Code (CAV2,CID, CVV2 or <= 5 N CVC2). REQUERIDO
 * customerId: Buyer’s identity document. no requerido.
 * extraField: Used for the departureTax (DEPARTURETAX field) and (FIRSTINSTALLMENT) (in cents).
 * 	       DEPARTURETAX mandatory, if installmentType equals 6 or 7 FIRSTINSTALLMENT is optional
 *
 * installmentType : Type of installment funding. REQUERIDO. value=3 is with interest. value=4 without interest. value=6 is with interest (IATA). value=7 is without interest (IATA)
 * installments : Number of installments. REQUERIDO
 * nit : Code of the order (in store) **. REQUERIDO  
 */

function obtenerSolicitudPago($authorizerId, $autoConfirmation, $cardExpiryDate, $cardNumber, $cardSecurityCode, $customerId, $extraField, $installmentType, $installments, $nit) {
    $paymentRequest = array('paymentRequest' => array
            (
            'authorizerId' => $authorizerId,
            'autoConfirmation' => $autoConfirmation,
            'cardExpiryDate' => $cardExpiryDate,
            'cardNumber' => $cardNumber,
            'cardSecurityCode' => $cardSecurityCode,
            'customerId' => $customerId,
            'extraField' => $extraField,
            'installmentType' => $installmentType,
            'installments' => $installments,
            'nit' => $nit
            ));
    return $paymentRequest;
}

function pagarTimeout($objetoPago, $paymentRequest) {
    if (true) {
        // correcto o incorrecto de forma aleatorioa
        //$result = simulador_doPayment();
        // pagar siempre correctamente
        $result = successPay();
        
        // pagar siempre con error
        //$result = errorPay();
    } else {
        $start = time(); // starting the timer
        $result = $objetoPago->doPayment($paymentRequest);
        $timing = time() - $start; // calculating the transaction time
        if ($timing >= 100 && $result == "") {
            // timeout
            $result = "-1";
        }
    }
    return $result;
}

function obtenerEstatusTransaccionTimeOut($objetoPago, $nit) {
    $status = "-1";
    $merchantKey = '586299377837BB813F22E5F538D5A00F9730CBEC048493B0';
    $getStatusRequest = array(
        'nit' => $nit,
        'merchantKey' => $merchantKey
    );

    $timing = 0;
    $start = time(); // iniciar el timer		
    $statusResponse = $objetoPago->getStatus($getStatusRequest);
    $timing = time() - $start;
    if ($timing >= 100 && $result == "") {
        // timeout
        return "-1";
    }
    return $statusResponse;
}

/*
 * obtener el Nit a partir de los datos del pago y nuestra cuenta
 * El nit es: Identifier of the transaction on e-SiTef (encrypted) 
 *
 * merchantUSN: Sequential number of the order (in store) *** . no requerido
 * monto: Total price of the purchase (in cents) *** . REQUERIDO. IMPORTANT: When using IATA this field is expected the sum (amount + departureTax)
 * merchantUSN: Sequential number of the order (in store) *** . no requerido  
 * orderId : Code of the order (in store) **. no requerido 
 */

function obtenerNitTimed($payment, $monto, $merchantUSN, $orderId) {
    $montoCents = $monto * 100;
    $transactionRequest = array('transactionRequest' => array
            (
            'amount' => $montoCents, //'099',             
            'merchantId' => 'globalr',
            'merchantUSN' => $merchantUSN,
            'orderId' => $orderId
            ));
    if (true) {
        //DESARROLLO
        $transactionResponse = simulador_beginTransaction();
    } else {


        $timing = 0;
        $start = time(); // iniciar el timer		
        $transactionResponse = $payment->beginTransaction($transactionRequest);
        $timing = time() - $start;

        if ($timing >= 100 && $transactionResponse == "") {
            // timeout
            return "-1";
        }
    }
    return $transactionResponse;
}

?>