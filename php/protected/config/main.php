<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
    Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
        //'theme'=>'admin', 
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Payment',
        'sourceLanguage' => 'en',
        'language'=>'es',
        'preload' => array(
            'log',
            'bootstrap'
        ),

    // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'application.extensions.coco.*',
                'application.payment.*',//OJO ESTO ES DE PAYMENT
	),
        
    // SEPARANDO FRONTEND AND BACKEND
        'behaviors' => array(
            'runEnd' => array(
                'class' => 'application.components.WebApplicationEndBehavior',
             ),
         ),
    //++++++++++++++++++++++++++++++++
    
    'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
                    'generatorPaths'=>array(
                        'bootstrap.gii',
                    ),
			'class'=>'system.gii.GiiModule',
			'password'=>'0762457',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
         'auth' => array(
                    'strictMode' => false, // when enabled authorization items cannot be assigned children of the same type.
                    'userClass' => 'Usuario', // the name of the user model class.
                    'userIdColumn' => 'id', // the name of the user id column.
                    'userNameColumn' => 'nombreUsuario', // the name of the user name column.
                    'defaultLayout' => 'application.views.layouts.main', // the layout used by the module.
                    'viewDir' => null, // the path to view files to use with this module.
                ),
		
	),

	// application components
	'components'=>array(
                //'coreMessages'=>array('basePath'=>'protected/messages'),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
                        'class' => 'auth.components.AuthWebUser',
		),
                'bootstrap'=>array(
                    'class'=>'bootstrap.components.Bootstrap',
                ),
                   'mailer' => array(
      'class' => 'application.extensions.mailer.EMailer',
      'pathViews' => 'application.views.email',
      'pathLayouts' => 'application.views.email.layouts'
   ),   
                // uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database          

            
             'authManager' => array(
                    'class' => 'CDbAuthManager',
                    'connectionID' => 'db',
                    'behaviors' => array(
                        'auth.components.AuthBehavior',
                    ),
                    'class' => 'auth.components.CachedDbAuthManager',
                    'cachingDuration' => 3600,
                ),
         
		//PRODUCCION
		/*'db'=>array(
			'connectionString' => 'mysql:host=127.9.227.2;dbname=globalr',
			'emulatePrepare' => true,
			'username' => 'admin8Le83Bh',
			'password' => 'DeJP_lN_nG2_',
			'charset' => 'utf8',
		),
            */
                //DESARROLLO
           /*    'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=globalpayment',
			'emulatePrepare' => true,
			'username' => 'gpayment',
			'password' => 'Global2014',
			'charset' => 'utf8',
		),*/
		

 		'db' => array(
            'connectionString' =>'mysql:host=' . (($_SERVER['SERVER_NAME'] === 'localhost') ? 'localhost;dbname=globalpayment' : '127.9.227.2;dbname=globalr'),
            'emulatePrepare' => true,
            'username' => (($_SERVER['SERVER_NAME']==='localhost') ? 'gpayment' : 'admincNDmXEX'),
            'password' => (($_SERVER['SERVER_NAME']==='localhost') ? 'Global2014' : 'nvRRHH44yh-k'),
            'charset' => 'utf8',
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning,info',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);