<?php

return CMap::mergeArray(
                require(dirname(__FILE__) . '/main.php'), array(
            'theme' => 'admin', // Tema BACKEND
            'components' => array(
                'urlManager' => array(
                    'urlFormat' => 'path',
                    'showScriptName' => true,//ojo con esto IMPORTANTE
                    'rules' => array(
                        'backend' => 'admin/index',
                        'backend/<_c>' => '<_c>',
                        'backend/<_c>/<_a>' => '<_c>/<_a>',
                    ),
                ),
            /* 'request' => array(
              'baseUrl' => '/moduloPayment/backend',
              ), */
            ), 
                )
);
?>
