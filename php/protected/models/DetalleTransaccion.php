<?php

/**
 * This is the model class for table "detalle_transaccion".
 *
 * The followings are the available columns in table 'detalle_transaccion':
 * @property integer $id
 * @property integer $TRANSACCION_id
 * @property integer $bancoEmisor
 * @property integer $bancoReceptor
 * @property string $fechaPago
 * @property string $numeroReferencia
 * @property double $monto
 * @property string $observaciones
 * @property string $rifCliente
 *
 * The followings are the available model relations:
 * @property Bancos $bancoEmisor0
 * @property Bancos $bancoReceptor0
 * @property Transaccion $tRANSACCION
 */
class DetalleTransaccion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detalle_transaccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TRANSACCION_id, bancoEmisor, bancoReceptor, fechaPago, numeroReferencia, monto, rifCliente', 'required'),
			array('TRANSACCION_id, bancoEmisor, bancoReceptor', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			array('numeroReferencia, rifCliente', 'length', 'max'=>45),
			array('observaciones', 'length', 'max'=>300),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, TRANSACCION_id, bancoEmisor, bancoReceptor, fechaPago, numeroReferencia, monto, observaciones, rifCliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bancoEmisor0' => array(self::BELONGS_TO, 'Bancos', 'bancoEmisor'),
			'bancoReceptor0' => array(self::BELONGS_TO, 'Bancos', 'bancoReceptor'),
			'tRANSACCION' => array(self::BELONGS_TO, 'Transaccion', 'TRANSACCION_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'TRANSACCION_id' => 'Transaccion',
			'bancoEmisor' => 'Banco Emisor',
			'bancoReceptor' => 'Banco Receptor',
			'fechaPago' => 'Fecha Pago',
			'numeroReferencia' => 'Numero Referencia',
			'monto' => 'Monto',
			'observaciones' => 'Observaciones',
			'rifCliente' => 'Rif Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('TRANSACCION_id',$this->TRANSACCION_id);
		$criteria->compare('bancoEmisor',$this->bancoEmisor);
		$criteria->compare('bancoReceptor',$this->bancoReceptor);
		$criteria->compare('fechaPago',$this->fechaPago,true);
		$criteria->compare('numeroReferencia',$this->numeroReferencia,true);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('rifCliente',$this->rifCliente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetalleTransaccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
