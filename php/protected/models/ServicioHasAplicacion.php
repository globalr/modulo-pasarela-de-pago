<?php

/**
 * This is the model class for table "servicio_has_aplicacion".
 *
 * The followings are the available columns in table 'servicio_has_aplicacion':
 * @property integer $id
 * @property integer $servicio_id
 * @property integer $aplicacion_id
 * @property string $fechaContratacion
 * @property double $precio
 * @property integer $contratado
 *
 * The followings are the available model relations:
 * @property Aplicacion $aplicacion
 * @property Servicio $servicio
 */
class ServicioHasAplicacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'servicio_has_aplicacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('servicio_id, aplicacion_id, fechaContratacion', 'required'),
			array('servicio_id, aplicacion_id, contratado', 'numerical', 'integerOnly'=>true),
			array('precio', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, servicio_id, aplicacion_id, fechaContratacion, precio, contratado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aplicacion' => array(self::BELONGS_TO, 'Aplicacion', 'aplicacion_id'),
			'servicio' => array(self::BELONGS_TO, 'Servicio', 'servicio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'servicio_id' => 'Servicio',
			'aplicacion_id' => 'Aplicacion',
			'fechaContratacion' => 'Fecha Contratacion',
			'precio' => 'Precio',
			'contratado' => 'Contratado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('servicio_id',$this->servicio_id);
		$criteria->compare('aplicacion_id',$this->aplicacion_id);
		$criteria->compare('fechaContratacion',$this->fechaContratacion,true);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('contratado',$this->contratado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function appAsignada($data,$row,$dataColumn){

            if($this->model()->findByAttributes(array('servicio_id'=>$data->id,'aplicacion_id'=>$dataColumn->name)))
                return 0;
            else
                return 1;        
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ServicioHasAplicacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
