<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $id
 * @property string $codigoEmpresa
 * @property string $nombreUsuario
 * @property string $password
 * @property string $razonSocial
 * @property string $rif
 * @property string $responsable
 * @property string $direccion
 * @property string $telefono
 * @property string $correo
 * @property integer $IDIOMA_id
 *
 * The followings are the available model relations:
 * @property Aplicacion[] $aplicacions
 * @property Idioma $iDIOMA
 */
require_once 'lib/globalr/cypher/cypher.php';
class Usuario extends CActiveRecord
{
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigoEmpresa, nombreUsuario, password, razonSocial, rif, responsable, direccion, telefono, correo, IDIOMA_id', 'required'),
			array('IDIOMA_id', 'numerical', 'integerOnly'=>true),
			array('codigoEmpresa, nombreUsuario, rif', 'length', 'max'=>45),
			array('password', 'length', 'max'=>256),
			array('razonSocial', 'length', 'max'=>100),
			array('responsable', 'length', 'max'=>40),
			array('direccion', 'length', 'max'=>300),
			array('telefono', 'length', 'max'=>15),
			array('correo', 'length', 'max'=>320),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigoEmpresa, nombreUsuario, password, razonSocial, rif, responsable, direccion, telefono, correo, IDIOMA_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aplicacions' => array(self::HAS_MANY, 'Aplicacion', 'USUARIOS_id'),
			'iDIOMA' => array(self::BELONGS_TO, 'Idioma', 'IDIOMA_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigoEmpresa' => 'Codigo Empresa',
			'nombreUsuario' => 'Nombre Usuario',
			'password' => 'Password',
			'razonSocial' => 'Razon Social',
			'rif' => 'Rif',
			'responsable' => 'Responsable',
			'direccion' => 'Direccion',
			'telefono' => 'Telefono',
			'correo' => 'Correo',
			'IDIOMA_id' => 'Idioma',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigoEmpresa',$this->codigoEmpresa,true);
		$criteria->compare('nombreUsuario',$this->nombreUsuario,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('razonSocial',$this->razonSocial,true);
		$criteria->compare('rif',$this->rif,true);
		$criteria->compare('responsable',$this->responsable,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('IDIOMA_id',$this->IDIOMA_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        protected function beforeSave() {
            $this->password = encrypt($this->password);
            return parent::beforeSave();
        }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
