<?php

/**
 * This is the model class for table "servicio_has_recurso".
 *
 * The followings are the available columns in table 'servicio_has_recurso':
 * @property integer $id
 * @property integer $servicio_id
 * @property integer $recurso_id
 *
 * The followings are the available model relations:
 * @property Recurso $recurso
 * @property Servicio $servicio
 */
class ServicioHasRecurso extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'servicio_has_recurso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('servicio_id, recurso_id', 'required'),
			array('servicio_id, recurso_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, servicio_id, recurso_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'recurso' => array(self::BELONGS_TO, 'Recurso', 'recurso_id'),
			'servicio' => array(self::BELONGS_TO, 'Servicio', 'servicio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'servicio_id' => 'Servicio',
			'recurso_id' => 'Recurso',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('servicio_id',$this->servicio_id);
		$criteria->compare('recurso_id',$this->recurso_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
            public function recursoAsignado($data,$row,$dataColumn){
       //var_dump($dataColumn->name);
        if($this->model()->findByAttributes(array('servicio_id'=>$data->id,'recurso_id'=>$dataColumn->name)))
            return 0;
        else
            return 1;        
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ServicioHasRecurso the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
