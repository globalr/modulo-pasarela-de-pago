<?php

/**
 * This is the model class for table "historico_transaccion".
 *
 * The followings are the available columns in table 'historico_transaccion':
 * @property integer $id
 * @property string $codigoTransaccion
 * @property string $fechaCreacion
 * @property string $descripcion
 * @property double $monto
 * @property integer $tiempoTransaccion
 * @property integer $APLICACION_id
 * @property string $STATUS_PAYMENT_id
 * @property string $codigoPago
 * @property string $rifCliente
 *
 * The followings are the available model relations:
 * @property DetalleHistorico[] $detalleHistoricos
 * @property Aplicacion $aPLICACION
 */
class HistoricoTransaccion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'historico_transaccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigoTransaccion, fechaCreacion, descripcion, monto, tiempoTransaccion, APLICACION_id, STATUS_PAYMENT_id, codigoPago, rifCliente', 'required'),
			array('tiempoTransaccion, APLICACION_id', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			array('codigoTransaccion, rifCliente', 'length', 'max'=>45),
			array('descripcion', 'length', 'max'=>200),
			array('STATUS_PAYMENT_id', 'length', 'max'=>17),
			array('codigoPago', 'length', 'max'=>22),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigoTransaccion, fechaCreacion, descripcion, monto, tiempoTransaccion, APLICACION_id, STATUS_PAYMENT_id, codigoPago, rifCliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detalleHistoricos' => array(self::HAS_MANY, 'DetalleHistorico', 'TRANSACCION_id'),
			'aPLICACION' => array(self::BELONGS_TO, 'Aplicacion', 'APLICACION_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigoTransaccion' => 'Codigo Transaccion',
			'fechaCreacion' => 'Fecha Creacion',
			'descripcion' => 'Descripcion',
			'monto' => 'Monto',
			'tiempoTransaccion' => 'Tiempo Transaccion',
			'APLICACION_id' => 'Aplicacion',
			'STATUS_PAYMENT_id' => 'Status Payment',
			'codigoPago' => 'Codigo Pago',
			'rifCliente' => 'Rif Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigoTransaccion',$this->codigoTransaccion,true);
		$criteria->compare('fechaCreacion',$this->fechaCreacion,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('tiempoTransaccion',$this->tiempoTransaccion);
		$criteria->compare('APLICACION_id',$this->APLICACION_id);
		$criteria->compare('STATUS_PAYMENT_id',$this->STATUS_PAYMENT_id,true);
		$criteria->compare('codigoPago',$this->codigoPago,true);
		$criteria->compare('rifCliente',$this->rifCliente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HistoricoTransaccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
