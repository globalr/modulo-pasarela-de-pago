<?php

class PagoTdc extends CFormModel
{
    public $monto;
    public $tiempo;
    public $idTransaccion;
    public $numeroTarjeta;
    public $tipoTarjeta;
    public $codigoSeguridad;
    public $fechaVencimiento;
    public $rif;   

 
    public function rules()
    {
        return array(
            array('tiempo', 'required'),//OJO CON LOS RULES
            array('monto','numerical','integerOnly' => false),
            array('monto,idTransaccion,numeroTarjeta,tipoTarjeta,codigoSeguridad,fechaVencimiento,rif','required'),
            array('numeroTarjeta', 'numerical','integerOnly' => true),
            array('numeroTarjeta', 'length','max' => 19,'min'=>15),
            array('tipoTarjeta', 'numerical','integerOnly'=>true),
            array('codigoSeguridad', 'numerical','integerOnly' => true),
            array('codigoSeguridad', 'length','max' => 5,'min'=>3),
            array('fechaVencimiento', 'numerical','integerOnly' => true),
            array('fechaVencimiento', 'length','max' => 4,'min'=>4),
            array('rif','length', 'max'=>20),
        );
    }
 public function attributeLabels()
	{
		return array(
			'monto' => 'Monto',
			'tiempo' => 'tiempo de Pago',
			'idTransaccion' => 'Codigo Transaccion',
			'numeroTarjeta' => 'Numero de la Tarjeta',
			'tipoTarjeta' => 'Tipo',
			'codigoSeguridad' => 'Codigo de Seguridad',
			'fechaVencimiento' => 'Fecha de Vencimiento',
			'rif' => 'Rif',			
		);
	}
}
