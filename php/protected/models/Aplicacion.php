<?php

/**
 * This is the model class for table "aplicacion".
 *
 * The followings are the available columns in table 'aplicacion':
 * @property integer $id
 * @property string $codigoAplicacion
 * @property string $nombre
 * @property string $descripcion
 * @property integer $USUARIOS_id
 * @property string $logo
 * @property string $fondo
 * @property string $estilo
 * @property string $urlRetorno
 * @property string $cancelUrl
 *
 * The followings are the available model relations:
 * @property Usuario $uSUARIOS
 * @property HistoricoTransaccion[] $historicoTransaccions
 * @property ServicioHasAplicacion[] $servicioHasAplicacions
 * @property Transaccion[] $transaccions
 */
class Aplicacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'aplicacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigoAplicacion, nombre, descripcion, USUARIOS_id', 'required'),
			array('USUARIOS_id', 'numerical', 'integerOnly'=>true),
			array('codigoAplicacion', 'length', 'max'=>45),
			array('nombre', 'length', 'max'=>100),
			array('descripcion, logo, fondo, estilo', 'length', 'max'=>300),
			array('urlRetorno, cancelUrl', 'length', 'max'=>2100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigoAplicacion, nombre, descripcion, USUARIOS_id, logo, fondo, estilo, urlRetorno, cancelUrl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'uSUARIOS' => array(self::BELONGS_TO, 'Usuario', 'USUARIOS_id'),
			'historicoTransaccions' => array(self::HAS_MANY, 'HistoricoTransaccion', 'APLICACION_id'),
			'servicioHasAplicacions' => array(self::HAS_MANY, 'ServicioHasAplicacion', 'aplicacion_id'),
			'transaccions' => array(self::HAS_MANY, 'Transaccion', 'APLICACION_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigoAplicacion' => 'Codigo Aplicacion',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'USUARIOS_id' => 'Usuarios',
			'logo' => 'Logo',
			'fondo' => 'Fondo',
			'estilo' => 'Estilo',
			'urlRetorno' => 'Url Retorno',
			'cancelUrl' => 'Cancel Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigoAplicacion',$this->codigoAplicacion,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('USUARIOS_id',$this->USUARIOS_id);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('fondo',$this->fondo,true);
		$criteria->compare('estilo',$this->estilo,true);
		$criteria->compare('urlRetorno',$this->urlRetorno,true);
		$criteria->compare('cancelUrl',$this->cancelUrl,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=>6,
                ),
		));
	}
        
        
         public function cargarImagen($result,$userdata){
//$result=> array('success'=>true,'filename'=>$filename,'size'=>$size,'ext'=>$ext,'path'=>$uploadDirectory,'fullpath'=>$fullpath);
           $imagen=$this->model()->findByPk($userdata);
           if($result['filename']==='logo')
                $imagen->logo=$result['filename'].'.'.$result['ext'];
           else
               if($result['filename']==='fondo')
                $imagen->fondo=$result['filename'].'.'.$result['ext'];
               
           $imagen->save(); 

        }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Aplicacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
