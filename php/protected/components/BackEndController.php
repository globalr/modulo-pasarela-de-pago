<?php
// SEPARANDO FRONTEND AND BACKEND
class BackEndController extends CController
{
    public $layout='column1';// SEPARANDO FRONTEND AND BACKEND
    public $menu=array();
    public $breadcrumbs=array();
 
    
    public function filters()
    {
       return array( 'accessControl' ); 
    }

    public function accessRules()
    {
         return array(
			array('allow',
				'roles' => array('admin'),
			),
			array('allow',
                                'controllers' => array('empresas','recurso'),
				//'expression' => 'Yii::app()->user->checkAccess("empresa")',
                                'roles' => array('empresa'),
			),
                        array('allow','controllers'=>array('admin'),'actions'=>array('CronJobTransacciones','CronJobGuardarEnHistorico'),'users'=>array('*')),
             //array('deny', 'deniedCallback' => function() {throw new CHttpException(403, 'Acceso Denegado.');}),
			array('deny', 'deniedCallback' => function() {Yii::app()->controller->redirect(yii::app()->createUrl('../site/accesoDenegado'));}),
		);
    }
       
}
?>
