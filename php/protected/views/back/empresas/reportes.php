<br>
<div class="tituloMenu"><h4>Reportes</h4> </div> 
<?php
$this->breadcrumbs = array(
    'Reportes' => array('reportes'),
    'Graficos' => array('reportes'),
);

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Graficos', 'url' => '#', 'active' => true),

)));
?>
<br>
<div class="panel panel-success col-md-5 ">
<h6>Transacciones por aplicacion Mensual</h6>
    <div class="panel-body">
<?php
$this->widget('ext.gChart.HzlVisualizationChart', array('visualization' => 'PieChart',
    'data' => $pieChart,
    'options' => array(
        //'title' => 'Transacciones por aplicacion',
        //'width' => 500,
                'height' => 300,
                
                'minorTicks' => 5)));
?>
    </div>
</div>
