<br>
<div class="tituloMenu"><h4>Aplicaciones</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Aplicaciones Asociadas', 'url' => '#', 'active' => true),
        array('label' => 'Añadir Aplicación', 'url' => yii::app()->createUrl('empresas/crearAplicacion')),
)));
?>
<?php
if ($aplicacionesAsignadas)
    $this->widget(
            'bootstrap.widgets.TbExtendedGridView', array(
        //'fixedHeader' => true,
        //'filter'=>$model,
        'headerOffset' => 40,
        // 40px is the height of the main navigation at bootstrap
        'type' => 'striped',
       //'sortableRows' => true,
        'dataProvider' => new CArrayDataProvider($aplicacionesAsignadas),
        'responsiveTable' => true,
        'template' => "{items}{summary}{pager}", 
        'columns' => array(
            //array('name' => 'id', 'header' => 'ID'),
            //array('name' => 'tipo', 'header' => 'Servicio'),
            array('name' => 'codigoAplicacion', 'header' => 'Codigo'),
            array('name' => 'nombre', 'header' => 'Aplicacion'),
            array('name' => 'descripcion', 'header' => 'Descripcion'),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'header' => 'Gestionar',
                'template' => '{view}{caracteristicas}',
                'buttons' => array(
                    'view' => array(
                    'label' => 'Ver',
                    'imageutl' => '',
                        'url' => 'yii::app()->createUrl("empresas/detalleAplicacion",array("id"=>$data->id))',                        
                    'options' => array(
                        'class' => 'btn btn-small'
                    ),
                ),
                    'caracteristicas' => array(
                        'label' => 'Personalizar',
                        'url' => 'yii::app()->createUrl("empresas/caracteristicas",array("id"=>$data->id))',
                        'options' => array(
                            'class' => 'btn btn-small delete btn-primary'
                        )
                    ),
                ),
                'htmlOptions' => array('style' => 'width: 160px;  text-align:right'),
            )
        ))
    );
else
    echo 'No hay Aplicaciones Asociadas aun.';
?>    

