<br>
<div class="tituloMenu"><h4>Servicios</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Servicio a Contratar', 'url' => '#', 'active' => true),
        array('label' => 'Servicios Contratados', 'url' => yii::app()->createUrl('empresas/serviciosContratados')),
        array('label' => 'Servicios Disponibles', 'url' => yii::app()->createUrl('empresas/listaServicios')),

)));
?>
<br>

                 <?php
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$serv,
                    'attributes'=>array(		
                                    'tipo',
                                    'precio',
                                    'descripcion',
                    ),
                    ));
                    ?>
                <br>
                <?php
                if ($aplicaciones) {
                    $form = $this->beginWidget('CActiveForm', array('id' => 'usuario-form', 'enableAjaxValidation' => false,));

                    echo 'Seleccione una Aplicacion para asignar al servicio: &nbsp;&nbsp;';

                    echo $form->dropDownList($aplicaciones[0], 'nombre', CHtml::listData(Aplicacion::model()->findAllByAttributes(array('USUARIOS_id' => yii::app()->user->id)), 'id', 'nombre'),
                            $htmlOptions = array('class'=>'listUi'), 
                            array('id' => 'appSeleccionada')                            
                    );
                    echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                    echo CHtml::ajaxButton("Contratar", Yii::app()->createUrl("empresas/ajaxSolicitarServicio"), array(
                        'data' => array('aplicacion' => 'js: $("#appSeleccionada option:selected").val()', 'servicio' => $serv->id,'precio'=>$serv->precio),
                        'update' => "#data",
                        //'beforeSend' => 'function(){$("#loading").addClass("visibleloading");}',
                        'complete' => 'function(){document.location.reload(true);}'
                            ), array('class' => 'btn btn-primary'));
                    $this->endWidget();
                } else {
                    yii::app()->user->setFlash('info', 'Para contratar servicios debes tener aplicaciones asociadas a tu cuenta. <a href="#"> &nbsp;Asocias aplicacion.</a>');
                }
                ?>           
                           

               <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'block'=>true, // display a larger alert block?
                    'fade'=>true, // use transitions?
                    'closeText'=>false, // close link text - if set to false, no close link is displayed
                    'alerts'=>array( // configurations per alert type
                        'info'=>array('block'=>true, 'fade'=>true), // success, info, warning, error or danger
                    ),
                )); ?>
