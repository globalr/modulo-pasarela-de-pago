<br>
<div class="tituloMenu"><h4>Servicios</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Servicios Contratados', 'url' => '#', 'active' => true),
        array('label' => 'Servicios Disponibles', 'url' => yii::app()->createUrl('empresas/listaServicios')),
)));
?>

            <?php
            if($servicios){
            $this->widget('bootstrap.widgets.TbGroupGridView', array(
                'type' => 'striped bordered',
                'dataProvider' => new CArrayDataProvider($servicios),
                'template' => "{items}{summary}{pager}", 
                'columns' => array(
                   array('name'=>'Aplicación',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link($data->aplicacion->nombre, array("empresas/detalleAplicacion","id"=>$data->aplicacion->id))'),
        
                    array('name' => 'aplicacion.nombre', 'header' => 'Aplicacion Asociada'),
                    array('name' => 'servicio.tipo', 'header' => 'Servicio'),
                    array('name' => 'fechaContratacion', 'header' => 'Fecha de Contrato'),
                    array('name' => 'precio', 'header' => 'Precio'),
                    array('name' => 'contratado', 'header' => 'Estado'),
                ),
                'mergeColumns' => array('aplicacion.nombre')
            ));
            }else{
                echo 'No tienes Servicios contratados aun.<br><br> Para contratar servicios debes tener aplicaciones asociadas a tu cuenta.';
                ?>
<a href="<?php echo yii::app()->createUrl('empresas/crearAplicacion')?>"> &nbsp;Asocias aplicacion</a>
                    <?php
            }
            ?> 


