<br>
<div class="tituloMenu"><h4>Servicios</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Servicios Contratados', 'url' => yii::app()->createUrl('empresas/serviciosContratados')),
        array('label' => 'Servicios Disponibles', 'url' => '#', 'active' => true),
)));
?>
<br>
<?php foreach ($servicios as $value):?>
<div class="col-md-4">
    <div class="panel panel-success">
        <div class="panel-heading">
            <?php echo $value->tipo; ?>
        </div>
        <div class="panel-body">
            <p><?php echo $value->descripcion; ?></p>
        </div>
        <div class="panel-footer">
            <div style="float: left; margin-top: 15px;">
                Precio:  <strong><?php echo $value->precio;?>  Bs.</strong>
            </div>
            <div style="float: right ">
                <?php $this->widget('bootstrap.widgets.TbButton',array(
                        'label' => 'Contratar',
                        'type' => 'primary',
                        'url'=> $this->createUrl('asignarServicio',array('idServicio'=>$value->id))
                    )                        
                );
                ?>   
            </div>
            <div style="clear: both"></div>            
        </div>
    </div>
</div>
<?php endforeach;?>