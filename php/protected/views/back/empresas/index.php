<br>
<?php $this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
    'heading'=>'Bienvenido '. yii::app()->user->name .'!',
)); ?>
<br>
    <p>Este es el nuevo modulo administrativo de gestion de transacciones de la pasarela de pago GlobalPayment.</p>
    <p><?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'size'=>'large',
        'label'=>'Como Funciona',
        'url'=> yii::app()->createurl('empresas/guiaIntegracion'),
    )); ?></p>
 
<?php $this->endWidget(); ?>
