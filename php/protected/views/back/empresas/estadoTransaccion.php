<br>
<div class="tituloMenu"><h4>Transacciones</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Transacciones', 'url' => '#', 'active' => true),
)));
?>

<br>
<?php
$form = $this->beginWidget('CActiveForm', array('id' => 'usuario-form', 'enableAjaxValidation' => false,));
echo 'Seleccione una Aplicacion: &nbsp;&nbsp;';
echo $form->dropDownList(
        $aplicacion, 
        'nombre', 
        CHtml::listData(Aplicacion::model()->findAllByAttributes(array('USUARIOS_id' => yii::app()->user->id)), 'id', 'nombre'),
        array(
            'options' => array($aplicacion->id => array('selected' => true)),
            'id' => 'appSeleccionada',
            'class'=>'listUi')
);
echo '&nbsp;&nbsp;&nbsp;';
echo CHtml::ajaxButton("actualizar", Yii::app()->createUrl("empresas/ajaxTransacciones"), array(
    'data' => array('aplicacion' => 'js: $("#appSeleccionada option:selected").val()'),
    'update' => "#data",
    'beforeSend' => 'function(){$("#loading").addClass("visibleloading");}',
    'complete' => 'function(){$("#loading").removeClass("visibleloading");}'), array('class' => 'btn btn-primary'));
$this->endWidget();
?>



<div id="data">
    <?php $this->renderPartial('_gridTrans', array('transacciones' => $transacciones)); ?>
</div>

<div class="panel-footer"> 
    <div id="loading" class="invisibleloading">
        <div class="progress progress-striped active">
            <div class="progress-bar" style="width: 100%;"></div>
        </div>
    </div>
</div>



