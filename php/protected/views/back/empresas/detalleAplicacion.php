<br>
<div class="tituloMenu"><h4>Aplicaciones - <?php echo $model->nombre?></h4> </div>  
<?php
$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	'Caracteristicas',
);


$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=> yii::app()->createUrl("empresas/aplicacionesAsociadas",array("id"=>$model->id))),
        array('label'=>'Ver Detalle', 'url'=>'#','active'=>true),
	array('label'=>'Caracteristicas', 'url'=>yii::app()->createUrl("empresas/caracteristicas",array("id"=>$model->id))),
)));
?>

<br>

                <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                            'type'=>'striped borderd ',
                            'data'=>$model,
                            'attributes'=>array(		
                                    'codigoAplicacion',		
                                    'nombre',
                                    'descripcion',
                                    'urlRetorno',
                                    array('label'=>'Empresa','name'=>'uSUARIOS.razonSocial'),
                                    array(
                                    'type'=>'raw',
                                    'value'=>CHtml::link(CHtml::encode('Ver'), array("Empresas/serviciosContratados")),
                                    'label'=>'Servicios Contratados'
                                    ),

                            ),
                    )); 
                    ?>  


