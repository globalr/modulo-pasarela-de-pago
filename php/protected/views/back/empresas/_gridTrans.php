                 
<?php

if ($transacciones)
    $this->widget(
            'bootstrap.widgets.TbExtendedGridView', array(
        'id' => 'gridTransacciones',
        //'fixedHeader' => true,
        //'filter'=>$model,
        'headerOffset' => 40,
        // 40px is the height of the main navigation at bootstrap
        'type' => 'striped',
        //'sortableRows' => true,
        'dataProvider' => new CArrayDataProvider($transacciones),
        'responsiveTable' => true,
        'template' => "{items}{summary}{pager}", 
        'columns' => array(
            //array('name' => 'id', 'header' => 'ID'),
            //array('name' => 'tipo', 'header' => 'Servicio'),
            array('name' => 'codigoTransaccion', 'header' => 'ID'),
            array('name' => 'fechaCreacion', 'header' => 'Fecha'),
            array('name' => 'monto', 'header' => 'Monto'),
            array('name' => 'codigoPago', 'header' => 'Tipo de pago'),
            array('name' => 'STATUS_PAYMENT_id', 'header' => 'Estado'),
        ))
    );
else
    echo 'No hay Transacciones aun';
?> 


