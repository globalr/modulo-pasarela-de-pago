<br>
<div class="tituloMenu"><h4>Aplicaciones</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Aplicaciones Asociadas', 'url' => yii::app()->createUrl('empresas/aplicacionesAsociadas')),
        array('label' => 'Añadir Aplicación', 'url' => '#', 'active' => true),
)));
?>
<br>
<div class="form">
    <?php
    if ($model->isNewRecord) {
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'aplicacion-form',
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'nombre'); ?>
            <br>
            <?php echo $form->textField($model, 'nombre', array('size' => 45, 'maxlength' => 45)); ?>
            <?php echo $form->error($model, 'nombre'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'descripcion'); ?>
            <br>
            <?php echo $form->textField($model, 'descripcion', array('size' => 120, 'maxlength' => 120)); ?>
            <?php echo $form->error($model, 'descripcion'); ?>
        </div>
        <?php echo $form->hiddenField($model, 'codigoAplicacion', array('value' => uniqid())); ?>
        <div class="row">
            <?php
            echo $form->hiddenField($model, 'USUARIOS_id', array('value' => yii::app()->user->id));
            ?>
        </div>
        <br>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Crear', array('class' => 'btn btn-primary')); ?>
        </div>

        <?php $this->endWidget();
    } ?>

</div><!-- form -->

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => false, // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'info' => array('block' => true, 'fade' => true), // success, info, warning, error or danger
    ),
));
?> 

