<br>
<div class="tituloMenu"><h4>Recursos</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Descarga de Recursos', 'url' => '#', 'active' => true),
        
)));
?>

            <?php
            if($recursos){
            $this->widget('bootstrap.widgets.TbGroupGridView', array(
                'type' => 'striped bordered',
                'dataProvider' => new CArrayDataProvider($recursos),
                'template' => "{items}{summary}{pager}", 
                'columns' => array(
                    array('name'=>'servicio.tipo',
                                    'header'=>'Servicio Asociado',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link($data->servicio->tipo, array("empresas/listaServicios","id"=>$data->servicio->id))'),
                    array('name' => 'recurso.Nombre', 'header' => 'Recurso'),
                    array('name' => 'recurso.tipo', 'header' => 'Tipo'),
                    array('name' => 'recurso.descripcion', 'header' => 'Descripcion'),                    
                    array('class'=>'CLinkColumn',                           
                            'urlExpression'=>'Yii::app()->createUrl("recurso/download",array("url"=>$data->recurso->url));',
                            'label'=>'Descargar',
                            'header'=>'Enlace',
                    ),
		
                ),
                'mergeColumns' => array('servicio.tipo')
            ));   
            }else{
                echo '<br>';
                Yii::app()->user->setFlash('warning', '<strong>¡No hay recursos disponibles!.</strong> 
                Recuerda que los recursos entan asociados a los servicios que contratas. Para descargar recursos 
                primero debes haber contratado algun servicio. <a href="'.$this->createUrl('listaServicios').'"> &nbsp;Contratar Servicios.</a>');
            }
            ?> 

                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'block'=>true, // display a larger alert block?
                    'fade'=>true, // use transitions?
                    'closeText'=>false, // close link text - if set to false, no close link is displayed
                    'alerts'=>array( // configurations per alert type
                        'warning'=>array('block'=>true, 'fade'=>true), // success, info, warning, error or danger
                    ),
                )); ?>
