<br>
<h4>Proceso de Envío</h4>
<br>
<p>El proceso empieza cuando el cliente entra a la aplicación WEB del comercio electrónico, 
    selecciona los productos o servicios que desea adquirir y presiona el botón pagar. El 
    comercio debe totalizar el valor de la compra y enviar a nuestro sistema un formulario 
    de pago por método <strong>POST</strong> con los parámetros que aparecen en la tabla PARAMETROS OBLIGATORIOS.</p>
<br>
<p>El cliente será dirigido a nuestra pasarela de pago, donde introducirá los datos de pago 
    y nuestro sistema procesará la transacción y 
    pasará a ejecutar el proceso de retorno.</p>
<br>
<h6>      Formularios requeridos</h6>
<br>
<ol>
    <li><strong>Formulario de Pago</strong></li>
</ol>
<br>
<p>Debes generar un formulario HTML con los datos de la transacción utilizando el método HTTPS POST 
    y que apunte a nuestro sistema (http://www.payment.globalr.net/pay). Este formulario puede
    ser similar a la siguiente interfaz:</p>
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/integracion/carrito.png"/>
<br>
<br><p>En la imagen anterior se presenta un modelo básico estándar para realizar el pago de ciertos
    productos, a modo de brindar una referencia de cómo puede el comercio electrónico presentar una 
    interfaz  de pago para sus clientes.</p>
<br><p>Para GlobalPayment la única información visible necesaria para procesar el pago en este caso 
    sería el valor reflejado en el campo <strong>Total (IVA Incl.)</strong> rebordeado en rojo en la imagen, 
    que es el     correspondiente al total final que el comercio electrónico cobrará a sus clientes 
    por los productos    o servicios prestados.</p>
<br><p>Cabe destacar que adicional al <strong>Total (IVA Incl.)</strong> GlobalPayment necesitará recibir otros 
    parámetros     que serán definidos  a continuación:</p>

<h6>Parametros Obligatorios:</h6>
<div id="yw2" class="grid-view">
<table class="items table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th id="yw2_c0">Parámetro</th><th id="yw2_c1" >Formato</th><th id="yw2_c2">Descripción</th></tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td style="width: 160px">idEmpresa</td>
            <td style="width: 220px">Digitos</td>
            <td>Es un número único que identifica al comercio electrónico en nuestro sistema, 
                dicho número será suministrado por nosotros al momento de realizar el contrato
                del servicio y deberá ser proporcionado por cada comercio en cada transacción
                generada, para realizar las validaciones correspondientes. </td>
        </tr>
        <tr class="even">
            <td style="width: 60px">password</td>
            <td>Valor String</td>
            <td>Es una combinación de caracteres que utilizará el comercio para poder obtener
                acceso a los servicios de nuestro sistema, dicho PASSWORD será suministrado por 
                nosotros al momento de realizar el contrato del servicio.</td>
        </tr>
        <tr class="odd">
            <td style="width: 60px">idAplicacion</td>
            <td>Valor String</td>
            <td>Cada comercio puede poseer varias aplicaciones en ejecución, por lo tanto deberá 
                suministrar el ID de la aplicación en cada transacción para poder realizar la asociación
                correspondiente. Este número de identificación será suministrado por nosotros al momento 
                de realizar el contrato del servicio.</td>
        </tr>
        <tr class="even">
            <td style="width: 60px">idTansaccion</td>
            <td>Valor String</td>
            <td>Es una combinación de caracteres que identificará cada una de las transacciones procesadas, 
                este identificador deberá ser el valor con el cual el comercio identifica las transacciones
                en su aplicación, ya que será utilizado como valor de retorno junto con otros parámetros 
                después de ser procesada la transacción.</td>
        </tr>
        <tr class="odd">
            <td style="width: 60px">tiempoTransaccion</td>
            <td>Dígitos</td>
            <td>Es un valor reflejado en minutos segundos y se utilizará para limitar el tiempo que poseerá
                el cliente para poder concretar una transacción, dicho valor límite será establecido y 
                suministrado por el comercio.</td>
        </tr>
        <tr class="even">
            <td style="width: 60px">tipoPago</td>
            <td>Valor string</td>
            <td>Es un código suministrado por el comercio electrónico para indicar la forma de pago que utilizará
                el cliente para realizar la transacción (ver tabla de código de pago).</td>
        </tr>
        <tr class="odd">
            <td style="width: 60px">Monto</td>
            <td>Valor Flotante</td>
            <td>Este parámetro representa la CANTIDAD TOTAL FINAL a ser cancelada por el cliente en relación con 
                la transacción que está siendo procesada. Dicho monto deberá ser en moneda venezolana y poseer
                solo 2 dígitos para representar los céntimos.</td>
        </tr>
        <tr class="even">
            <td style="width: 60px">urlRetorno</td>
            <td>Valor string</td>
            <td>Es la dirección web a la cual serán retornados todos los resultados relacionados con cada 
                transacción. La misma deberá ser suministrada por el comercio electrónico una sola vez, al
                comienzo de la integración.</td>
        </tr>
        <tr class="even">
            <td style="width: 60px">cancelUrl</td>
            <td>Valor string</td>
            <td>Es la dirección web a la cual serán retornados todos los resultados relacionados con cada 
                transacción. La misma deberá ser suministrada por el comercio electrónico una sola vez, al
                comienzo de la integración.</td>
        </tr>

    </tbody>
</table>

</div>
<br>
<h6>Codigos de Pago</h6>
<div id="yw2" class="grid-view col-md-4">
<table class="items table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th id="yw2_c0">Valor</th><th id="yw2_c1" >Descripción</th></tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td style="width: 60px">1</td>
            <td style="width: 100px">Tarjeta de Crédito</td>
        </tr>
        <tr class="even">
            <td style="width: 60px">2</td>
            <td>Depósito Bancario</td>
        </tr>
        <tr class="odd">
            <td style="width: 60px">3</td>
            <td>Transferencia Bancaria</td>
        </tr>
    </tbody>
</table>
</div>
<div class="clearfix"></div>
<h4>Proceso de Retorno</h4>
<br>
<p>Nuestro sistema termina de procesar la transacción y envía los datos de la misma a la página de actualización del 
    comercio para que registre el nuevo estatus automáticamente.</p>
<br>
<p>Al mismo tiempo redirige al cliente a la página de respuesta del comercio y envía la información de la transacción
    a la misma mediante el método<strong> HTTPS POST</strong> , para que se pueda notificar al cliente el resultado obtenido.</p>
<br>
<p>Adicionalmente el sistema le enviará al representante del comercio un correo electrónico de confirmación
    de la transacción.</p>
<br>
<p>Opcionalmente el sistema podrá enviar un correo electrónico al representante del comercio y a su cliente 
    cada vez que la transacción cambie de estado.</p>
<br>
<h6>      Formularios requeridos</h6>
<br>
<ol>
    <li>
        <strong>Página de Respuesta</strong>
        <br><br>
        <p>Al finalizar una transacción nuestro sistema enviará al cliente a la página de respuesta del 
            comercio electrónico. A esta página nuestro sistema enviará información sobre el estado de una
            transacción a través del método<strong> HTTPS POST</strong> y desde allí el comercio podrá suministrarle al cliente 
            la información relacionada con la transacción. </p>
    </li>
    <br>
            
       <p>Adicionalmente si el comercio desea conocer el status de las transacciones en un momento determinado, 
            podrá consumir un servicio web suministrado  por nosotros para tal fin.</p>
    
</ol>
<br>