<br>
<div class="tituloMenu"><h4>Integración</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Guia Base', 'url' => '#', 'active' => true),
)));
?>
<br>
<?php

$this->widget(
    'bootstrap.widgets.TbWizard',
    array(
        'type' => 'pills', // 'tabs' or 'pills'
        'pagerContent' => '<div style="float:right">
					
					<input type="button" class="btn button-last btn-primary" name="last" value="Final" />
				</div>
				<div style="float:left">
					<input type="button" class="btn button-first btn-primary" name="first" value="Inicio" />
					
				</div><br /><br />',
        'options' => array(

            'firstSelector' => '.button-first',
            'lastSelector' => '.button-last',
            
            //'onTabClick' => 'js:function(tab, navigation, index) {alert("Tab Click Disabled");return false;}',
        ),
        
        'tabs' => array(
            array('label' => 'Introduccion','content' => $this->renderPartial('integracion/_pagina1',null,true),'active' => true,),
            array('label' => 'Proceso de Pago', 'content' => $this->renderPartial('integracion/_pagina2',null,true)),
            array('label' => 'Metodos de Pago', 'content' => $this->renderPartial('integracion/_pagina3',null,true)),
            array('label' => 'Proceso de Integracion', 'content' => $this->renderPartial('integracion/_pagina4',null,true)),
            array('label' => 'Consumo de servicio Web', 'content' => $this->renderPartial('integracion/_pagina6',null,true)),
            array('label' => 'Diagrama de Integracion', 'content' => $this->renderPartial('integracion/_pagina5',null,true)),
        ),
    )
); ?>
        
<div class="panel-footer"> 
    <div id="loading" class="visibleloading">
        <div class="progress progress-striped active">
            <div class="progress-bar" style="width: 100%;"></div>
        </div>
    </div>
</div>

