<br>
<p>
La pasarela de pago GlobalPayment permite a los comercios electrónicos realizar el cobro de sus productos
y servicios a través de la Web de forma cómoda y segura, ofreciendo diversos servicios diseñados 
para facilitar este proceso.</p>
<p>
 El presente documento es una guía generalizada que te permite conocer el proceso de integración 
 de tus aplicaciones con nuestra Pasarela de Pago.&nbsp; Adicionalmente describe los métodos de pago permitidos y como 
 asociarlos correctamente, indicando en cada sección los pasos a seguir y los requisitos necesarios para lograrlo.</P>
<br>