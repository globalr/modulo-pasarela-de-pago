<br>
<p>Con GlobalPayment es muy sencillo recibir tus pagos online:</p>

<ol>
  <li>Los clientes ingresan a tu comercio y seleccionan los productos o servicios que van a pagar.</li>
  <br>
  <li>Al presionar el botón “pagar” serán redireccionados a la pasarela de pago GlobalPayment donde 
      introducirán  los datos de pago necesarios.</li>
  <br>
  <li>El sistema validará la información de pago y tramitará el pago respectivo.</li>
  <br>
  <li>El sistema te notificará mediante correo electrónico el resultado de la transacción procesada, 
      opcionalmente el sistema podrá enviarte a ti al cliente un mensaje de texto (SMS) con la información referente al pago.</li>
  <br>
  <li>Al día siguiente de procesadas las transacciones GlobalPayment transferirá el <strong>MontoRetorno</strong> a 
      la cuenta que nos proporcionaste, junto con el informe de transacciones 
      que respalden la misma. </li>
  <br>
  <li>
      <?php
      $this->widget(
    'bootstrap.widgets.TbLabel',
    array(
        'type' => 'success',
        // 'success', 'warning', 'important', 'info' or 'inverse'
        'label' => 'MontoRetorno = (Monto total de Transacciones) – (Comisión GlobalPayment)',
        'htmlOptions'=>array('style'=>'font-size: 16px')
    )
);
      ?>
  </li>
  <br>

