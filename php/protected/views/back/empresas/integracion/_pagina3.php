<br>
<p>GlobalPayment cuenta con 3 métodos de pago totalmente seguros: Tarjeta de crédito, 
    depósitos y transferencias bancarias.<p>

<h4>Tarjeta de Crédito</h4>

<p>Nuestra pasarela les brindará a tus clientes la posibilidad de realizar pagos con 
    tarjetas de crédito VISA y MASTERCARD de una manera segura y confiable.</p>
<br>
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/integracion/TDC.png"/>
<br><br>
<h4>Campos del Formulario</h4>
<div id="yw2" class="grid-view">
    <table class="items table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th id="yw2_c0">Dato</th><th id="yw2_c1" >Tipo</th><th id="yw2_c2">Descripcion</th></tr>
        </thead>
        <tbody>
            <tr class="odd">
                <td style="width: 160px">Tiempo restante para pagar la transacción</td>
                <td style="width: 220px">Valor reflejado en segundos</td>
                <td>Es el tiempo que tiene disponible el cliente para realizar el pago de la transacción 
                    antes que sea anulada como medida de seguridad, si el tiempo llega a 0 la transacción
                    se anulará y el cliente  deberá volver al formulario y registrar otra vez los productos o servicios a pagar. GlobalPayment 
                    registrará dicha transacción con estado <strong>Tiempo Expirado</strong>.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Monto</td>
                <td>Valor reflejado en moneda venezolana con el formato:
                    000.00 utilizando como separador decimal el punto (.) y utilizando solo 2 digitos para representar los centimos.
                </td>
                <td>Es el monto total que será debitado de la tarjeta de crédito, correspondiente a los 
                    servicios o productos que el cliente desea cancelar.  Este monto no es editable por el cliente,
                    el mismo será recibido directamente desde  el formulario  de pago del comercio electrónico.</td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Numero</td>
                <td>Número entero conformado máximo por 19 dígitos.</td>
                <td>Es el número de la tarjeta de crédito a ser debitada. En caso de ser incorrecto, se le notificará 
                    al cliente el error y se le solicitará  que ingrese un número de tarjeta correcto.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Tipo</td>
                <td>• VISA<br>
                    • MASTERCARD
                </td>
                <td>Es el tipo de la tarjeta de crédito habilitada para realizar el pago, el cliente podrá 
                    seleccionar 1 de 2 tipos de tarjetas según sea el caso.</td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Fecha de vencimiento</td>
                <td>MM/YY se maneja el mismo formato de fecha que está impreso en la tarjeta de crédito.</td>
                <td>Es la fecha de vencimiento de la tarjeta de crédito a debitar, en caso de ser incorrecta, se
                    le notificará al cliente el error y se le pedirá que ingrese una nueva fecha.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Código de seguridad</td>
                <td>Dígitos </td>
                <td>Es el código de 3 dígitos que se encuentra al reverso de la tarjeta de crédito a debitar. 
                    En caso de ser incorrecto, se le notificará al cliente el error y se le pedirá que ingrese 
                    un nuevo código de seguridad.</td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Cédula o RIF</td>
                <td>String</td>
                <td>Indica el número de documento que identifica a la 
                    persona titular de la tarjeta de crédito.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Términos de Uso</td>
                <td>No aplica</td>
                <td>Para poder realizar el pago, el cliente debe haber leído y estar de acuerdo con los términos 
                    de uso de GlobalPayment, relacionados con la ley de delitos informáticos.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Botón Realizar Pago</td>
                <td>No aplica</td>
                <td>Es el botón utilizado para procesar el pago de los productos o servicios seleccionados por el
                    cliente, en caso de no haber irregularidades con los datos introducidos, la transacción será
                    procesada hacia la entidad bancaria y pasará a un estado <strong>Procesando</strong>, una vez la
                    entidad bancaria dé respuesta de la transacción, la misma pasará a tener 1 de 2 estados posibles:
                    <br>
                    • <strong>Aprobada</strong>
                    <br>
                    • <strong>Rechazada</strong>
                    <br>
                    El cliente será redirigido al comercio electrónico junto con el ID de la transacción y el estado de la misma, para que se genere el mensaje correspondiente.
                </td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Botón Volver</td>
                <td>No aplica</td>
                <td>Es el botón utilizado por el cliente para retornar a la página de selección de los productos 
                    o servicios a pagar en caso que desee realizar algún cambio o verificar  su transacción.</td>
            </tr>
        </tbody>
    </table>

</div>

<h4>Depósito / Transferencia Bancaria</h4>
<p>GlobalPayment proporciona cuentas en la mayoría de los bancos que tienen presencia en el país con la finalidad de brindar 
    accesibilidad a los clientes al momento de realizar los pagos.</p>
<p>Adicionalmente cuenta con personal especializado encargado de realizar el proceso interno 
    de verificación y validación de los pagos.</p>
<h6>Depósitos bancarios.</h6>
<p>Los depósitos bancarios serán verificados en aproximadamente 30 minutos luego de que el 
    cliente los registre en el sistema.</p>
<h6>Transferencias</h6>
<ul>
    <li><p><strong> Entre el mismo banco:</strong> Estas transferencias serán verificadas en aproximadamente 30 minutos luego de que el cliente las registre en el sistema.</p></li>
    <li><p><strong>Entre bancos diferentes:</strong> Estas transferencias podrán ser verificadas en un rango de 24 a 48 horas hábiles luego de su realización.</p></li>
</ul> 
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/integracion/depTrans.png"/>
<br><br>
<h4>Campos del Formulario</h4>
<div id="yw2" class="grid-view">
    <table class="items table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th id="yw2_c0">Dato</th><th id="yw2_c1" >Tipo</th><th id="yw2_c2">Descripcion</th></tr>
        </thead>
        <tbody>
            <tr class="odd">
                <td style="width: 160px">Tipo</td>
                <td style="width: 220px">No aplica</td>
                <td>Se utiliza para seleccionar una de las 2 formas de pago que el cliente puede tramitar: <ul><li>Transferencia</li> <li>Depósito</li> </ul> </td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Banco Emisor</td>
                <td>No aplica
                </td>
                <td>Se encuentra habilitada en caso de haber seleccionado como método de pago la 
                    transferencia bancaria, y se utiliza para seleccionar el banco desde el cual 
                    se realiza la transferencia.</td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Banco Receptor</td>
                <td>No aplica</td>
                <td>Se encuentra habilitado en todo momento y se utiliza para seleccionar el banco
                    al cual fueron destinados los fondos, ya sea por transferencia o por depósito.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Fecha</td>
                <td>YYYY/MM/DD/
                </td>
                <td>Se utiliza para ingresar la fecha en la cual fue realizado el pago, 
                    ya sea por transferencia o por depósito.</td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Monto</td>
                <td>Valor reflejado en moneda venezolana con el formato:
                    000.00 utilizando como separador decimal el punto (.) y utilizando solo 2 digitos para representar los centimos.
                </td>
                <td>Es el valor total a pagar por el cliente, es un campo que no es editable y el 
                    mismo será recibido directamente desde el formulario de pago del comercio electrónico. 
                    Debe corresponder con la cantidad reflejada en el depósito o la trasferencia para 
                    poder concretar el pago.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Numero de Referencia</td>
                <td>Dígitos </td>
                <td>Es el número de identificación que se generó al momento de realizar la transferencia 
                    o depósito bancario, y será utilizado internamente por nuestro sistema para validar el
                    pago realizado.</td>
            </tr>
            <tr class="odd">
                <td style="width: 60px">Botón Realizar Pago</td>
                <td>No aplica </td>
                <td>Es el botón utilizado para procesar el pago de los productos o servicios seleccionados por el
                    cliente, en caso de no haber irregularidades con los datos introducidos, la transacción será
                    procesada por nuestro sistema y pasará a un estado <strong>Procesando</strong>, una vez verificada la transacción, la misma pasará a tener 1 de 2 estados posibles:
                    <br>
                    • <strong>Aprobada</strong>
                    <br>
                    • <strong>Rechazada</strong>
                    <br>
                    El cliente será redirigido al comercio electrónico junto con el ID de la transacción y el estado de la misma, para que se genere el mensaje correspondiente.
                    <br>En caso de ser una transferencia si es realizada 
                    entre bancos distintos en un día hábil cualquiera, se hará efectiva al siguiente 
                    día hábil. 
                    <br>En caso de ser una trasferencia al mismo banco o un depósito bancario, 
                    se hará efectiva en un periodo no mayor a 30 minutos.</td>
            </tr>
            <tr class="even">
                <td style="width: 60px">Botón Volver</td>
                <td>No aplica</td>
                <td>Es el botón utilizado por el cliente para retornar a la página de selección de los productos 
                    o servicios a pagar, en caso de que desee realizar algún cambio o verificar su transacción.</td>
            </tr>

        </tbody>
    </table>

</div>