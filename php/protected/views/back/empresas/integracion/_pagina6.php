<br>
<p>GlobalPayment cuenta con un servicio web SOAP que permite conocer el estatus actual de una 
    transacción realizada por cualquier cliente.</p>
<p>Para hacer buen uso del servicio Web es necesario tener en cuenta ciertos parametros:</p>

<h6>Parametros para consumir el servicio SOAP</h6>
<ul>
    <li>String <strong>NAMESPACE</strong> = "http://globalr-modulopayment.rhcloud.com"</li>
    <li>String <strong>URL</strong> ="http://globalr-modulopayment.rhcloud.com/servicios/transaccion?ws=1"</li>
    <li>String <strong>METHOD_NAME</strong> = "getTransaccion"</li>
    <li>String <strong>SOAP_ACTION</strong> = "urn:ServiciosControllerwsdl#getTransaccion"</li>
</ul>	
<p>el metodo del servicio Web a ser consumido recibe 4 parámetros de entrada y genera 1 valor
     de retorno</p>
<p><strong>getTransaccion(idEmpresa, password, idAplicacion, idTransaccion)</strong></p>

<p>A continuacion se explican cada uno de estos parametros de entrada y de retorno.</p>

<h6>Parámetros de Entrada</h6>
<div id="yw2" class="grid-view">
<table class="items table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th id="yw2_c0">Parámetro</th><th id="yw2_c1" >Tipo de Dato</th><th id="yw2_c1" >Descripción</th></tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td style="width: 160px">idEmpresa</td>
            <td style="width: 220px">String</td>
            <td>Es el Código único que identifica al comercio electrónico en nuestro sistema.</td>
        </tr>
        <tr class="even">
            <td style="width: 60px">password</td>
            <td>String</td>
            <td>Es el Código que utiliza el comercio para poder obtener acceso a los servicios de nuestro sistema.</td>
        </tr>
        <tr class="odd">
            <td style="width: 60px">idAplicacion</td>
            <td>String</td>
            <td>Es el código de la aplicación de la cual se quiere consultar la transacción.</td>
        </tr>
        <tr class="odd">
            <td style="width: 60px">idTransaccion</td>
            <td>String</td>
            <td>Es el Código que identifica la transacción a ser procesada.</td>
        </tr>
    </tbody>
</table>
</div>
<h6>Valores de retorno</h6>
<div id="yw2" class="grid-view">
<table class="items table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th id="yw2_c0">Valor</th><th id="yw2_c1" >Tipo de Dato</th><th id="yw2_c1" >Descripción</th></tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td style="width: 160px">Status</td>
            <td style="width: 220px">String</td>
            <td>Es el Código correspondiente al estatus que posee la transacción en ese instante, (Ver tabla de Estatus de Transacción).
                <br><br>En caso de ocurrir algun error con los parametros de entrada, el servicio Web retornará
                un codigo de error dependiendo del caso, (Ver tabla de Codigos de Error).
            </td>
        </tr>
    </tbody>
</table>
</div>

<h6>Status de Transacción</h6>
<div id="yw2" class="grid-view">
<table class="items table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th id="yw2_c0">Status</th><th id="yw2_c1" >Descripción</th></tr>
    </thead>
    <tbody>
        <tr class="odd">            
            <td style="width: 160px">Procesando</td>            
            <td>La transacción está siendo evaluada por nuestro sistema.
            </td>
        </tr>
                <tr class="odd">            
            <td style="width: 160px">Aprobada</td>            
            <td>La transacción fue procesada exitosamente y el pago fue realizado.
            </td>
        </tr>
                <tr class="odd">            
            <td style="width: 160px">Denegada</td>            
            <td>La transacción fue Denegada por algun error en los datos de pago o por fondos insuficientes.
            </td>
        </tr>
                <tr class="odd">            
            <td style="width: 160px">Tiempo Expirado</td>            
            <td>El cliente sobrepasó el tiempo permitido para realizar el pago de la transacción.
            </td>
        </tr>
                <tr class="odd">            
            <td style="width: 160px">Error Interno</td>            
            <td>Existió alguna falla interna y no se pudo procesar el pago, en cuyo caso debes comunicarlo con soporte técnico.
            </td>
        </tr>
        
    </tbody>
</table>
</div>
<h6>Codigos de Error</h6>
<div id="yw2" class="grid-view">
<table class="items table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th id="yw2_c0">Código</th><th id="yw2_c1" >Descripción</th></tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td style="width: 160px">ERW1</td>            
            <td>idEmpresa suministrado no es correcto o la empresa no está registrada en el sistema.
            </td>
        </tr>
                <tr class="odd">
            <td style="width: 160px">ERW2</td>            
            <td>El password suministrado no es correcto.
            </td>
        </tr>
                <tr class="odd">
            <td style="width: 160px">ERW3</td>            
            <td>idAplicacion suministrado no es correcto o la aplicación no está asociada a esta empresa.
            </td>
        </tr>
                <tr class="odd">
            <td style="width: 160px">ERW4</td>            
            <td>idTransaccion suministrado no es correcto o la transacción no está registrada en el sistema.
            </td>
        </tr>
    </tbody>
</table>
</div>
<p>A cotinuación se presenta un ejemplo desarrollado en PHP del consumo del servicio Web:</p>
<p>ini_set (  'soap.wsdl_cache_enable'  ,  0  );<br>
ini_set (  'soap.wsdl_cache_ttl'  ,  0  );<br>
$client = new SoapClient('http://www.globalpayment.com/servicios/transaccion');<br>
$status = $client->getTransaccion('empresa1','password1','aplicacion1','transaccion1');</p>