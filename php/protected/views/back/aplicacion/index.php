<br>
<div class="tituloMenu"><h4>Aplicaciones</h4> </div>        
 <?php
/* @var $this AplicacionController */
/* @var $dataProvider CActiveDataProvider */

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index','active'=>true),
	array('label'=>'Crear', 'url'=>'create'),	
)));
?>
           
    <?php 

$this->widget(
        'bootstrap.widgets.TbExtendedGridView', array(
    //'fixedHeader' => false,
    //'filter'=>$model,
    'ajaxUpdate'=>false, 
    'headerOffset' => 40,
    // 40px is the height of the main navigation at bootstrap
    'type' => 'striped',
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'template' => "{items}{summary}{pager}",   
    'columns' =>array( 
        array('name'=>'codigoAplicacion', 'header'=>'Codigo'),
        array('name'=>'nombre', 'header'=>'Nombre'),
        array('name'=>'uSUARIOS.razonSocial', 'header'=>'Empresa Asociada'), 
        array('name'=>'Transacciones',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link("Ver...", array("reportes/TransaccionesPorApp","id"=>$data->id))'),
        array(            
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'header'=>'Gestionar',
			'template' => '{view} {update}{delete}{caracteristicas}{servicios}',
			'buttons' => array(
			      'view' => array(
					'label'=> 'Ver',
					'options'=>array(
						'class'=>'btn btn-small view'
					)
				),	
                              'update' => array(
					'label'=> 'Actualizar',
                                        
					'options'=>array(
						'class'=>'btn btn-small update'
					)
				),
				'delete' => array(
					'label'=> 'Eliminar',
                                        'click'=>'',
					'options'=>array(                                            
						'class'=>'btn btn-small delete'
					)
				),
                            'caracteristicas' => array(
					'label'=> 'Personalizar',
                                        'url'=>'yii::app()->createUrl("aplicacion/caracteristicas",array("id"=>$data->id))',
					'options'=>array(
						'class'=>'btn btn-small delete btn-primary'
					)
				),
                            'servicios' => array(
					'label'=> 'Contratar Servicios',
                                        'url'=>'yii::app()->createUrl("servicio/asignar",array("id"=>$data->id))',
					'options'=>array(
						'class'=>'btn btn-small delete btn-primary',
                                                'style'=>'margin-left:5px'
					),
                                        
				)
			),
            'htmlOptions'=>array('style'=>'width: 40%; text-align:right '),
           ),))
);
?>




