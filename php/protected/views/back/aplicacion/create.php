<br>
<div class="tituloMenu"><h4>Aplicaciones</h4> </div>      
<?php

$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	'Crear',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
	array('label'=>'Crear', 'url'=>'create','active'=>true),	
)));
?>
<br>

                <?php $this->renderPartial('_form', array('model'=>$model,'empresa'=>$empresa,'accion'=>3)); ?> 
