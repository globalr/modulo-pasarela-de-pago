<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */
/* @var $form CActiveForm */
?>
<div class="span12">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aplicacion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
            <br>
		<?php echo $form->textField($model,'nombre',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
             <br>
		<?php echo $form->textField($model,'descripcion',array('size'=>120,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'codigoAplicacion'); ?>
             <br>
		<?php echo $form->textField($model,'codigoAplicacion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'codigoAplicacion'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'urlRetorno'); ?>
             <br>
		<?php echo $form->textField($model,'urlRetorno',array('size'=>120,'maxlength'=>2100)); ?>
		<?php echo $form->error($model,'urlRetorno'); ?>
	</div>
	<div class="row">
            <?php
            
            //acion=1 asignar
            //accion=2 actualizar
            //accion=3 crear
            
            
             if($accion==1){
                 echo $form->hiddenField($empresa,'id'); 
             }else{
                 if($accion==2){
                     echo $form->labelEx($model,'Empresa');  
                     echo '<br>';
                    echo $form->dropDownList($empresa,'razonSocial',CHtml::listData(Usuario::model()->findAll(), 'id', 'razonSocial'),$htmlOptions = array('class'=>'listUi'), array('options' => array($empresa->id=>array('selected'=>true))));
                 }else{
                     if($accion==3){   
                         echo $form->labelEx($model,'Empresa'); 
                         echo '<br>';
                        echo $form->dropDownList($empresa,'razonSocial',CHtml::listData(Usuario::model()->findAll(), 'id', 'razonSocial'),$htmlOptions = array('class'=>'listUi'),array('empty'=>'Seleccionar'));
                     }
                 }
             }
             
//echo $form->dropDownList($empresa,'razonSocial', CHtml::listData($empresa->findAll(), 'id', 'razonSocial')); 
		 //echo $form->textField($empresa,'razonSocial',array('disabled'=>true));          
            echo $form->error($model,'USUARIOS_id'); 
                 ?>
	</div>
        <br>
	<div class="row buttons">
           
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Asignar Aplicación' : 'Guardar',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>