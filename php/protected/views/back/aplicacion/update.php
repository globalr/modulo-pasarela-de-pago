<br>
<div class="tituloMenu"><h4>Aplicaciones - <?php echo $model->nombre?></h4> </div>  
<?php
$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	'Editar',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../index'),
        array('label'=>'Ver', 'url'=>yii::app()->createUrl("aplicacion/view",array("id"=>$model->id))),
        array('label'=>'Editar', 'url'=>'#','active'=>true),
	array('label'=>'Ver Caracteristicas', 'url'=>yii::app()->createUrl("aplicacion/caracteristicas",array("id"=>$model->id))),
)));

?>
<br>

               <?php $this->renderPartial('_form', array('model'=>$model,'empresa'=>$empresa,'accion'=>2)); ?>


