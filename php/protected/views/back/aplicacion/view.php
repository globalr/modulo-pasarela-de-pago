<br>
<div class="tituloMenu"><h4>Aplicaciones - <?php echo $model->nombre?></h4> </div>  
<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	$model->nombre,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
	array('label'=>'Ver', 'url'=>'#','active'=>true),
        array('label'=>'Editar', 'url'=>yii::app()->createUrl("aplicacion/update",array("id"=>$model->id))),
        array('label'=>'Eliminar', 'url'=>yii::app()->createUrl("aplicacion/delete",array("id"=>$model->id))),
	array('label'=>'Ver Caracteristicas', 'url'=>yii::app()->createUrl("aplicacion/caracteristicas",array("id"=>$model->id))),
        
)));
?>

<br>

                <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                            'type'=>'striped borderd ',
                            'data'=>$model,
                            'attributes'=>array(		
                                    'codigoAplicacion',		
                                    'nombre',
                                    'descripcion',
                                    'urlRetorno',
                                array('name'=>'uSUARIOS.razonSocial',
                                    'type'=>'raw',
                                    'value'=>CHtml::link(CHtml::encode($model->uSUARIOS->razonSocial), array("adminEmpresa/view","id"=>$model->uSUARIOS->id)),
                                    'label'=>'Empresa'
                                    ),
                                array(
                                    'type'=>'raw',
                                    'value'=>CHtml::link(CHtml::encode('Ver Todas'), array("reportes/TransaccionesPorApp","id"=>$model->id)),
                                    'label'=>'Transacciones'
                                    ),
                                array(
                                    'type'=>'raw',
                                    'value'=>CHtml::link(CHtml::encode('Ver'), array("servicio/asignar","id"=>$model->id)),
                                    'label'=>'Servicios Asignados'
                                    ),


                            ),
                    )); 
                    ?>  


