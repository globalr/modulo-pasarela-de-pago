<br>
<div class="tituloMenu"><h4>Aplicaciones - <?php echo $model->nombre?></h4> </div>  
<?php
$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	'Caracteristicas',
);


$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../index'),
        array('label'=>'Ver', 'url'=>yii::app()->createUrl("aplicacion/view",array("id"=>$model->id))),
	array('label'=>'Editar', 'url'=>yii::app()->createUrl('aplicacion/update',array('id'=>$model->id))),
	array('label'=>'Caracteristicas', 'url'=>'#','active'=>true),
)));
?>

<br>
<div class="col-md-4">
    <div class="panel panel-success">
        <div class="panel-heading">
            Logo
        </div>
        <div class="panel-body">

            <a href="#" class="thumbnail">
                <img src="<?php echo yii::app()->baseUrl . '/images/' . $model->nombre . '/' . $model->logo; ?>" 
                     alt="Logo de tu Empresa" 
                     onerror="this.src='<?php echo yii::app()->baseUrl . '/images/default.png'; ?>'">
            </a>

        </div>
        <div class="panel-footer">
            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'buttonText' => 'cargar Logo',
                'id' => 'cocowidget1',
                'onCompleted' => 'function(id,filename,jsoninfo){ alert("Cargado correctamente"); location.reload(true); }',
                'onCancelled' => 'function(id,filename){ alert("Cancelado por el usuario"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'),
                'sizeLimit' => 2000000,
                'uploadDir' => 'images/' . $model->nombre,
                'replaceFile' => true,
                // para recibir el archivo subido:
                'receptorClassName' => 'application.models.Aplicacion',
                'methodName' => 'cargarImagen',
                'userdata' => $model->primaryKey,
                'multipleFileSelection' => false,
                'defaultControllerName' => 'aplicacion',
                'defaultActionName' => 'coco',
                'filename' => 'logo',
                'dropFilesText' => 'Drop Files Here !',
                'htmlOptions' => array('style' => 'width: 100%;'),
            ));
            ?>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="panel panel-success">
        <div class="panel-heading">
            Fondo
        </div>
        <div class="panel-body">
            <a href="#" class="thumbnail">
                <img src="<?php echo yii::app()->baseUrl . '/images/' . $model->nombre . '/' . $model->fondo; ?>" 
                     alt="Logo de tu Empresa" 
                     onerror="this.src='<?php echo yii::app()->baseUrl . '/images/default.png'; ?>'">
            </a>
        </div>
        <div class="panel-footer">
            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'buttonText' => 'cargar Fondo',
                'id' => 'cocowidget2',
                'onCompleted' => 'function(id,filename,jsoninfo){ alert("Cargado correctamente"); location.reload(true); }',
                'onCancelled' => 'function(id,filename){ alert("Cancelado por el usuario"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'),
                'sizeLimit' => 2000000,
                'uploadDir' => 'images/' . $model->nombre,
                'replaceFile' => true,
                // para recibir el archivo subido:
                'receptorClassName' => 'application.models.Aplicacion',
                'methodName' => 'cargarImagen',
                'userdata' => $model->primaryKey,
                'multipleFileSelection' => false,
                'defaultControllerName' => 'aplicacion',
                'defaultActionName' => 'coco',
                'filename' => 'fondo',
                'dropFilesText' => 'Drop Files Here !',
                'htmlOptions' => array('style' => 'width: 100%;'),
            ));
            ?>
        </div>
    </div>
</div>

