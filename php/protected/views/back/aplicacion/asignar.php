<br>
<div class="tituloMenu"><h4>Empresas - <?php echo $empresa->razonSocial; ?></h4> </div>   
<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	'Asignar Aplicacion',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>yii::app()->createUrl('adminEmpresa/lista')),
        array('label' => 'Ver', 'url' => yii::app()->createUrl('adminEmpresa/view', array('id' => $empresa->id))),
	array('label'=>'Editar', 'url'=>yii::app()->createUrl('adminEmpresa/update',array('id'=>$empresa->id))),
	array('label'=>'Asignar Aplicacion', 'url'=>'#','active'=>true),
)));
?>
<br>

                <?php $this->renderPartial('_form', array('model'=>$model,'empresa'=>$empresa,'accion'=>1)); ?>
