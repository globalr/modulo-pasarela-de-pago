<br>
<div class="tituloMenu"><h4>Aplicaciones - <?php echo $model->nombre?></h4> </div>  
<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	$model->nombre,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
	array('label'=>'Ver', 'url'=>yii::app()->createUrl("aplicacion/view",array("id"=>$model->id))),
        array('label'=>'Editar', 'url'=>yii::app()->createUrl("aplicacion/update",array("id"=>$model->id))),
        array('label'=>'Eliminar', 'url'=>'#','active'=>true),
	array('label'=>'Ver Caracteristicas', 'url'=>yii::app()->createUrl("aplicacion/caracteristicas",array("id"=>$model->id))),
        
)));
?>

<br>
<?php if($servicios):?>  
    <div class="alert alert-block alert-danger" >
        <h6><strong>ATENCIÓN</strong></h6> Esta Aplicacion tiene Servicios asociados. Primero debe revocar los servicios asociados antes de borrar una aplicación..&nbsp;&nbsp;&nbsp;&nbsp;
              
        <div class="float-right"><a class="btn btn-primary" href="<?php echo yii::app()->createUrl('servicio/asignar',array('id'=>$model->id));?>">Revocar Servicios</a></div>
    </div> 
<?php elseif($transacciones):?>  
    <div class="alert alert-block alert-danger" >
        <h6><strong>ATENCIÓN</strong></h6> Esta Aplicacion tiene Transacciones asociadas. Si continua se eliminaran todas estas transacciones del sistema.&nbsp;&nbsp;&nbsp;&nbsp;
        <br><br>
        <a class="btn btn-primary" href="<?php echo yii::app()->createUrl('reportes/transaccionesPorApp',array('id'=>$model->id));?>">Ver Transacciones</a>
        <div class="float-right"><a class="btn btn-danger" href="<?php echo yii::app()->createUrl('aplicacion/deleteAplicacion',array('id'=>$model->id));?>">Eliminar</a></div>
    </div> 
<?php else:?> 
<div class="alert alert-block alert-info" >
    <span  style="font-size: 20px; font-weight: bold; text-transform: uppercase;">Está usted seguro.</span> &nbsp;&nbsp;&nbsp;si procede se eliminará la Aplicacion del sistema.
    <div class="float-right"> <a class="btn btn-primary" href="<?php echo yii::app()->createUrl('aplicacion/deleteAplicacion',array('id'=>$model->id));?>">Eliminar</a></div>
    
    </div> 
<?php endif;?> 