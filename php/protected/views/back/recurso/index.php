<br>
<div class="tituloMenu"><h4>Recursos</h4> </div> 
<?php
/* @var $this RecursoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Recursos',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index','active'=>true),
	array('label'=>'Añadir', 'url'=>'create'),	
)));
?>

            <?php
            $this->widget(
                    'bootstrap.widgets.TbExtendedGridView', array(
                //'fixedHeader' => true,
                //'filter'=>$model,
                'headerOffset' => 40,
                // 40px is the height of the main navigation at bootstrap
                'type' => 'striped',
                //'sortableRows' => true,
                'dataProvider' => $model->search(),
                'responsiveTable' => true,
                'template' => "{items}{summary}{pager}", 
                'columns' => array(array('name' => 'id', 'header' => 'Id'),
                    array('name' => 'Nombre', 'header' => 'Nombre'),
                    array('name' => 'tipo', 'header' => 'Tipo'),
                    array('name' => 'descripcion', 'header' => 'Descripcion'),
                    array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header' => 'Gestionar',
                        'template' => '{view} {update} {asignar}',
                        'buttons' => array(
                            'view' => array(
                                'label' => 'Ver',
                                'imageutl' => '',
                                'options' => array(
                                    'class' => 'btn btn-small'
                                ),
                            ),
                            'update' => array(
                                'label' => 'Actualizar',
                                'options' => array(
                                    'class' => 'btn btn-small update '
                                )
                            ),
                            'delete' => array(
                                'label' => 'Eliminar',
                                'options' => array(
                                    'class' => 'btn btn-small delete '
                                )
                            ),
                            'asignar' => array(
                                'label' => 'Asignar',
                                'url' => 'yii::app()->createUrl("recurso/asignarRecurso",array("id"=>$data->id))',
                                'options' => array(
                                    'class' => 'btn btn-small delete btn-primary'
                                )
                            )
                        ),
                        'htmlOptions' => array('style' => 'width: 220px; text-align:right'),
                    ),))
            );
            ?> 




