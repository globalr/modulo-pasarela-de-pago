<br>
<div class="tituloMenu"><h4>Recursos</h4> </div> 
<?php
/* @var $this RecursoController */
/* @var $model Recurso */

$this->breadcrumbs=array(
	'Recursos'=>array('index'),
	'Añadir',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
	array('label'=>'Añadir', 'url'=>'#','active'=>true),	
)));
?>
<br>

                <?php $this->renderPartial('_form', array('model'=>$model)); ?> 
