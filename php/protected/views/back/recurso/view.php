<br>
<div class="tituloMenu"><h4>Recursos - <?php echo $model->Nombre?></h4> </div> 
<?php
/* @var $this RecursoController */
/* @var $model Recurso */

$this->breadcrumbs=array(
	'Recursos'=>array('index'),
	$model->Nombre,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
        array('label'=>'Ver', 'url'=>'#','active'=>true),
        array('label'=>'Editar', 'url'=>yii::app()->createUrl('recurso/update',array('id'=>$model->id))),
)));
?>
<br>

               <?php $this->widget('bootstrap.widgets.TbDetailView', array(
        'type'=>'striped borderd ',
	'data'=>$model,
	'attributes'=>array(		
		'id',
		'Nombre',
		'tipo',
		'descripcion',
		array('value'=>CHtml::link(CHtml::encode($model->url), array('recurso/download','url'=>$model->url)),'label'=>'Recurso','type'=>'raw'),
		//array('name'=>'servicioHasRecursos.id','label'=>'Servicio'),
	),
)); ?>
<br>
<div class="tituloMenu"><h6>Servicios Asignados</h6></div>
<?php
            if ($serviciosAsignados)
                $this->widget(
                        'bootstrap.widgets.TbExtendedGridView', array(
                    'fixedHeader' => true,
                    //'filter'=>$model,
                    'headerOffset' => 40,
                    // 40px is the height of the main navigation at bootstrap
                    'type' => 'striped',
                    'sortableRows' => true,
                    'dataProvider' => new CArrayDataProvider($serviciosAsignados),
                    'responsiveTable' => true,
                    'template' => "{items}{summary}{pager}", 
                    'columns' => array(
                        //array('name' => 'id', 'header' => 'ID'),
                        //array('name' => 'tipo', 'header' => 'Servicio'),
                        
                        array('name'=>'Servicio',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link($data->servicio->tipo, array("servicio/view","id"=>$data->servicio->id))'),
        
                        array('name' => 'servicio.precio', 'header' => 'Precio'),
                        array('name' => 'servicio.descripcion', 'header' => 'Descripcion'),
                    ))
                );
            else
                echo 'No hay Recursos asignados';
            ?> 



