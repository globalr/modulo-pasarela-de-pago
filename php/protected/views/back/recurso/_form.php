<?php
/* @var $this RecursoController */
/* @var $model Recurso */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'recurso-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con  <span class="required">*</span> son requeridos.</p>
<div class="span6">
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
            <br>
		<?php echo $form->textField($model,'Nombre',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'Nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo'); ?>
            <br>
		<?php echo $form->textField($model,'tipo',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
            <br>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>4)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

        <div class="row">
		<?php echo $form->hiddenfield($model,'url',array('id'=>'url')); ?>

	</div>
        
	<div class="row">
          
            <?php
$this->widget('ext.coco.CocoWidget'
        ,array(
            'buttonText'=>'Adjuntar Archivo',
            'id'=>'cocowidget3',
            'onCompleted'=>'function(id,filename,jsoninfo){document.getElementById("url").value="images/recursos/"+filename;}',
            'onCancelled'=>'function(id,filename){ alert("Cancelado por el usuario"); }',
            'onMessage'=>'function(m){ alert(m); }',
            'allowedExtensions'=>array('jpeg','jpg','pdf','csv','php'),
            'sizeLimit'=>2000000,
            'uploadDir' => 'images/recursos',
            'replaceFile'=>false,
            // para recibir el archivo subido:
            'receptorClassName'=>'application.models.Recurso',
            'methodName'=>'cargarRecurso',
            'userdata'=>$model->primaryKey,
            'multipleFileSelection'=>false,
            'defaultControllerName'=>'recurso',
            'defaultActionName'=>'coco',  
            //'filename'=> 'archivo',
            'dropFilesText'=>'Drop Files Here !',
            'htmlOptions'=>array('style'=>'width: 100%;'),
        ));
        
   ?>
            
         
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
</div><!-- form -->