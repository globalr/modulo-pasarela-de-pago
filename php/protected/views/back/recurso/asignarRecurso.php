<br>
<div class="tituloMenu"><h4>Recursos - <?php echo $model->Nombre?></h4> </div> 
<?php
$this->breadcrumbs=array(
	'Recursos',
);


$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../index'),
	array('label'=>'Añadir', 'url'=>'../create'),
        array('label'=>'Asignar', 'url'=>'#','active'=>true),
)));


?>
<h6>Asignar <?php echo $model->Nombre;?> a:</h6>
<?php

$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
        'type' => 'striped bordered',
        'dataProvider' => $servicios->search(),
        'template' => "{items}{summary}{pager}", 
        'columns' => array(
            array('name' => 'id', 'header' => 'ID'),
            array('name' => 'tipo', 'header' => 'Servicio'),
            array('name' => 'descripcion', 'header' => 'Descripcion'),
            array(
                'class' => 'bootstrap.widgets.TbToggleColumn',
                'toggleAction' => 'seleccionar',                
                'name'=>$model->id,
                'value'=> array(ServicioHasRecurso::model(),'recursoAsignado'), 
                'checkedButtonLabel'=>'Asignar',
                'uncheckedButtonLabel'=>'Revocar',  
                'displayText'=>true,
                'htmlOptions'=>array('style'=>'width: 120px'),
                'header' => 'Asignar'
            ),
        )
    )
);
?>
