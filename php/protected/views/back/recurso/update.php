<br>
<div class="tituloMenu"><h4>Recursos - <?php echo $model->Nombre?></h4> </div> 
<?php
/* @var $this RecursoController */
/* @var $model Recurso */

$this->breadcrumbs=array(
	'Recursos'=>array('index'),
	$model->Nombre,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../index'),
	array('label'=>'Ver', 'url'=>yii::app()->createUrl('recurso/view',array('id'=>$model->id))),
        array('label'=>'Editar', 'url'=>'#','active'=>true),
)));

?>
<br>

                <?php $this->renderPartial('_form', array('model'=>$model)); ?>

