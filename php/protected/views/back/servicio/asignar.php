<br>
<div class="tituloMenu"><h4>Servicios</h4> </div> 
<?php

$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	'Asignar Servicios',
);
echo '<br>';
$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../index'),
	array('label'=>'Asignar', 'url'=>'#','active'=>true),	
)));

?>


<h6>Asignar Servicios a: <?php echo $model->nombre;?></h6>
<?php

$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
        'type' => 'striped bordered',
        'dataProvider' => $servicios->search(),
        'template' => "{items}{summary}{pager}",
        'columns' => array(
            array('name' => 'id', 'header' => 'ID'),
            array('name' => 'tipo', 'header' => 'Nombre'),
            array('name' => 'descripcion', 'header' => 'Descripcion'),
            array(
                'class' => 'bootstrap.widgets.TbToggleColumn',
                'toggleAction' => 'seleccionar',                
                'name'=>$model->id,
                'value'=> array(ServicioHasAplicacion::model(),'appAsignada'), 
                'checkedButtonLabel'=>'Asignar',
                'uncheckedButtonLabel'=>'Revocar',  
                'displayText'=>true,
                'htmlOptions'=>array('style'=>'width: 120px'),
                'header' => 'Asignar'
            ),
        )
    )
);
?>
