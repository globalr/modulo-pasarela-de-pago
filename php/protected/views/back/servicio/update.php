<br>
<div class="tituloMenu"><h4>Servicios - <?php echo $model->tipo?></h4> </div>
<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),	
	$model->tipo,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../index'),
        array('label' => 'Ver', 'url' => yii::app()->createUrl('servicio/view', array('id' => $model->id))),
	array('label'=>'Editar', 'url'=>'#','active'=>true),

)));
	?>
<br>

                <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>



