<br>
<div class="tituloMenu"><h4>Servicios - <?php echo $model->tipo?></h4> </div> 
<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	$model->tipo,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
	array('label'=>'Ver', 'url'=>'#','active'=>true),
        array('label' => 'Editar', 'url' => yii::app()->createUrl('servicio/update', array('id' => $model->id))),
   

)));
?>


<br>
<div class="tituloMenu"> <h6>Aplicaciones Asignadas</h6></div>

            <?php
            if ($aplicacionesAsignadas)
                $this->widget(
                        'bootstrap.widgets.TbExtendedGridView', array(
                    'fixedHeader' => true,
                    //'filter'=>$model,
                    'headerOffset' => 40,
                    // 40px is the height of the main navigation at bootstrap
                    'type' => 'striped',
                    'sortableRows' => true,
                    'dataProvider' => new CArrayDataProvider($aplicacionesAsignadas),
                    'responsiveTable' => true,
                    'template' => "{items}{summary}{pager}", 
                    'columns' => array(
                        //array('name' => 'id', 'header' => 'ID'),
                        //array('name' => 'tipo', 'header' => 'Servicio'),
                        array('name'=>'Aplicacion',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link($data->aplicacion->nombre, array("aplicacion/view","id"=>$data->aplicacion->id))'),
        
                        
                        array('name' => 'aplicacion.uSUARIOS.razonSocial', 'header' => 'Empresa'),
                        array('name' => 'fechaContratacion', 'header' => 'Fecha deContratacion'),
                        array('name' => 'precio', 'header' => 'Precio'),
                    ))
                );
            else
                echo 'No hay Aplicaciones asignadas';
            ?> 
<br>
<div class="tituloMenu"><h6>Recursos Asignados</h6></div>


            <?php
            if ($recursosAsignados)
                $this->widget(
                        'bootstrap.widgets.TbExtendedGridView', array(
                    'fixedHeader' => true,
                    //'filter'=>$model,
                    'headerOffset' => 40,
                    // 40px is the height of the main navigation at bootstrap
                    'type' => 'striped',
                    'sortableRows' => true,
                    'dataProvider' => new CArrayDataProvider($recursosAsignados),
                    'responsiveTable' => true,
                    'template' => "{items}{summary}{pager}", 
                    'columns' => array(
                        //array('name' => 'id', 'header' => 'ID'),
                        //array('name' => 'tipo', 'header' => 'Servicio'),
                        array('name'=>'Recurso',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link($data->recurso->Nombre, array("recurso/view","id"=>$data->recurso->id))'),
        
                        array('name' => 'recurso.Nombre', 'header' => 'Recurso'),
                        array('name' => 'recurso.tipo', 'header' => 'Tipo'),
                        array('name' => 'recurso.descripcion', 'header' => 'Descripcion'),
                    ))
                );
            else
                echo 'No hay Recursos asignados';
            ?> 
