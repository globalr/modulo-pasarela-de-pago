<br>
<div class="tituloMenu"><h4>Servicios</h4> </div> 
<?php
$this->breadcrumbs=array(
	'Servicios',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index','active'=>true),
	array('label'=>'Crear', 'url'=>'create'),

)));

?>


            <?php
            $this->widget(
                    'bootstrap.widgets.TbExtendedGridView', array(
                //'fixedHeader' => true,
                //'filter'=>$model,
                'headerOffset' => 40,
                // 40px is the height of the main navigation at bootstrap
                'type' => 'striped',
                //'sortableRows' => true,
                'dataProvider' => $model->search(),
                'responsiveTable' => true,
                'template' => "{items}{summary}{pager}", 
                'columns' => array(array('name' => 'id', 'header' => 'Id'),
                    array('name' => 'tipo', 'header' => 'Tipo'),
                    array('name' => 'precio', 'header' => 'Precio'),
                    array('name' => 'descripcion', 'header' => 'Descripcion'),
                    array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header' => 'Gestionar',
                        'template' => '{view} {update} ',
                        'buttons' => array(
                            'view' => array(
                                'label' => 'Ver',
                                'imageutl' => '',
                                'options' => array(
                                    'class' => 'btn btn-small'
                                ),
                            ),
                            'update' => array(
                                'label' => 'Actualizar',
                                'options' => array(
                                    'class' => 'btn btn-small update '
                                )
                            ),
                            'delete' => array(
                                'label' => 'Eliminar',
                                'options' => array(
                                    'class' => 'btn btn-small delete '
                                )
                            ),
                            'asignar' => array(
                                'label' => 'Asignar Servicio',
                                'url' => 'yii::app()->createUrl("servicio/asignar",array("id"=>$data->id))',
                                'options' => array(
                                    'class' => 'btn btn-small delete btn-info'
                                )
                            )
                        ),
                        'htmlOptions' => array('style' => 'width: 200px; text-align:right;'),
                    ),))
            );
            ?>




