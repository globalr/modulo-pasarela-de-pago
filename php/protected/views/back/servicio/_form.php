<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'servicio-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Los campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>

        <?php echo $form->labelEx($model,'tipo'); ?>
             <br>
	<?php echo $form->textField($model,'tipo',array('maxlength'=>20)); ?>
        <?php echo $form->error($model,'tipo'); ?>
             <br>
        <?php echo $form->labelEx($model,'precio'); ?>
             <br>
	<?php echo $form->textField($model,'precio'); ?>
        <?php echo $form->error($model,'precio'); ?>
             <br>
        <?php echo $form->labelEx($model,'descripcion'); ?>
             <br>
        <?php echo $form->textArea($model,'descripcion',array('rows' => 5)); ?>
	<?php //echo $form->textField($model,'descripcion',array('class'=>'span4','maxlength'=>200)); ?>
        <?php echo $form->error($model,'descripcion'); ?>


            <br>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary')); ?>

<?php $this->endWidget(); ?>
