<br>
<div class="tituloMenu"><h4>Servicios</h4> </div> 
<?php
$this->breadcrumbs=array(
	'Servicios'=>array('index'),
	'Crear',
);


$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'index'),
	array('label'=>'Crear', 'url'=>'create','active'=>true),

)));
?>
<br>

                <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
