<br>
<div class="tituloMenu"><h4>Reportes</h4> </div> 
<?php
$this->breadcrumbs = array(
    'Reportes' => array('reportes'),
    'Graficos' => array('reportes'),
);

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Gráficos', 'url' => yii::app()->createUrl('reportes/reportes')),
        array('label' => 'Lista de Transacciones', 'url' => yii::app()->createUrl('reportes/listas'), 'active' => true),
)));
?>

<div class="col-md-3">
    Empresa
<?php
echo CHtml::dropDownList('empresaId', '', CHtml::listData(Usuario::model()->findAll(), 'id', 'razonSocial'), array(
    'prompt' => 'Todas',
    'class'=>'select-admin',
    'ajax' => array(
        'type' => 'POST', //request type
        'url' => CController::createUrl('reportes/actualizarAplicacionAjax'), //url to call.
//Style: CController::createUrl('currentController/methodToCall')
        'update' => '#aplicacionId', //selector to update
        'data' => array('empresaId' => 'js:this.value'),
//'data'=>'js:javascript statement' 
//leave out the data key to pass all form values through
)));
?>

</div>

<div class="col-md-3">
    Aplicación
<?php echo CHtml::dropDownList('aplicacionId', '', array(), array('prompt' => 'Todas','class'=>'select-admin')); ?>
</div>
<div class="col-md-3">
    Tipo de Pago
<?php echo CHtml::dropDownList('tipoPago', '',ENUMHtml::enumItem(Transaccion::model(), 'codigoPago'),array('prompt' => 'Todos','class'=>'select-admin'));?>
</div>
<div class="col-md-2">
    Estatus
<?php echo CHtml::dropDownList('estado', '', ENUMHtml::enumItem(Transaccion::model(), 'STATUS_PAYMENT_id'),array('prompt' => 'Todos','class'=>'select-admin'));?>
</div>
<div class="col-md-1">
    &nbsp;
<?php echo CHtml::ajaxButton("Buscar", Yii::app()->createUrl("reportes/ajaxTransacciones"),
        array(
            'data' => array(
                'empresaId' => 'js: $("#empresaId option:selected").val()',
                'aplicacionId' => 'js: $("#aplicacionId option:selected").val()',
                'tipoPago' => 'js: $("#tipoPago option:selected").val()',
                'estado' => 'js: $("#estado option:selected").val()',),
            
            'update' => "#transacciones",
            'beforeSend' => 'function(){$("#loading").addClass("visibleloading");}',
            'complete' => 'function(){$("#loading").removeClass("visibleloading");}'),
        array('class' => 'btn btn-primary'));
?>
</div>

<div id="transacciones" class="col-md-12">
    <?php
    if ($model)
        $this->widget(
                'bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'gridServicio',
            'fixedHeader' => true,
            //'filter' => $model,
            'headerOffset' => 40, // 40px is the height of the main navigation at bootstrap
            'type' => 'striped',
            //'sortableRows' => true,
            'dataProvider' => $model,
            'responsiveTable' => true,
            'template' => "{items}{summary}{pager}",
            'columns' => array(
                array('name'=>'codigoTransaccion',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link("$data->codigoTransaccion", array("solicitudes/detalleDeposito","id"=>$data->id))'),
                array('name' => 'aPLICACION.nombre', 'header' => 'Aplicacion'),
                array('name' => 'rifCliente', 'header' => 'Cliente'),
                array('name' => 'fechaCreacion', 'header' => 'Fecha - Hora'),
                array('name' => 'codigoPago', 'header' => 'Tipo Pago'/* ,'filter'=>CHtml::listData(Transaccion::model()->findAll(), 'codigoPago', 'codigoPago') */),
                array('name' => 'monto', 'header' => 'Monto Pago'),
                array('name' => 'STATUS_PAYMENT_id', 'header' => 'Estado'),
            ))
        );
    else{
        echo'<br>';        
        echo 'No hay Transacciones en estos momentos.';
    }
    ?>   
</div>