    <?php
    if ($model)
        $this->widget(
                'bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'gridServicio',
            'fixedHeader' => true,
            //'filter' => $model,
            'headerOffset' => 40, // 40px is the height of the main navigation at bootstrap
            'type' => 'striped',
            //'sortableRows' => true,
            'dataProvider' => new CArrayDataProvider($model),
            'responsiveTable' => true,
            'template' => "{items}{summary}{pager}",
            'columns' => array(
                array('name'=>'codigoTransaccion',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link("$data->codigoTransaccion", array("solicitudes/detalleDeposito","id"=>$data->id))'),
                
                array('name' => 'aPLICACION.nombre', 'header' => 'Aplicacion'),
                array('name' => 'rifCliente', 'header' => 'Cliente'),
                array('name' => 'fechaCreacion', 'header' => 'Fecha - Hora'),
                array('name' => 'codigoPago', 'header' => 'Tipo Pago'/* ,'filter'=>CHtml::listData(Transaccion::model()->findAll(), 'codigoPago', 'codigoPago') */),
                array('name' => 'monto', 'header' => 'Monto Pago'),
                array('name' => 'STATUS_PAYMENT_id', 'header' => 'Estado'),
            ))
        );
    else{
        echo'<br>';
        echo 'No se consiguieron coincidencias.';
    }
    ?>   
