<br>
<div class="span10">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Solicitud de Servicios
            </div>
            <div class="panel-body">
                <?php
                if ($solicitudServicios)
                    $this->widget(
                        'bootstrap.widgets.TbExtendedGridView', array(
                            'id'=>'gridServicio',
                        'fixedHeader' => true,
                        //'filter'=>$model,
                        'headerOffset' => 40,
                        // 40px is the height of the main navigation at bootstrap
                        'type' => 'striped',
                        'sortableRows' => true,
                        'dataProvider' => new CArrayDataProvider($solicitudServicios),
                        'responsiveTable' => true,
                        'template' => "{items}",
                        'columns' => array(
                            //array('name' => 'id', 'header' => 'ID'),
                            //array('name' => 'tipo', 'header' => 'Servicio'),
                            array('name' => 'servicio.tipo', 'header' => 'Servicio'),
                            array('name' => 'aplicacion.nombre', 'header' => 'Aplicacion'),
                            array('name' => 'aplicacion.uSUARIOS.razonSocial', 'header' => 'Empresa'), 
                            array('name' => 'fechaContratacion', 'header' => 'FechaSolicitud'),
                            array(            
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'header'=>'Accion',
                                                'template' => '{Aprobar} {Rechazar} ',
                                                'buttons' => array(                                                      
                                                    'Aprobar' => array(
                                                        'label'=>'Aprobar',                                                        
                                                        'url'=>'Yii::app()->createUrl("solicitudes/ajaxAprobarServicio", array("id"=>$data->id,"opcion"=>2))',
                                                        'options' => array( 'ajax' => array('type' => 'post', 'url'=>'js:$(this).attr("href")', 'success' => 'js:function(data) { $.fn.yiiGridView.update("gridServicio")}'),
                                                           'class'=>'btn btn-small  btn-info' ),
                                                    ),
                                                    'Rechazar' => array(
                                                        'label'=>'Rechazar',                                                        
                                                        'url'=>'Yii::app()->createUrl("solicitudes/ajaxAprobarServicio", array("id"=>$data->id,"opcion"=>3))',
                                                        'options' => array( 'ajax' => array('type' => 'post', 'url'=>'js:$(this).attr("href")', 'success' => 'js:function(data) { $.fn.yiiGridView.update("gridServicio")}'),
                                                           'class'=>'btn btn-small  btn-warning' ),
                                                    ),
                                                ), 
                                           'htmlOptions'=>array('style'=>'width: 210px'),
                                   )
                            
                        ))
                    );
                else
                    echo 'No hay Solicitudes en estos momentos.';
                ?>    
            </div>
            <div class="panel-footer">  
            </div>
            <div style="clear: both"></div>   
            </div>
        </div>
    </div>
