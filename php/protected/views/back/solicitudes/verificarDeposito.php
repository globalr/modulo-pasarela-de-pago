<br>
<div class="tituloMenu"><h4>Solicitudes - Aprobar Depositos</h4> </div> 
<?php 
$this->pageTitle=Yii::app()->name . ' - Depositos';
$this->breadcrumbs=array(
	'Solicitudes','Depositos'
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
        array('label'=>'Registro de Empresas', 'url'=>'#'),
	//array('label'=>'Adicion de Aplicaciones', 'url'=>'lista'),
	//array('label'=>'Contratacion de Servicios', 'url'=>'adicionServicio'),
        array('label'=>'Aprobacion de Depositos y Transferencia', 'url'=>'verificarDeposito','active'=>true),



)));
?>


                <?php
                if ($solicitudDeposito)
                    $this->widget(
                        'bootstrap.widgets.TbExtendedGridView', array(
                        'id'=>'gridServicio',
                        'fixedHeader' => true,
                        //'filter'=> Transaccion::model(),
                        'headerOffset' => 40,// 40px is the height of the main navigation at bootstrap
                        'type' => 'striped',
                        'sortableRows' => true,
                        'dataProvider' => new CArrayDataProvider($solicitudDeposito),
                        'responsiveTable' => true,
                        'template' => "{items}{summary}{pager}",          
                        'columns' => array(
                            //array('name' => 'id', 'header' => 'ID'),
                            //array('name' => 'tipo', 'header' => 'Servicio'),
                            array('name' => 'codigoTransaccion', 'header' => 'Codigo'),
                            array('name' => 'fechaCreacion', 'header' => 'Fecha - Hora'),
                            array('name' => 'monto', 'header' => 'Monto'),
                            array('name' => 'codigoPago', 'header' => 'Tipo de pago'),
                            array('name' => 'STATUS_PAYMENT_id', 'header' => 'Estado'),
                            array(            
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'header'=>'Accion',
                                                'template' => '{Procesar}',
                                                'buttons' => array(                                                      
                                                    'Procesar' => array(
                                                        'label'=>'Procesar',                                                        
                                                        'url'=>'Yii::app()->createUrl("solicitudes/detalleDeposito", array("id"=>$data->id))',
                                                        'options' => array( 'class'=>'btn btn-small  btn-primary' ),
                                                    ),
                                                ), 
                                          
                                   )
                            
                        ))
                    );
                else
                    echo 'No hay Solicitudes en estos momentos.';
                ?>    
