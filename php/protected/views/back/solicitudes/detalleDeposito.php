<br>
<div class="tituloMenu"><h4>Solicitudes - Detalle Transacción</h4> </div> 
<?php 
$this->pageTitle=Yii::app()->name . ' - Depositos';
$this->breadcrumbs=array(
	'Solicitudes','Depositos'
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
        //array('label'=>'Registro de Empresas', 'url'=>'#'),
	//array('label'=>'Adicion de Aplicaciones', 'url'=>'lista'),
	//array('label'=>'Contratacion de Servicios', 'url'=>'adicionServicio'),
        array('label'=>'Aprobacion de Depositos y Transferencia', 'url'=> yii::app()->createUrl('solicitudes/verificarDeposito'),'active'=>true),



)));
?>
<br>
<div class="col-md-5">             
                            <?php
                            $this->widget('bootstrap.widgets.TbDetailView', array(
                                'cssFile' => Yii::app()->theme->baseUrl .'/css/profile.css',
                                'type' => 'striped borderd ',
                                'data' => $detalle,
                                'attributes' => array(
                                    array('name'=>'Transaccion','value'=>$deposito->codigoTransaccion),
                                    array('name'=>'Tipo Pago','value'=>$deposito->codigoPago),
                                    array('name'=>'Estado','type'=>'html','value'=>'<span style="font-size:16px;" class="label label-default ">'.$deposito->STATUS_PAYMENT_id.'</span>'),                                    
                                    array('name'=> 'bancoEmisor0.nombre','label'=>'Banco Emisor'),
                                    array('name'=> 'bancoReceptor0.nombre','label'=>'Banco Receptor'),
                                    'fechaPago',
                                    array('name'=>'Rif','value'=>$deposito->rifCliente),
                                    'numeroReferencia',
                                    'monto',                                   

                                ),
                            ));
                            ?> 

    
    </div>  
<div class="col-md-7">
      <?php $this->widget('bootstrap.widgets.TbAlert', array(
                                'block'=>true, // display a larger alert block?
                                'fade'=>true, // use transitions?
                                'closeText'=>false, // close link text - if set to false, no close link is displayed
                                'alerts'=>array( // configurations per alert type
                                    'info'=>array('block'=>true, 'fade'=>true), // success, info, warning, error or danger
                                ),
                            )); ?>
                            <?php
                            
                            $form = $this->beginWidget('CActiveForm', array('id' => 'deposito-form', 'enableAjaxValidation' => false,));
                            echo 'Observaciones<br>';    
                            echo $form->textArea(
                                            $detalle,
                                            'observaciones',
                                            array('class' => 'col-md-12', 'rows' => 5,'id'=>'observacion')
                                        );
                            echo '&nbsp;<br>';
                                echo CHtml::ajaxButton("Aprobar", Yii::app()->createUrl("solicitudes/AjaxAprobarRechazar"), array(
                                    'data' => array('observacion' => 'js: $("#observacion").val()', 'transaccionId' => $deposito->id,'accion'=>'aprobar'),
                                    'update' => "#resultado",
                                    'complete' => 'function(){document.location.reload(true);}'
                                        ), array('class' => 'btn btn-primary','style'=>'float:left; margin-left:20px'));
                                
                                echo CHtml::ajaxButton("Rechazar", Yii::app()->createUrl("solicitudes/AjaxAprobarRechazar"), array(
                                    'data' => array('observacion' => 'js: $("#observacion").val()', 'transaccionId' => $deposito->id,'accion'=>'rechazar'),
                                    'update' => "#resultado",
                                    'complete' => 'function(){document.location.reload(true);}'
                                        ), array('class' => 'btn btn-default','style'=>'float:right; margin-right:20px'));

                            $this->endWidget();                            
                            ?>
                              

</div>
