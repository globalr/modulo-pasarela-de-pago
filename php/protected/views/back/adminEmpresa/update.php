<br>
<div class="tituloMenu"><h4>Empresas - <?php echo $model->razonSocial?></h4> </div> 
<?php
$this->breadcrumbs=array(
	'empresas'=>array('index'),	
	$model->razonSocial,
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'../lista'),
        array('label' => 'Ver', 'url' => yii::app()->createUrl('adminEmpresa/view', array('id' => $model->id))),
	array('label'=>'Editar', 'url'=>'#','active'=>true),
	array('label'=>'Asignar Aplicacion', 'url'=> yii::app()->createUrl('aplicacion/asignar',array('id'=>$model->id))),
)));
?>
<br>

<?php $this->renderPartial('_form', array('model'=>$model)); ?> 


