<br>
<div class="tituloMenu"><h4>Empresas - <?php echo $model->razonSocial?></h4> </div> 
<?php
if (Yii::app()->user->checkAccess('admin')) {
    $this->breadcrumbs=array(
	'Empresa'=>array('index'),
	$model->razonSocial,
);

    $this->widget(
            'bootstrap.widgets.TbMenu', array(
        'type' => 'tabs',
        'items' => array(
            array('label' => 'Lista', 'url' => 'lista'),
            array('label' => 'Ver', 'url' => '#', 'active' => true),
            array('label' => 'Editar', 'url' => yii::app()->createUrl('adminEmpresa/update', array('id' => $model->id))),
            array('label' => 'Asignar Aplicacion', 'url' => yii::app()->createUrl('aplicacion/asignar', array('id' => $model->id))),
    )));
}
?>

                            <?php
                            $this->widget('bootstrap.widgets.TbDetailView', array(
                                'type' => 'striped borderd ',
                                'data' => $model,
                                'attributes' => array(
                                    'nombreUsuario',
                                    'password',
                                    'codigoEmpresa',                                    
                                    'razonSocial',
                                    'responsable',
                                    'rif',
                                    'direccion',
                                    'telefono',
                                    'correo',
                                    array('name' => 'iDIOMA.nombre', 'label' => 'Idioma'),
                                ),
                            ));
                            ?>   

    <div class="tituloMenu"><h6>Aplicaciones Asignadas</h6> </div> 

<?php if (Yii::app()->user->checkAccess('admin')): ?>
                <?php
                if ($aplicacionesAsignadas)
                    $this->widget(
                            'bootstrap.widgets.TbExtendedGridView', array(
                        'fixedHeader' => true,
                        //'filter'=>$model,
                        'headerOffset' => 40,
                        // 40px is the height of the main navigation at bootstrap
                        'type' => 'striped',
                        'sortableRows' => true,
                        'dataProvider' => new CArrayDataProvider($aplicacionesAsignadas),
                        'responsiveTable' => true,
                        'template' => "{items}{summary}{pager}", 
                        'columns' => array(
                            //array('name' => 'id', 'header' => 'ID'),
                            //array('name' => 'tipo', 'header' => 'Servicio'),
                            array('name'=>'codigoAplicacion',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link($data->codigoAplicacion, array("aplicacion/view","id"=>$data->id))'),
                            array('name' => 'nombre', 'header' => 'Nombre'),
                            array('name' => 'descripcion', 'header' => 'Descripcion'),
                        ))
                    );
                else
                    echo 'No hay Aplicaciones asignadas';
                ?>   

<?php endif; ?>