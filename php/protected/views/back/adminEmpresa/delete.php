<br>
<div class="tituloMenu"><h4>Empresas - <?php echo $model->razonSocial?></h4> </div> 
<?php
if (Yii::app()->user->checkAccess('admin')) {
    $this->breadcrumbs=array(
	'Empresa'=>array('Delete'),
	$model->razonSocial,
);

    $this->widget(
            'bootstrap.widgets.TbMenu', array(
        'type' => 'tabs',
        'items' => array(
            array('label' => 'Lista', 'url' => 'lista'),
            array('label' => 'Ver', 'url' => yii::app()->createUrl('adminEmpresa/view', array('id' => $model->id))),
            array('label' => 'Editar', 'url' => yii::app()->createUrl('adminEmpresa/update', array('id' => $model->id))),
            array('label' => 'Eliminar', 'url' =>'#', 'active' => true),
            array('label' => 'Asignar Aplicacion', 'url' => yii::app()->createUrl('aplicacion/asignar', array('id' => $model->id))),
    )));
}
?>
<br>
<?php if($aplicacionesAsignadas):?>  
    <div class="alert alert-block alert-danger" >
        <h6><strong>ATENCIÓN</strong></h6> Esta Empresa tiene aplicaciones asociadas. primero debe elimimnar las aplicacion
        antes de eliminar la empresa.&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-primary" href="<?php echo yii::app()->createUrl('adminEmpresa/view',array('id'=>$model->id));?>">Ver Aplicaciones</a>
    </div> 
<?php else:?> 
<div class="alert alert-block alert-info" >
    <span  style="font-size: 20px; font-weight: bold; text-transform: uppercase;">Está usted seguro.</span> &nbsp;&nbsp;&nbsp;si procede se eliminará la Empresa del sistema.
    <div class="float-right"> <a class="btn btn-primary" href="<?php echo yii::app()->createUrl('adminEmpresa/deleteEmpresa',array('id'=>$model->id));?>">Eliminar</a></div>
    
    </div> 
<?php endif;?> 
