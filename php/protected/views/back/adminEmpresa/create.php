<br>
<div class="tituloMenu"><h4>Empresas</h4> </div> 
<?php
$this->breadcrumbs=array(
	'Empresas'=>array('index'),
	
	'Crear',
);

$this->widget(
    'bootstrap.widgets.TbMenu',
        array(
            'type'=>'tabs',
                'items' =>array(
	array('label'=>'Lista', 'url'=>'lista'),
	array('label'=>'Crear', 'url'=>'create','active'=>true),
)));
?>

<br>
               <?php $this->renderPartial('_form', array('model'=>$model)); ?>
