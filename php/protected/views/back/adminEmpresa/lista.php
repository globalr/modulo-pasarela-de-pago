<br>
<div class="tituloMenu"><h4>Empresas</h4> </div> 
<?php
$this->pageTitle = Yii::app()->name . ' - Empresas';

$this->widget(
        'bootstrap.widgets.TbMenu', array(
    'type' => 'tabs',
    'items' => array(
        array('label' => 'Lista', 'url' => 'lista', 'active' => true),
        array('label' => 'Crear', 'url' => 'create'),
)));
?>
<?php
$this->widget(
        'bootstrap.widgets.TbExtendedGridView', array(
    //'fixedHeader' => true,
    //'filter'=>$model,
    'headerOffset' => 40,
    // 40px is the height of the main navigation at bootstrap
    'type' => 'striped',
    //'sortableRows' => true,
    'dataProvider' => $model->search(),
    'responsiveTable' => true,
    'template' => "{items}{summary}{pager}", 
    'columns' => array(
        array('name' => 'codigoEmpresa', 'header' => 'Codigo'),
        array('name' => 'razonSocial', 'header' => 'Nombre'),
        array('name' => 'rif', 'header' => 'Rif'),
        array('name' => 'responsable', 'header' => 'Representante'),
        array('name' => 'iDIOMA.nombre', 'header' => 'Idioma'),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'header' => 'Gestionar',
            'template' => '{view} {update} {delete}{asignar}',
            'buttons' => array(
                'view' => array(
                    'label' => 'Ver',
                    'imageutl' => '',
                    'options' => array(                        
                        'class' => 'btn btn-small'
                    ),
                ),
                'update' => array(
                    'label' => 'Actualizar',
                    'options' => array(
                        'class' => 'btn btn-small update '
                    )
                ),
                'delete' => array(
                    'label' => 'Eliminar',
                    'click'=>'',
                    'options' => array(                        
                        'class' => 'btn btn-small delete '
                    )
                ),
                'asignar' => array(
                    'label' => 'Asignar Aplicacion',
                    'url' => 'yii::app()->createUrl("aplicacion/asignar",array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn btn-small delete btn-primary'
                    )
                )
            ),
            'htmlOptions' => array('style' => 'width: 300px;  text-align:right'),
        ),))
);
?>




