<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

    <div class="span12">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuario-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los Campos con  <span class="required">*</span> Son obligatorios</p>

	<?php echo $form->errorSummary($model); ?>

	<div class=".col-md-3">
		<?php  echo $form->labelEx($model,'nombreUsuario'); ?>
            <?php  echo '<br>'; ?>
            
		<?php echo  $form->textField($model,'nombreUsuario',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nombreUsuario'); ?>
	</div>

	<div class=".col-md-3">
            <?php  echo $form->labelEx($model,'password'); ?>
           <?php  echo '<br>'; ?>
		<?php echo  $form->textField($model,'password',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'password'); ?>
            

	</div>
        <div class="span3">
		<?php echo $form->labelEx($model,'codigoEmpresa'); ?>
             <br>
		<?php echo $form->textField($model,'codigoEmpresa',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'codigoEmpresa'); ?>
	</div>
	<div class="span3">
		<?php echo $form->labelEx($model,'razonSocial'); ?>
             <br>
		<?php echo $form->textField($model,'razonSocial',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'razonSocial'); ?>
	</div>

	<div class="span3">
		<?php echo $form->labelEx($model,'rif'); ?>
             <br>
		<?php echo $form->textField($model,'rif',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'rif'); ?>
	</div>

	<div class="span3">
		<?php echo $form->labelEx($model,'responsable'); ?>
             <br>
		<?php echo $form->textField($model,'responsable',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'responsable'); ?>
	</div>

	<div class="span3">
		<?php echo $form->labelEx($model,'direccion'); ?>
             <br>
		<?php echo $form->textField($model,'direccion',array('size'=>80,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="span3">
		<?php echo $form->labelEx($model,'telefono'); ?>
             <br>
		<?php echo $form->textField($model,'telefono',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="span3">
		<?php echo $form->labelEx($model,'correo'); ?>
             <br>
		<?php echo $form->textField($model,'correo',array('size'=>60,'maxlength'=>320)); ?>
		<?php echo $form->error($model,'correo'); ?>
	</div>

	<div class="span3">
		<?php echo $form->labelEx($model,'IDIOMA_id'); ?>
             <br>
                <?php echo $form->dropDownList($model, 'IDIOMA_id', array('Seleccione...', 'Español', 'Ingles'),$htmlOptions = array('class'=>'listUi')); ?>
		<?php //echo $form->textField($model,'IDIOMA_id'); ?>
		<?php echo $form->error($model,'IDIOMA_id'); ?>
             
	</div>

	<div class="span3 buttons">
            <br>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
