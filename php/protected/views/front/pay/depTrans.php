<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Global Payment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=""> 
        <meta name="author" content="">
        <!-- Le styles -->        
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/css/payment.css" rel="stylesheet">      
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/js/payment.js"></script>       
    </head>

    <body style="background-image: url('<?php echo Yii::app()->request->baseUrl."/images/".$valoresEstaticos['dirApp']."/fondo.jpg" ?>'); background-size: 100% 100%; background-repeat: no-repeat">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="navbar-brand"  href="#">
                        <img style="height: 60px; margin:5px;" src="<?php echo Yii::app()->request->baseUrl."/images/".$valoresEstaticos['dirApp']."/logo.png" ?>">
                    </a>
                </div>
            </div>
        </div>

        <br>
        <div class="container pago">
            <div class="form-signin form-horizontal" >
                <div style="padding-left: 15px;">  
                    <h2 class="form-signin-heading">Pago con deposito Bancario</h2>

                    <?php
                    /** @var TbActiveForm $form */
                    $form = $this->beginWidget(
                            'bootstrap.widgets.TbActiveForm', array(
                        'id' => 'horizontalForm',
                        'type' => 'horizontal',
                            )
                    );
                    ?>

                    
                    <div class="alert alert-block alert-danger">
                        <p class="note">Los Campos con  <span class="required">*</span> Son obligatorios</p>
                        <?php echo $form->errorSummary($model); ?>
                    </div>                    
                    
                    <?php echo $form->hiddenField($model, 'TRANSACCION_id'); ?>
                    <div class="control-group">
                        <div class="control-label sub">
                            <strong> Tipo de pago</strong> 
                        </div>
                        <div class="controls">                        
                           <select name="tipoPago" style="width:192px;">
                                <option value="2">Depósito</option>
                                <option value="3">Transferencia</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <div class="control-label sub">
                            <?php echo $form->labelEx($model, 'bancoEmisor'); ?> 
                        </div>
                        <div class="controls">                        
                            <?php echo $form->dropDownList($model, 'bancoEmisor', CHtml::listData(Bancos::model()->findAll('funcion="Emisor" || funcion="Ambos"'), 'id', 'nombre')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="control-label sub">
                            <?php echo $form->labelEx($model, 'bancoReceptor'); ?> 
                        </div>
                        <div class="controls">                        
                            <?php echo $form->dropDownList($model, 'bancoReceptor', CHtml::listData(Bancos::model()->findAll('funcion="Receptor" || funcion="Ambos"'), 'id', 'nombre')); ?>
                        </div>
                    </div>
                    <div class="control-group">                        
                        <div class="control-label sub">
                            <?php echo $form->labelEx($model, 'fechaPago'); ?>
                        </div>
                        <div class="controls">
                            <?php
                            $this->widget('bootstrap.widgets.TbDatePicker', array(
                                'model' => $model,
                                'attribute' => 'fechaPago',
                                'options' => array(
                                    'language' => 'es',
                                    'format' => 'yyyy/mm/dd',
                                    'autoclose' => true,
                            )));
                            ?>
                            <?php echo $form->error($model, 'fechaPago'); ?>
                        </div>
                    </div>
                    <div class="control-group">

                        <div class="control-label sub">
                            <?php echo $form->labelEx($model, 'rifCliente'); ?>
                        </div>
                        <div class="controls">
                            <?php echo $form->textField($model, 'rifCliente'); ?>
                            <?php echo $form->error($model, 'rifCliente'); ?>
                        </div>
                    </div>
                    <div class="control-group">

                        <div class="control-label sub">
                            <?php echo $form->labelEx($model, 'numeroReferencia'); ?>
                        </div>
                        <div class="controls">
                            <?php echo $form->textField($model, 'numeroReferencia'); ?>
                            <?php echo $form->error($model, 'numeroReferencia'); ?>
                        </div>
                    </div>

                    <div class="control-group">

                        <div class="control-label sub">
                            <?php echo $form->labelEx($model, 'monto'); ?>
                        </div>
                        <div class="controls">
                            <?php echo $form->textField($model, 'monto', array('readonly' => true)); ?>
                            <?php echo $form->error($model, 'monto'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                    <input type="checkbox" required name="terminos" value="terminos">&nbsp;&nbsp;Acepto los <a href="#" role="button" onmouseup="$('#modalTerminos').modal('show')">terminos de uso</a><br>
                    </div>
                    <div style="text-align: center">
                        <br>
                        <?php echo CHtml::submitButton('Realizar Pago', array('class' => 'btn btn-info')); ?>
                         
                         <div style="float: right;">
                            <?php echo CHtml::link('Volver',$valoresEstaticos['cancelUrl'], array('class' => 'btn btn-default')); ?>
                             
                         </div>
                       
                    </div>

                    <?php $this->endWidget(); ?>
                    <br>
                </div>
            </div>
        </div> <!-- /container -->

        
        <div id="modalTerminos" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Términos y Condiciones </h4>
                    </div>
                    <div class="modal-body">
                        <p>    LOS DATOS INTRODUCIDOS SERAN VALIDADOS CON EL BANCO EMISOR DE LA TARJETA QUE UD SUMINISTRO. EL ABUSO O MANEJO DE INFORMACION EN FORMA FRAUDULENTA ESTA PENADA EN LA LEY ESPECIAL CONTRA DELITOS INFORMATICOS</p>
            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                        
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
                <!-- Modal -->
        
        <div class="footer navbar navbar-fixed-bottom" style="background-color: #1b1b1b; padding: 5px 0;">
            <div class="container">
                <p>Copyright 2012 Global Payment - <a href="http://www.globalr.net" target="_blank">Global Resources</a></p>
                <p>Caracas - San Crist&oacute;bal </p>
            </div>
        </div> 
    </body>
</html>
