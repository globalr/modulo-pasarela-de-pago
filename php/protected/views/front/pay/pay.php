<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Global Payment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=""> 
        <meta name="author" content="">

        <!-- Le styles -->        
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/css/payment.css" rel="stylesheet">


        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- scripts -->    
      
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/js/payment.js"></script>       
    </head>

    <body>
        <?php
        //0 desencriptar parametros
        /******************************************************************
         * la siguiente linea es la correcta para pasar a produccion en el*
         * servidor de globalr                                            *
         *include '../../../dev-payment/lib/globalr/cypher/cypher.php';   *
         ******************************************************************/
        
        /******************************************************************
         * pruebas en local                                               *
         * include '../../payment/lib/globalr/cypher/cypher.php';         *
         * ****************************************************************/
        //include '../../payment/lib/globalr/cypher/cypher.php';
        //include '../../../dev-payment/lib/globalr/cypher/cypher.php';   
        ini_set("display_errors", 0);
        
        
 /******************OJO esto lo añadi yo **********/       
        //yii::import('application.payment.*');
        require_once 'lib/globalr/cypher/cypher.php';
/*************************************************/

        // 1 calcular el tiempo restante para completar el pago
        // si existe la variable de sesion tiempo, que indica la fecha de vencimiento
        if (isset($_SESSION['idTransaccion']) && isset($_SESSION['tiempo']) && ($_SESSION['idTransaccion'] == $_POST['idTransaccion'])) {
            $fechaVecimiento = $_SESSION['tiempo'];
        } else {
            // si no existe se calcula
            $tiempoRestante = $_POST['tiempo'];
            $strtotimeArg = "+$tiempoRestante seconds";
            $fechaVecimiento = strtotime(date('Y-m-d H:i:s', strtotime($strtotimeArg)));
            $_SESSION['tiempo'] = $fechaVecimiento;
        }

        $hoy = strtotime(date('Y-m-d H:i:s'));
        $interval = $fechaVecimiento - $hoy;
        if ($interval > 0) {
            $minutes = floor($interval / 60);
            $seconds = ($interval % 60);
        } else {
            $minutes = 0;
            $seconds = 0;
        }
        $_SESSION['minutosRestan'] = $minutes;
        $_SESSION['segundosRestan'] = $seconds;


        
        
      /*  
        
        //********************** imprimmiendo lo que llega
        
        echo $idTransaccion = decrypt($_POST['idTransaccion']);
        echo '<br>';
        echo $tiempo = decrypt($_POST['tiempo']);
        echo '<br>';
        echo $totalfinal = decrypt($_POST['totalfinal']);
        echo '<br>';
        echo $cancelUrl = decrypt($_POST['cancelUrl']);
        echo '<br>';
        echo $css = decrypt($_POST['css']);
        echo '<br>';
        echo $urlActualizarEstatus = $_POST['urlActualizarEstatus'];
        echo '<br>';
        echo $logo = decrypt($_POST['logo']);
        echo '<br>';
        echo $redirectTo = decrypt($_POST['redirectTo']);
        echo '<br>';
        echo $urlServer = $_POST['urlServer'];
        echo '<br>';
        echo  $origen = decrypt($_POST['origen']);
        echo '<br>';
        echo $codigo = decrypt($_POST['codigo']);
        
        yii::app()->end();
        //****************************************************
        
       */ 
        
        
        
        
        
        
        
        
        
        
        
        
        // OJO CON ESTO. PREGUNTAR A SAMUEL
        // 
        // 
        // 
        // 2) actualizar los datos de la transacción
        // guardar las variables que vienen de payment como variables de session
        if (isset($_SESSION['idTransaccion'])) {
           
            // si no viene de la pagina de error
            if (isset($_POST['idTransaccion']) && !isset($_POST['fromerr'])) {

                $transaccion = decrypt($_POST['idTransaccion']);

                if ($transaccion != $_SESSION['idTransaccion']) {
                    $_SESSION['idTransaccion'] = $transaccion;
                    $_SESSION['totalfinal'] = decrypt($_POST['totalfinal']);
                    $_SESSION['codigo'] = decrypt($_POST['codigo']);
                    $_SESSION['origen'] = decrypt($_POST['origen']);
                    echo decrypt($_POST['totalfinal']);
                }
            }
        } else {
            $_SESSION['idTransaccion'] = decrypt($_POST['idTransaccion']);
            $_SESSION['totalfinal'] = decrypt($_POST['totalfinal']);
            $_SESSION['codigo'] = decrypt($_POST['codigo']);
            $_SESSION['origen'] = decrypt($_POST['origen']);
             
        }

        ?>


        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand pull-right" style="color: #999999" href="#">Global Payment</a>
                    <ul class="nav">
                        <li class="">
                            <a href="<?php echo $_POST['cancelUrl']; ?>">Volver</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="container" style="margin-top: 15px"> 

            <form name="principal" method="post" action="process" class="form-signin form-horizontal" autocomplete="off">
                <input type="hidden" name="css" value="<?php echo $_POST['css']; ?>"/>
                <input type="hidden" name="urlActualizarEstatus" value="<?php echo $_POST['urlActualizarEstatus']; ?>"/>
                <input type="hidden" name="logo" value="<?php echo $_POST['logo']; ?>"/>
                <input type="hidden" name="redirectTo" value="<?php echo $_POST['redirectTo']; ?>"/>			
                <input type="hidden" name="cancelUrl" value="<?php echo $_POST['cancelUrl']; ?>"/>	
                <input type="hidden" id="authorizerId" name="authorizerId"/>
                <input type="hidden" name="tiempo" value="<?php echo $_POST['tiempo']; ?>"/>	  
                <input type="hidden" name="idTransaccion" value="<?php echo $_SESSION['idTransaccion']; ?>"/>		                    		                                                  
                <input type="hidden" name="urlServer" value="<?php echo $_POST['urlServer']; ?>"/>		                    		                                                  

                <h2 class="form-signin-heading">Pago con tarjeta de cr&eacute;dito</h2>
                <?php if (isset($_POST['admin']) == false) { ?>                
                    <div class="alert alert-block" style="text-align: center">
                        Tiempo restante para pagar la transacci&oacute;n <span class="timer"><span id="minutos"><?php echo $_SESSION['minutosRestan']; ?></span>:<span id="segundos"><?php echo $_SESSION['segundosRestan']; ?></span></span>
                    </div> 
                    <?php
                } else {
                    echo "<input type=\"hidden\" name=\"admin\" value=\"true\"/>";
                }
                ?>                                
                <div class="control-group">
                    <label class="control-label sub">Total Bs.</label>
                    <div class="controls">
                        <input type="text" class="input-block-level" disabled="disabled" value="<?php echo $_SESSION['totalfinal']; ?>">	      
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label sub" for="cardNumber">Numero</label>
                    <div class="controls">
                        <input name="cardNumber" id="cardNumber" type="text" class="input-block-level" placeholder="Numero de tarjeta"  maxlength="19" autocomplete="off" tabindex="1" onkeypress="return checkNumber(event);">	      
                    </div>
                </div>    
                <div class="control-group">
                    <label class="control-label sub" for="authorizerId">Tipo</label>
                    <div class="controls">

                        <label class="checkbox inline">
                            <input type="radio" name="authorizerId" id="inlineCheckbox1" value="1" tabindex="2">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/images/visa.gif" alt="Visa" title="Visa"/>  
                        </label>
                        <label class="checkbox inline">
                            <input type="radio" name="authorizerId" id="inlineCheckbox2" value="2" tabindex="3">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/images/master.gif" alt="Mastercard" title="Mastercard"/>  
                        </label>
                        <label class="checkbox inline">
                            <input type="radio" name="authorizerId" id="inlineCheckbox3" value="3" tabindex="4">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/images/amex.gif" alt="American Express" title="American Express"/>  
                        </label>	

                    </div>
                </div>  
                <div class="control-group">
                    <label class="control-label sub" for="cardExpiryDate">Fecha de vencimiento (mmaa)</label>
                    <div class="controls">
                        <input id="cardExpiryDate" name="cardExpiryDate" type="text" class="input-block-level" placeholder="Fecha de vencimiento" maxlength="4" autocomplete="off" tabindex="5"
                               onkeypress="return checkNumber(event);" pattern="((0[1-9])|(1[0-2]))[1-9][0-9]">	      
                    </div>
                </div>  
                <div class="control-group">
                    <label class="control-label sub" for="cardSecurityCode">C&oacute;digo de seguridad
                        <a onclick="$('#helpModal').modal('show')" id="csc-hover">¿Qué es esto?</a>	    
                    </label>
                    <div class="controls">
                        <input id="cardSecurityCode" name="cardSecurityCode" type="password" class="input-block-level" placeholder="C&oacute;digo de seguridad" maxlength="5" autocomplete="off" tabindex="6" onkeypress="return checkNumber(event);">	      
                    </div>
                </div>  	
                <div class="control-group">
                    <label class="control-label sub" for="customerId">C&eacute;dula o Rif del cliente</label>
                    <div class="controls">
                        <input id="customerId" name="customerId" type="text" class="input-block-level" placeholder="C&eacute;dula o Rif del cliente" autocomplete="off" tabindex="7" onkeypress="return checkNumber(event);">	      
                    </div>
                </div>  

                <div class="well well-small" style="background-color: white; padding-bottom: 2px; padding-top: 2px">
                    <div class="media">
                        <div class="media-body">
                            <label class="checkbox">
                                <input type="checkbox" name="acceptconditions" id="acceptconditions" tabindex="8"> Acepto los <a href="#" role="button" onmouseup="$('#myModal').modal('show')">terminos de uso</a>  
                            </label>                             
                        </div>
                    </div>
                </div>                   

                <div class="control-group">
                    <div class="controls">
                        <div class="btn-group">
                            <button name="botonatras" id="botonatras" class="btn btn-large" type="button" onclick="volver()" tabindex="10">Volver</button>
                            <button name="botonpagar" id="botonpagar" class="btn btn-large" type="button" onclick="this.disabled = true; doSubmit()" tabindex="9">Pagar</button>
                        </div>  
                    </div>
                </div>				           

                <div class="well well-small">
                    <div class="media">
                        <a class="pull-left">
                            <span id="siteseal" class="media-object">
                                <script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=N5YS3CBkYsMB3mS7WdblBrVryJbYWLLyxPbxANIk4eZOrKoiAxZIw2idr">
                                </script>
                            </span>            
                        </a>
                        <div class="media-body">
                            Seguridad certificada.
                        </div>
                    </div>
                </div>                       	

            </form>  
            <form name="volverform" method="post" action="<?php echo $_POST['cancelUrl']; ?>">  </form>      	
        </div> <!-- /container -->



        <div class="footer navbar navbar-fixed-bottom" style="background-color: #1b1b1b; padding: 5px 0;">
            <div class="container">
                <p>Copyright 2012 Global Payment - <a href="http://www.globalr.net" target="_blank">Global Resources</a></p>
                <p>Caracas - San Crist&oacute;bal </p>
            </div>
        </div>

        <!-- Modal -->
        <div id="waitModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Procesando pago</h3>
            </div>
            <div class="modal-body">	  
                <div class="paragraphs">
                    <div class="row">
                        <div class="span4">
                            <img style="float:left" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/images/ajax-loader.gif" alt="cargando">      
                            <p>Por favor espere…</p>
                            <div style="clear:both"></div>
                        </div>    
                    </div>
                </div>	  	  	  
            </div>
        </div>        


        <!-- Modal -->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">T&eacute;rminos y Condiciones</h3>
            </div>
            <div class="modal-body" style="max-height: 200px">
                <p>    LOS DATOS INTRODUCIDOS SERAN VALIDADOS CON EL BANCO EMISOR DE LA TARJETA QUE UD SUMINISTRO. EL ABUSO O MANEJO DE INFORMACION EN FORMA FRAUDULENTA ESTA PENADA EN LA LEY ESPECIAL CONTRA DELITOS INFORMATICOS</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            </div>
        </div>  

        <!-- Modal -->
        <div id="helpModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">C&oacute;digo de seguridad</h3>
            </div>
            <div class="modal-body" style="max-height: 200px">
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 14px;font-weight: normal;line-height: 20px;">
                <p>En la mayor parte de las tarjetas de cr&eacute;dito, el c&oacute;digo de seguridad de la tarjeta son los tres ultimos n&uacute;meros
                    que aparecen en el lugar donde usted firma en la parte posterior de la Tarjeta de Cr&eacute;dito. En el 	
                    ejemplo, el c&oacute;digo de seguridad de la tarjeta es "267".</p>
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/images/codesec.png" alt="Codigo de seguridad" title="Codigo de seguridad"/>  
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            </div>
        </div>         


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/JavaScript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/js/util.js"></script> 
        <script type="text/javascript">
            document.principal.cardNumber.focus();	
            function doSubmit(){
                var retorno = true;
                var input;
                var mensaje='Por favor ingrese solo n\xfameros';
                
                if(document.principal.acceptconditions.checked == false){
                    document.principal.botonpagar.disabled = false;	
                    alert("Debe aceptar los terminos y condiciones");
                    return;
                }
                if(!fechaTDC(document.principal.cardExpiryDate)){
                    retorno =  false;
                    mensaje='Por favor ingrese 4 digitos con el fotmato mmyy';
                    input = document.principal.cardExpiryDate;
                }else{
                    if(!allnumeric(document.principal.cardNumber)){
                        //retorno =  false;
                        //input = document.principal.cardNumber;			
                    }else{
                        if(!allnumeric(document.principal.cardSecurityCode)){
                            retorno =  false;
                            input = document.principal.cardSecurityCode;			
                        }else{
                            if(!allnumeric(document.principal.customerId)){
                                retorno =  false;
                                input = document.principal.customerId;			
                            }
                        }				
                    }
		
                }
                if(!retorno){
                    alert(mensaje);  
                    input.focus();  		
                    document.principal.botonpagar.disabled = false;
                }else{
                    // validar el autorizedId 
                    if(document.principal.inlineCheckbox1.checked==false && document.principal.inlineCheckbox2.checked==false && document.principal.inlineCheckbox3.checked==false){
                        alert('Por favor ingrese un n\xfamero de tarjeta de un tipo conocido'); 
                        document.principal.botonpagar.disabled = false;	
                        retorno = false;			
                    }else{
                        $('#waitModal').modal('show');
                        document.principal.submit();
                    }
                }
                return retorno;					
            }
            function revisarTipo(){
		
                var numero = document.principal.cardNumber.value;
                var first = numero.charAt(0);
                var second = numero.charAt(1);
                var third = numero.charAt(2);
                document.principal.authorizerId.value=-1;					
			
                var backPos = "0px 0px";
		
                if (first == "4") {
                    //Visa
                    backPos = "110px 0px";
                    document.principal.authorizerId.value=1;
                }
                else if ( (first == "3") && ((second == "4") || (second == "7")) ) {
                    //American Express
                    backPos = "170px 0px";
                    document.principal.authorizerId.value=3;			
                }
                else if ( (first == "5") ) {
                    //Mastercard
                    backPos = "60px 0px";
                    document.principal.authorizerId.value=2;			

                }
                document.getElementById("imgicon").style.backgroundPosition=backPos;
            }
            function volver(){
                document.volverform.submit();	
            }
            function openwindow(url, title, w, h){
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
                window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);	
            }
        </script> 
        <script type="text/javascript">
            window.setInterval(timerListener, 1000);
            function timerListener() { 
                decrementSeconds("segundos");
            }      
        </script> 
        <?php
        echo "<script type=\"text/javascript\">";
        echo "try{calcularTiempos(" . $_SESSION['minutosRestan'] . "," . $_SESSION['segundosRestan'] . ");}catch(e){}";
        echo "</script>";
        ?>        

    </body></html>
