<?php

require_once "lib/nusoap.php";

$URL = "http://payment.globalr.net/consultarEstatusTransaccionPaymentDesarrollo.php";
$namespace = $URL . '?wsdl';
//using soap_server to create server object


$server = new soap_server();
$server->configureWSDL('paymentcepd', $namespace);


$server->register("consultarEstatusTransaccionPaymentDesarrollo", array('transaccionid' => 'xsd:int', 'appid' => 'xsd:int'), array("return" => "xsd:string"));

/**
  Dado el id del sistema y el id de la transaccion
  se consulta a sitef el estatus de la transaccion
  y se retorna al client
 */
function consultarEstatusTransaccionPaymentDesarrollo($transaccionid, $appid) {
    $status = "base";
    $mysqli = new mysqli("localhost", "globalr_paydev", "gl0b4lP4y_2012", "globalr_pagos_dev");

    /* comprobar la conexión */
    if (!mysqli_connect_errno()) {
        $consulta = "SELECT estatus FROM transaccion WHERE transaccion_id=$transaccionid AND aplicacion_id=$appid";

        if ($resultado = $mysqli->query($consulta)) {

            /* obtener el array de objetos */
            if ($fila = $resultado->fetch_row()) {
                $status = $fila[0];
            }

            /* liberar el conjunto de resultados */
            $resultado->close();
        }
        /* cerrar la conexión */
        $mysqli->close();
    }
    return $status;
}

$server->service($HTTP_RAW_POST_DATA);
?>