
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Global Payment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=""> 
        <meta name="author" content="">

        <!-- Le styles -->        
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/css/payment.css" rel="stylesheet">
 

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/pay/resources/js/payment.js"></script>       
    </head>

    <body style="background-image: url('<?php echo Yii::app()->request->baseUrl."/images/".$valoresEstaticos['dirApp']."/fondo.jpg" ?>'); background-size: 100% 100%; background-repeat: no-repeat">
        
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                   <a class="navbar-brand"  href="#">
                       <img style="height: 60px; margin:5px;" src="<?php echo Yii::app()->request->baseUrl."/images/".$valoresEstaticos['dirApp']."/logo.png" ?>">
                   </a>
                    
                    
                </div>
            </div>
        </div>
        <br>
        <div class="container pago" >            
           
            
            <div class="form-signin form-horizontal" >
                
                 <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'block'=>true, // display a larger alert block?
                    'fade'=>true, // use transitions?
                    'closeText'=>false, // close link text - if set to false, no close link is displayed
                    'alerts'=>array( // configurations per alert type
                        'info'=>array('block'=>true, 'fade'=>true,'closeText'=>false), // success, info, warning, error or danger
                    ),
                )); ?>
                <div style="padding-left: 15px;">  
                    <h2 class="form-signin-heading">Pago con Tarjeta de Credito</h2>

                    <?php
                    /** @var TbActiveForm $form */
                    $form = $this->beginWidget(
                            'bootstrap.widgets.TbActiveForm', array(
                        'id' => 'horizontalForm',
                        'type' => 'horizontal',
                            )
                    );
                    ?>
                    
                    
                    
                    <div class="alert alert-block alert-danger" >
                        Tiempo restante para pagar la transaccion <span class="timer"><span id="segundos"></span></span>
                        <?php echo $form->errorSummary($model); ?>
                    </div> 
                   <!-- <p class="note">Los Campos con  <span class="required">*</span> Son obligatorios</p> -->

                    <?php echo $form->hiddenField($model, 'idTransaccion'); ?> 
                    <?php echo $form->hiddenField($model, 'tiempo',array('id'=>'tiempo')); ?>
                    
                    

                    <div class="control-group">
                        
                        <div class="control-label sub">
                        <?php echo $form->labelEx($model, 'monto'); ?>
                        </div>
                        <div class="controls">
                        <?php echo $form->textField($model, 'monto',array('readonly'=>true)); ?>
                        <?php echo $form->error($model, 'monto'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                       
                        <div class="control-label sub">
                        <?php echo $form->labelEx($model, 'numeroTarjeta'); ?>
                        </div>
                        <div class="controls">
                        <?php echo $form->textField($model, 'numeroTarjeta'); ?>
                        <?php echo $form->error($model, 'numeroTarjeta'); ?>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <div class="control-label sub">
                        <?php echo $form->labelEx($model, 'tipoTarjeta'); ?>
                        </div>
                        <div class="controls">
                    <?php echo $form->radioButtonList(
                        $model,
                        'tipoTarjeta',
                        array(
                            '<img src="'.Yii::app()->request->baseUrl.'/assets/pay/resources/images/visa.gif" alt="Visa" title="Visa"/> ',
                            '<img src="'.Yii::app()->request->baseUrl.'/assets/pay/resources/images/master.gif" alt="Mastercard" title="Mastercard"/> ',
                        ),
                       array(
                            'labelOptions'=>array('style'=>'display:inline'), // add this code
                            'separator'=>'  ',
                        )     
                    ); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        
                        <div class="control-label sub">
                        <?php echo $form->labelEx($model, 'fechaVencimiento'); ?>
                        </div>
                        <div class="controls">
                        <?php echo $form->textField($model, 'fechaVencimiento'); ?>
                        <?php echo $form->error($model, 'fechaVencimiento'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        
                        <div class="control-label sub">
                        <?php echo $form->labelEx($model, 'codigoSeguridad'); ?>
                        </div>
                        <div class="controls">
                        <?php echo $form->textField($model, 'codigoSeguridad'); ?>
                        <?php echo $form->error($model, 'codigoSeguridad'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        
                        <div class="control-label sub">
                        <?php echo $form->labelEx($model, 'rif'); ?>
                        </div>
                        <div class="controls">
                        <?php echo $form->textField($model, 'rif'); ?>
                        <?php echo $form->error($model, 'rif'); ?>
                        </div>
                    </div>  
                    <div class="control-group">
                    <input type="checkbox" required name="terminos" value="terminos">&nbsp;&nbsp;Acepto los <a href="#" role="button" onmouseup="$('#modalTerminos').modal('show')">terminos de uso</a><br>
                    </div>
                     <br>
                     <div style="text-align: center">
                        
                            <?php echo CHtml::submitButton('Realizar Pago', array('class' => 'btn btn-info')); ?>
                         
                         <div style="float: right;">
                             <?php echo CHtml::link('Volver',$valoresEstaticos['cancelUrl'], array('class' => 'btn btn-default')); ?>
                             
                         </div>
                     </div>
                        
                    </div>

                    <?php $this->endWidget(); ?>      
                <br>
<div class="well well-small">
                    <div class="media">
                        <a class="pull-left">
                            <span id="siteseal" class="media-object">
                                <script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=N5YS3CBkYsMB3mS7WdblBrVryJbYWLLyxPbxANIk4eZOrKoiAxZIw2idr">
                                </script>
                            </span>            
                        </a>
                        <div class="media-body">
                            Seguridad certificada.
                        </div>
                    </div>
                </div> 
                </div>
            </div>
              	
        </div> <!-- /container -->


        <div class="footer navbar navbar-fixed-bottom" style="background-color: #1b1b1b; padding: 5px 0; text-align: center">
            <div class="container">
                <p>Copyright 2012 Global Payment - <a href="http://www.globalr.net" target="_blank">Global Resources</a></p>
            </div>
        </div>    


        <div id="modalTerminos" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Términos y Condiciones </h4>
                    </div>
                    <div class="modal-body">
                        <p>    LOS DATOS INTRODUCIDOS SERAN VALIDADOS CON EL BANCO EMISOR DE LA TARJETA QUE UD SUMINISTRO. EL ABUSO O MANEJO DE INFORMACION EN FORMA FRAUDULENTA ESTA PENADA EN LA LEY ESPECIAL CONTRA DELITOS INFORMATICOS</p>
            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                        
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
                <!-- Modal -->
        <div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">T&eacute;rminos y Condiciones</h3>
            </div>
            <div class="modal-body" style="max-height: 200px">
                <p>    LOS DATOS INTRODUCIDOS SERAN VALIDADOS CON EL BANCO EMISOR DE LA TARJETA QUE UD SUMINISTRO. EL ABUSO O MANEJO DE INFORMACION EN FORMA FRAUDULENTA ESTA PENADA EN LA LEY ESPECIAL CONTRA DELITOS INFORMATICOS</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            </div>
        </div>  
        
        <!-- SCRIPT PARA DECREMENTAR LOS SEGUNDOS -->
                     <script>
                        var segundos = document.getElementById("tiempo").value;
                        function contar(){
                                if(segundos <= 0){
                                        document.getElementById("segundos").innerHTML = "Tiempo Expirado ";                                        
                                        
                                } else {
                                        segundos--;
                                        document.getElementById("segundos").innerHTML = segundos + " segundos.";
                                }
                                
                               document.getElementById("tiempo").value = segundos; 
                        }
                        setInterval("contar()",1000);                       
                        
                        
                    </script>

           <!-- FIN SCRIPT PARA DECREMENTAR LOS SEGUNDOS -->
        
    </body>
</html>

