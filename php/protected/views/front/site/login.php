<?php if(!yii::app()->user->isGuest){
    yii::app()->controller->redirect('index');
}?>
<div class="main">
    <div class="icon_grids">
        <div class="wrap">
            
            <div class="section group" style="text-align: -webkit-center;">
                <div class="span_1_of_3" style="text-align: left;">
                    <div class="contact-form">
				  	<h3>Iniciar Sesion</h3>
					    
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'login-form',
                                'enableClientValidation' => true,
                                'focus' => array($model, 'username'),
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                            ));
                            ?>

                            <p class="note">Campos con * Son requeridos.</p>
                            
                            
                            <div class="row">
                                <label for="username">Usuario *</label>                                
                                <?php echo $form->textField($model, 'username'); ?>
                                <?php echo $form->error($model, 'username'); ?>
                            </div>
                            <br>
                            <div class="row">
                                <label for="password">Contraseña *</label>
                                <?php echo $form->passwordField($model, 'password'); ?>
                                <?php echo $form->error($model, 'password'); ?>
                            </div>
                            <br>
                            <div class=" row buttons">
                                <?php echo CHtml::submitButton('Iniciar Sesion', array('class' => 'botonPrincipal')); ?>
                            </div>

                            <?php $this->endWidget(); ?>
                       
                    </div>
                    



                    <div style="clear: both"></div>
                </div>
               
            </div>
        </div>

    </div>
</div>


