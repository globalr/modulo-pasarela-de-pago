      <div class="main">
      	 <div class="services">
      	   <div class="wrap">
	      	   <div class="section group">
				<div class="listview_1_of_2 images_1_of_2">
					<div class="grid_icon_service"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/tdc.png"></a></div>
				    <div class="text list_2_of_1">
						<h3>Pagos por Tarjeta de Crédito</h3>
                                                <p class="justify">Nuestra pasarela les brindará a tus clientes la posibilidad de realizar pagos con tarjetas de crédito
                                                    VISA y MASTERCARD de una manera segura y confiable. Los pagos se Gestionan inmediatamente y lo mejor es que no es necesario brindar informacion bancaria a ningun tercero</p>
						<div class="more_info"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">Más informacion</a></div>
				   </div>
			   </div>			
				<div class="listview_1_of_2 images_1_of_2">
					
                                            <div class="grid_icon_service"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/deposito.png"></a></div>	  
                                        
					<div class="text list_2_of_1">
						  <h3>Pagos por Depósito Bancario</h3>
						  <p class="justify"> Si tus clientes no poseen tarjeta de crédito ¡No te preocupes!. GlobalPayment proporciona cuentas en la mayoría de los bancos a nivel Nacional con la finalidad de brindar accesibilidad a tus clientes al momento de realizar los pagos.
                                                    </p>
                                                    <p>Con este servicio tus clientes solo deben registrar los datos del deposito y nuestro personal autorizado se encargará de verificar el pago y cargar el saldo a tu cuenta.</p>
                                                    
						  <div class="more_info"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">Más informacion</a></div>
					</div>
				</div>
			</div>  
			<div class="section group">
				<div class="listview_1_of_2 images_1_of_2">
					
                                            <div class="grid_icon_service"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/transferencia.png"></a></div>
					
				    <div class="text list_2_of_1">
						<h3>Pagos por Transferencia Bancaria</h3>
						<p class="justify">Es una forma cómoda de realizar tus pagos. Permite realizar 
                                                    estos desde casi cualquier parte del mundo, inclusive desde tu casa. Unicamente 
                                                    necesitas tener algunos datos esenciales a la mano y alguna conexión a internet.</p>
                                                <p class="justify">No necesitas enfrentarte al caotico viaje hasta el banco ni hacer colas dentro del mismo para pagar. </p> 
						<p class="justify">No te enfrentas a la inseguridad de llevar dinero en efectivo para cualquier lugar que vallas.</p> 
                                                <div class="more_info"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">Más informacion</a></div>
				   </div>
			   </div>			
				<div class="listview_1_of_2 images_1_of_2">
					
						  <div class="grid_icon_service"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/reportes.png"></a></div>
					
					<div class="text list_2_of_1">
						  <h3>Reporte de Transacciones</h3>
						  <p class="justify">Tu como representante del comercio electronico, puedes comprobar el estado de las transacciones desde cualquier lugar del mundo.</p>
						  <p class="justify">Inclusive puedes ver resultados estadisticos que te permitan identificar los patrones de pago que generan tus clientes.</p>
                                                  <div class="more_info"><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">Más informacion</a></div>
					</div>
				</div>
			</div>  
 	   
	     </div>
	   </div>
    </div>