<?php
$this->pageTitle = Yii::app()->name . ' - Integración';
$this->breadcrumbs = array(
    'Integración',
);
?>

<div class="main">
    <div class="icon_grids">
        <div class="wrap ">
            <div class="about_info">
                <h3>Con GlobalPayment es muy sencillo recibir tus pagos online</h3>
                <br><br>
                <div class="container">
                    <div class="pasos col-md-2">
                        <div class="imagen-paso ">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/pasos/productos.png" />
                        </div>
                        <div class="texto-paso">
                            <p >Los clientes ingresan a tu comercio electrónico y seleccionan los productos 
                            o servicios a pagar.</p>
                        </div>
                    </div>
                    <div class="pasos col-md-2">
                        <div class="imagen-paso ">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/pasos/pago.png" />
                        </div>
                        <div class="texto-paso">
                            <p >Serán redireccionados a GlobalPayment donde introducirán los 
                            datos de pago necesarios.</p>
                        </div>
                    </div>
                    <div class="pasos col-md-2">
                        <div class="imagen-paso ">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/pasos/verificacion.png" />
                        </div>
                        <div class="texto-paso">
                            <p >El sistema validará la información de pago 
                                y tramitará el pago respectivo.</p>
                        </div>
                    </div>
                    <div class="pasos col-md-2">
                        <div class="imagen-paso ">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/pasos/confirmacion.png" />
                        </div>
                        <div class="texto-paso">
                            <p >Se notificará por correo electrónico a tu  comercio y al cliente, el resultado de la transacción procesada.
                            </p>
                        </div>
                    </div>
                    <div class="pasos col-md-2">
                        <div class="imagen-paso ">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/pasos/dinero.png" />
                        </div>
                        <div class="texto-paso">
                            <p >Recibes el dinero en tu cuenta.</p>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>

