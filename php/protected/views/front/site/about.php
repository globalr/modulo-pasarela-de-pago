<div class="main">
    <div class="icon_grids ">
        <div class="wrap">
            <div class="container" style="width: 85%;">
                <div class="about_info ">
                    <h3>GlobalPayment</h3>
                    <P class="justify">La pasarela de pago GlobalPayment permite a los comercios electrónicos realizar el
                        cobro de sus productos y servicios a través de la Web de forma cómoda y segura, ofreciendo diversos 
                        servicios diseñados para facilitar este proceso.</p>

                    <h3>Ventajas y Beneficios</h3>
                    <P class="justify">• Realizar cobros de manera segura y sin que tus clientes revelen datos de tarjetas de crédito o bancarios a terceros.</p>
                    <P class="justify">• Registro detallado de cada una de sus operaciones.</p>
                    <P class="justify">• Alertas de pagos y transacciones a su correo electrónico de manera automática</p>
                    <P class="justify">• Ventas y cobros las 24 horas del dia.</p>
                    <P class="justify">• Podrás evitar grandes inversiones en infraestructura, desarrollos tecnológicos, mantenimiento y administración de un propio sistema de pago. Nostros nos encargamos de todo y tú solo pagas un monto por cada transacción exitosa.</p>
                    <P class="justify">• Adicionalmente cuenta con personal técnico, encargado de realizar el proceso interno de supervision de los pagos.</p>

                    <h3>Seguridad y Confianza</h3>
                    <P class="justify">Para nosotros es primordial proporcionar un ambiente en el cual tus clientes puedan realizar sus pagos de forma
                        segura y confiable, para ello hemos implementado un mecanismo de control estricto que nos permite realizar seguimientos completos 
                        de las operaciones realizadas por nuestra plataforma.</p>

                    <h3>Alta disponibilidad</h3>
                    <P class="justify">La infraestructura escalable de GlobalPayment se adapta a las necesidades de tu comercio y el sistema está en capacidad de soportar altos volúmenes transaccionales</p>



                    <div class="image group">
                        <div class="grid images_3_of_1">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/valores.jpg" alt="" />
                        </div>
                        <div class="grid about_desc">
                            <p><span>Misión</span></p>
                            <p class="justify">Nuestra misión ser su aliado tecnológico para que su negocio obtenga los máximos beneficios de las TI,
                                alineándonos con sus iniciativas estratégicas y aportando valor a los procesos clave de su empresa</p>
                            <p><span>Valores</span></p>
                            <p class="justify">
                                • Calidad en nuestros procesos.<br> 
                                • Pasión por el servicio y enfoque al cliente.<br>                               
                                • Innovación, siempre de la mano con la tecnología.<br> 
                                • Lealtad hacia nuestros aliados, clientes, socios, y colaboradores.</p>
                        </div>
                    </div>
                </div>           
            </div>
        </div>

    </div>
</div>