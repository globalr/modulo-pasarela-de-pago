<!DOCTYPE HTML>
<html>
<head>
<title>Contacto</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

</head>
<body>
		
      <div class="main">
                                                          

      	 <div class="contact">
			<div class="wrap">
			<!---start-contact---->
                        <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'block'=>true, // display a larger alert block?
                    'fade'=>true, // use transitions?
                    'closeText'=>false, // close link text - if set to false, no close link is displayed
                    'alerts'=>array( // configurations per alert type
                        'info'=>array('block'=>true, 'fade'=>true,'closeText'=>false), // success, info, warning, error or danger
                    ),
                )); ?>
			<div class="section group">				
				<div class="col span_2_of_1" style="margin-right:70px">
					<div class="contact_info">
			    	 	<h3>Buscanos en el Mapa</h3>
                                        <div class="map" style="border: 1px solid rgba(85, 85, 85, 0.19);">
					   	<iframe src="https://www.google.com/maps/embed?pb=!1m22!1m12!1m3!1d988.2978708071086!2d-72.21580809756986!3d7.769509057560794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m7!1i0!3e0!4m0!4m3!3m2!1d7.7694604!2d-72.2158912!5e0!3m2!1ses-419!2s!4v1400685485948" width="400" height="450" frameborder="0" style="border:0"></iframe>		
                                                </div>
      				</div>      			
				</div>				
				<div class="col span_1_3">
				  <div class="contact-form">
				  	<h3>Contáctanos</h3>					  

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<label for="name">Nombre</label>  
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<label for="email">Correo</label>  
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<label for="subject">Asunto</label>  
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<label for="body">Cuerpo</label>  
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Enviar',array('class'=>'botonPrincipal')); ?>
	</div>

<?php $this->endWidget(); ?>

				    </div>
  				</div>				
			  </div>
			  </div>
			<!---End-contact---->
			<div class="clear"> </div>
		</div>
    </div>

</body>
</html>

