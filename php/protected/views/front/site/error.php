<body>
    <div class="main">
        <div class="services">
            <div class="wrap">
                <div class="error-page"> 
                    <h3>Oops!</h3>
                    <p><?php echo CHtml::encode($message); ?></p>
                    <br><br><br>
                    
                        <div class="button btn btn-lg">
                            <span>
                                <a href="<?php echo yii::app()->user->returnUrl ?>">Volver</a>
                                <a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">Soporte</a>
                            </span>
                        </div>
                        



                </div>		 	   
            </div>
        </div>
    </div>

