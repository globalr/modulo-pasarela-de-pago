   <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/css/camera.css" rel="stylesheet" type='text/css' media='all'>
        
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/js/camera/jquery.min.js"></script>
        <script src='<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/js/camera/jquery.mobile.customized.min.js'></script>
        <script src='<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/js/camera/jquery.easing.1.3.js'></script> 
        <script src='<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/js/camera/camera.min.js'></script>
              
<script>
		jQuery(function(){
			
			jQuery('#camera_1').camera({ //here I declared some settings, the height and the presence of the thumbnails 
                                height: '39%',
                                pagination: true,
                                thumbnails: false,
                                fx: 'random',
                                time: 2000
                        });


		});
	</script>
<div class="fluid_container">
    <div class="camera_wrap camera_azure_skin" id="camera_1" >
            <div data-src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/camera/slides/slider1.jpg">
                <div class="camera_caption fadeIn">
                    Camera is a responsive/adaptive slideshow. <em>Try to resize the browser window</em>
                </div>
            </div>
            <div data-src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/camera/slides/leaf.jpg">
                <div class="camera_caption fadeIn">
                    It uses a light version of jQuery mobile, <em>navigate the slides by swiping with your fingers</em>
                </div>
            </div>
            <div  data-src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/camera/slides/road.jpg">
                <div class="camera_caption fadeIn">
                    <em>It's completely free</em> (even if a donation is appreciated)
                </div>
            </div>
            <div data-src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/camera/slides/sea.jpg">
                <div class="camera_caption fadeIn">
                    Camera slideshow provides many options <em>to customize your project</em> as more as possible
                </div>
            </div>
            <div data-src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/camera/slides/shelter.jpg">
                <div class="camera_caption fadeIn">
                    It supports captions, HTML elements and videos and <em>it's validated in HTML5</em> (<a href="http://validator.w3.org/check?uri=http%3A%2F%2Fwww.pixedelic.com%2Fplugins%2Fcamera%2F&amp;charset=%28detect+automatically%29&amp;doctype=Inline&amp;group=0&amp;user-agent=W3C_Validator%2F1.2" target="_blank">have a look</a>)
                </div>
            </div>
            <div data-src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/camera/slides/tree.jpg">
                <div class="camera_caption fadeIn">
                    Different color skins and layouts available, <em>fullscreen ready too</em>
                </div>
            </div>
        </div><!-- #camera_wrap_1 -->
    </div><!-- .fluid_container -->
    
 <div class="icon_grids">
     <div class="clear">
         <br>
     </div>
	 	   <div class="wrap">	 		
	 		 	<div class="section group">
				 <div class="grid_1_of_3 images_1_of_3">
					<div class="grid_icon "><a href="<?php echo Yii::app()->createUrl('site/servicios'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/g1.png"></a></div>
					  <h3>Servicios</h3>
                                          <p class="justify">Hemos diseñado pensando en su comodidad y seguridad una plataforma de pago que le 
                                              garantizara efectuar diversas formas de pago y transacciones en tiempo  real desde su casa u oficina 
                                              o sencillamente a través de su dispositivo móvil, completamente confiable…</p>
				     <div class="button"><span><a href="<?php echo Yii::app()->createUrl('site/servicios'); ?>">Saber más</a></span></div>
				</div>
				<div class="grid_1_of_3 images_1_of_3">
					  <div class="grid_icon"><a href="<?php echo Yii::app()->createUrl('site/integracion'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/g2.png"></a></div>
					  <h3>Integración</h3>
					  <p class="justify">Únete a nuestra lista de usuarios y disfruta de nuestros servicios donde te  ofrecemos 
                                              un ambiente donde puedas realizar tus pagos de manera segura y con confianza, es muy importante. 
                                              Por ello  implementamos una serie de elementos que nos permiten identificar a cada uno de los usuarios  
                                              brindando la tranquilidad de saber que estos son estrictamente verificados y validados….</p>
				     <div class="button"><span><a href="<?php echo Yii::app()->createUrl('site/integracion'); ?>">Saber más</a></span></div>
				</div>
				<div class="grid_1_of_3 images_1_of_3">
					  <div class="grid_icon"><a href="<?php echo Yii::app()->createUrl('site/soporte'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/idea/assets/images/g3.png"></a></div>
					  <h3>Soporte</h3>
					  <p class="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				     <div class="button"><span><a href="<?php echo Yii::app()->createUrl('site/soporte'); ?>">Saber más</a></span></div>
				</div>
			</div>
	    </div>
 </div>
<div class="main">
    <div class="container">            

 
    </div> <!-- /container -->

</div> <!-- /main -->



